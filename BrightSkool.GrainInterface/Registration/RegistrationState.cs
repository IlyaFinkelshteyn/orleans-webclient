﻿using BrightSkool.Domain.Models;
using Patterns.EventSourcing.Interface;
using System;

namespace BrightSkool.GrainInterface.Registration
{
    [Serializable]
    public class RegistrationState : ICanApplyEvent<RegistrationOperations, RegistrationState>
    {
        private string _status;
        public RegistrationModel _registrationModel { get; set; }
        public RegistrationState(RegistrationModel registrationmodel)
        {
            _registrationModel = registrationmodel;
        }

        public RegistrationState(string status, RegistrationModel registrationmodel)
        {
            _registrationModel = registrationmodel;
            _status = status;
        }

        public RegistrationState() { }

        public RegistrationState ApplyEvent(TimestampedValue<RegistrationOperations> value, RegistrationState currentState)
        {
            return value.Value.Match(SaveRegistration(currentState), ProcessRegistration(_status, currentState));
        }

        private static Func<RegistrationModel, RegistrationState> SaveRegistration(RegistrationState current)
        {
            return registration => new RegistrationState(current._registrationModel = registration);
        }

        private static Func<string, RegistrationModel, RegistrationState> ProcessRegistration(string status,RegistrationState current)
        {
            return (RegistrationStatus, RegistrationModel) => new RegistrationState(current._status = RegistrationStatus, current._registrationModel = RegistrationModel);
        }
    }
}
