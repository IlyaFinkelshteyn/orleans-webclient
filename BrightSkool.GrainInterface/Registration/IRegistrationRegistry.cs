﻿using Orleans.Concurrency;
using Patterns.Registry.Interface;
using Patterns.SmartCache.Interface;
using System;

namespace BrightSkool.GrainInterface.Registration
{
    [Immutable]
    public class RegistrationCatalogItem
    {
        public Guid Id { get; set; }
        public string StudentName { get; set; }
        public string Nationality { get; set; }
        public string standard { get; set; }
        public string Status { get; set; }
        public string SiblingId { get; set; }
    }

    public interface IRegistrationRegistryGrain : IRegistryGrain<IRegistrationItemGrain>
    {
    }
    public interface IRegistrationItemGrain : ICachedItemGrain<RegistrationCatalogItem>
    {
    }
}
