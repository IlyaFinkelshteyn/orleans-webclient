﻿using BrightSkool.Domain.Models;
using System;

namespace BrightSkool.GrainInterface.Registration
{
    [Serializable]
    public abstract class RegistrationOperations
    {
        private RegistrationOperations()
        {
        }

        public static RegistrationOperations SaveRegistration(RegistrationModel registration)
        {
            return new ChoiceTypes.SaveRegistration(registration);
        }

        public static RegistrationOperations ProcessRegistration(string status, RegistrationModel registration)
        {
            return new ChoiceTypes.ProcessRegistration(status,registration);
        }

        public abstract TResult Match<TResult>(Func<RegistrationModel, TResult> SaveFunc, Func<string,RegistrationModel, TResult> ProcessFunc);

        private static class ChoiceTypes
        {
            [Serializable]
            internal class SaveRegistration : RegistrationOperations
            {
                public SaveRegistration(RegistrationModel registration)
                {
                    _registrationmodel = registration;
                }

                private RegistrationModel _registrationmodel { get; }

                public override TResult Match<TResult>(Func<RegistrationModel, TResult> SaveFunc, Func<string,RegistrationModel, TResult> ProcessFunc)
                {
                    return SaveFunc(_registrationmodel);
                }
            }

            [Serializable]
            internal class ProcessRegistration : RegistrationOperations
            {
                public ProcessRegistration(string status, RegistrationModel registration)
                {
                    _registrationmodel = registration;
                    _registrationmodel.Status.Status = status;
                    _registrationmodel.Status.Code = status;
                    _status = status;
                }

                private RegistrationModel _registrationmodel { get; }
                private string _status { get; }

                public override TResult Match<TResult>(Func<RegistrationModel, TResult> SaveFunc, Func<string,RegistrationModel, TResult> ProcessFunc)
                {
                    return ProcessFunc(_status,_registrationmodel);
                }
            }
        }
    }
}
