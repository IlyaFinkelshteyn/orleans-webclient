﻿using BrightSkool.Domain.Models;
using Patterns.SmartCache.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BrightSkool.Domain.Models.CommonModel;

namespace BrightSkool.GrainInterface.Masters
{
    /// <summary>
    /// Grain interface ICountry
    /// </summary>
    public interface IApplicationStatus: ICachedItemGrainStringKey<List<ApplicationStatus>>
    {
    }
}
