﻿using BrightSkool.Domain.Models;
using Orleans;
using Patterns.SmartCache.Interface;
using System.Collections.Generic;
using System.Threading.Tasks;
using static BrightSkool.Domain.Models.CommonModel;

namespace BrightSkool.GrainInterface.Masters
{
    /// <summary>
    /// Grain interface IRace
    /// </summary>
    public interface IRace : ICachedItemGrainStringKey<List<Race>>
    {
    }
}
