#if !EXCLUDE_CODEGEN
#pragma warning disable 162
#pragma warning disable 219
#pragma warning disable 414
#pragma warning disable 649
#pragma warning disable 693
#pragma warning disable 1591
#pragma warning disable 1998
[assembly: global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0")]
[assembly: global::Orleans.CodeGeneration.OrleansCodeGenerationTargetAttribute("BrightSkool.GrainInterface, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null")]
namespace BrightSkool.GrainInterface.Registration
{
    using global::Orleans.Async;
    using global::Orleans;
    using global::System.Reflection;

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.SerializableAttribute, global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.GrainReferenceAttribute(typeof (global::BrightSkool.GrainInterface.Registration.IRegistration))]
    internal class OrleansCodeGenRegistrationReference : global::Orleans.Runtime.GrainReference, global::BrightSkool.GrainInterface.Registration.IRegistration
    {
        protected @OrleansCodeGenRegistrationReference(global::Orleans.Runtime.GrainReference @other): base (@other)
        {
        }

        protected @OrleansCodeGenRegistrationReference(global::System.Runtime.Serialization.SerializationInfo @info, global::System.Runtime.Serialization.StreamingContext @context): base (@info, @context)
        {
        }

        protected override global::System.Int32 InterfaceId
        {
            get
            {
                return 1148267176;
            }
        }

        public override global::System.String InterfaceName
        {
            get
            {
                return "global::BrightSkool.GrainInterface.Registration.IRegistration";
            }
        }

        public override global::System.Boolean @IsCompatible(global::System.Int32 @interfaceId)
        {
            return @interfaceId == 1148267176 || @interfaceId == -701106275;
        }

        protected override global::System.String @GetMethodName(global::System.Int32 @interfaceId, global::System.Int32 @methodId)
        {
            switch (@interfaceId)
            {
                case 1148267176:
                    switch (@methodId)
                    {
                        case -262900526:
                            return "SaveRegistration";
                        case 1820844817:
                            return "ProcessRegistration";
                        case 1245628999:
                            return "GetRegistration";
                        case -454132849:
                            return "GetState";
                        case -58790303:
                            return "GetEvents";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 1148267176 + ",methodId=" + @methodId);
                    }

                case -701106275:
                    switch (@methodId)
                    {
                        case -454132849:
                            return "GetState";
                        case -58790303:
                            return "GetEvents";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -701106275 + ",methodId=" + @methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + @interfaceId);
            }
        }

        public global::System.Threading.Tasks.Task @SaveRegistration(global::BrightSkool.Domain.Models.RegistrationModel @registrationmodel)
        {
            return base.@InvokeMethodAsync<global::System.Object>(-262900526, new global::System.Object[]{@registrationmodel});
        }

        public global::System.Threading.Tasks.Task @ProcessRegistration(global::System.String @status)
        {
            return base.@InvokeMethodAsync<global::System.Object>(1820844817, new global::System.Object[]{@status});
        }

        public global::System.Threading.Tasks.Task<global::BrightSkool.Domain.Models.RegistrationModel> @GetRegistration()
        {
            return base.@InvokeMethodAsync<global::BrightSkool.Domain.Models.RegistrationModel>(1245628999, null);
        }

        public global::System.Threading.Tasks.Task<global::BrightSkool.GrainInterface.Registration.RegistrationState> @GetState()
        {
            return base.@InvokeMethodAsync<global::BrightSkool.GrainInterface.Registration.RegistrationState>(-454132849, null);
        }

        public global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::Patterns.EventSourcing.Interface.TimestampedValue<global::BrightSkool.GrainInterface.Registration.RegistrationOperations>>> @GetEvents(global::System.Nullable<global::System.DateTime> @startTime, global::System.Nullable<global::System.DateTime> @endTime)
        {
            return base.@InvokeMethodAsync<global::System.Collections.Generic.List<global::Patterns.EventSourcing.Interface.TimestampedValue<global::BrightSkool.GrainInterface.Registration.RegistrationOperations>>>(-58790303, new global::System.Object[]{@startTime, @endTime});
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::Orleans.CodeGeneration.MethodInvokerAttribute("global::BrightSkool.GrainInterface.Registration.IRegistration", 1148267176, typeof (global::BrightSkool.GrainInterface.Registration.IRegistration)), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute]
    internal class OrleansCodeGenRegistrationMethodInvoker : global::Orleans.CodeGeneration.IGrainMethodInvoker
    {
        public global::System.Threading.Tasks.Task<global::System.Object> @Invoke(global::Orleans.Runtime.IAddressable @grain, global::Orleans.CodeGeneration.InvokeMethodRequest @request)
        {
            global::System.Int32 interfaceId = @request.@InterfaceId;
            global::System.Int32 methodId = @request.@MethodId;
            global::System.Object[] arguments = @request.@Arguments;
            if (@grain == null)
                throw new global::System.ArgumentNullException("grain");
            switch (interfaceId)
            {
                case 1148267176:
                    switch (methodId)
                    {
                        case -262900526:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistration)@grain).@SaveRegistration((global::BrightSkool.Domain.Models.RegistrationModel)arguments[0]).@Box();
                        case 1820844817:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistration)@grain).@ProcessRegistration((global::System.String)arguments[0]).@Box();
                        case 1245628999:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistration)@grain).@GetRegistration().@Box();
                        case -454132849:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistration)@grain).@GetState().@Box();
                        case -58790303:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistration)@grain).@GetEvents((global::System.Nullable<global::System.DateTime>)arguments[0], (global::System.Nullable<global::System.DateTime>)arguments[1]).@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 1148267176 + ",methodId=" + methodId);
                    }

                case -701106275:
                    switch (methodId)
                    {
                        case -454132849:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistration)@grain).@GetState().@Box();
                        case -58790303:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistration)@grain).@GetEvents((global::System.Nullable<global::System.DateTime>)arguments[0], (global::System.Nullable<global::System.DateTime>)arguments[1]).@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -701106275 + ",methodId=" + methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + interfaceId);
            }
        }

        public global::System.Int32 InterfaceId
        {
            get
            {
                return 1148267176;
            }
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.SerializerAttribute(typeof (global::BrightSkool.GrainInterface.Registration.RegistrationState)), global::Orleans.CodeGeneration.RegisterSerializerAttribute]
    internal class OrleansCodeGenBrightSkool_GrainInterface_Registration_RegistrationStateSerializer
    {
        private static readonly global::System.Reflection.FieldInfo field0 = typeof (global::BrightSkool.GrainInterface.Registration.RegistrationState).@GetTypeInfo().@GetField("_status", (System.@Reflection.@BindingFlags.@Instance | System.@Reflection.@BindingFlags.@NonPublic | System.@Reflection.@BindingFlags.@Public));
        private static readonly global::System.Func<global::BrightSkool.GrainInterface.Registration.RegistrationState, global::System.String> getField0 = (global::System.Func<global::BrightSkool.GrainInterface.Registration.RegistrationState, global::System.String>)global::Orleans.Serialization.SerializationManager.@GetGetter(field0);
        private static readonly global::System.Action<global::BrightSkool.GrainInterface.Registration.RegistrationState, global::System.String> setField0 = (global::System.Action<global::BrightSkool.GrainInterface.Registration.RegistrationState, global::System.String>)global::Orleans.Serialization.SerializationManager.@GetReferenceSetter(field0);
        [global::Orleans.CodeGeneration.CopierMethodAttribute]
        public static global::System.Object DeepCopier(global::System.Object original)
        {
            global::BrightSkool.GrainInterface.Registration.RegistrationState input = ((global::BrightSkool.GrainInterface.Registration.RegistrationState)original);
            global::BrightSkool.GrainInterface.Registration.RegistrationState result = new global::BrightSkool.GrainInterface.Registration.RegistrationState();
            global::Orleans.@Serialization.@SerializationContext.@Current.@RecordObject(original, result);
            result.@_registrationModel = (global::BrightSkool.Domain.Models.RegistrationModel)global::Orleans.Serialization.SerializationManager.@DeepCopyInner(input.@_registrationModel);
            setField0(result, getField0(input));
            return result;
        }

        [global::Orleans.CodeGeneration.SerializerMethodAttribute]
        public static void Serializer(global::System.Object untypedInput, global::Orleans.Serialization.BinaryTokenStreamWriter stream, global::System.Type expected)
        {
            global::BrightSkool.GrainInterface.Registration.RegistrationState input = (global::BrightSkool.GrainInterface.Registration.RegistrationState)untypedInput;
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@_registrationModel, stream, typeof (global::BrightSkool.Domain.Models.RegistrationModel));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(getField0(input), stream, typeof (global::System.String));
        }

        [global::Orleans.CodeGeneration.DeserializerMethodAttribute]
        public static global::System.Object Deserializer(global::System.Type expected, global::Orleans.Serialization.BinaryTokenStreamReader stream)
        {
            global::BrightSkool.GrainInterface.Registration.RegistrationState result = new global::BrightSkool.GrainInterface.Registration.RegistrationState();
            global::Orleans.@Serialization.@DeserializationContext.@Current.@RecordObject(result);
            result.@_registrationModel = (global::BrightSkool.Domain.Models.RegistrationModel)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::BrightSkool.Domain.Models.RegistrationModel), stream);
            setField0(result, (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream));
            return (global::BrightSkool.GrainInterface.Registration.RegistrationState)result;
        }

        public static void Register()
        {
            global::Orleans.Serialization.SerializationManager.@Register(typeof (global::BrightSkool.GrainInterface.Registration.RegistrationState), DeepCopier, Serializer, Deserializer);
        }

        static OrleansCodeGenBrightSkool_GrainInterface_Registration_RegistrationStateSerializer()
        {
            Register();
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.SerializerAttribute(typeof (global::BrightSkool.Domain.Models.RegistrationModel)), global::Orleans.CodeGeneration.RegisterSerializerAttribute]
    internal class OrleansCodeGenBrightSkool_Domain_Models_RegistrationModelSerializer
    {
        [global::Orleans.CodeGeneration.CopierMethodAttribute]
        public static global::System.Object DeepCopier(global::System.Object original)
        {
            global::BrightSkool.Domain.Models.RegistrationModel input = ((global::BrightSkool.Domain.Models.RegistrationModel)original);
            global::BrightSkool.Domain.Models.RegistrationModel result = new global::BrightSkool.Domain.Models.RegistrationModel();
            global::Orleans.@Serialization.@SerializationContext.@Current.@RecordObject(original, result);
            result.@AllergicTo = input.@AllergicTo;
            result.@AllergyType = input.@AllergyType;
            result.@Applicant = (global::BrightSkool.Domain.Models.CommonModel.Person)global::Orleans.Serialization.SerializationManager.@DeepCopyInner(input.@Applicant);
            result.@ApplicationNumber = input.@ApplicationNumber;
            result.@Email = input.@Email;
            result.@GuardianDetails = (global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Guardian>)global::Orleans.Serialization.SerializationManager.@DeepCopyInner(input.@GuardianDetails);
            result.@Id = (global::System.Guid)global::Orleans.Serialization.SerializationManager.@DeepCopyInner(input.@Id);
            result.@IsSiblingStudyingInSameSchool = input.@IsSiblingStudyingInSameSchool;
            result.@IsTermsAndConditionAccept = input.@IsTermsAndConditionAccept;
            result.@IsUsageOfDataAccept = input.@IsUsageOfDataAccept;
            result.@Level = input.@Level;
            result.@MotherTongue = input.@MotherTongue;
            result.@Nationality = (global::BrightSkool.Domain.Models.CommonModel.Nationality)global::Orleans.Serialization.SerializationManager.@DeepCopyInner(input.@Nationality);
            result.@PIN = (global::BrightSkool.Domain.Models.CommonModel.PIN)global::Orleans.Serialization.SerializationManager.@DeepCopyInner(input.@PIN);
            result.@PhoneNumber = (global::BrightSkool.Domain.Models.CommonModel.Phone)global::Orleans.Serialization.SerializationManager.@DeepCopyInner(input.@PhoneNumber);
            result.@RegisteredDate = input.@RegisteredDate;
            result.@Religion = (global::BrightSkool.Domain.Models.CommonModel.Religion)global::Orleans.Serialization.SerializationManager.@DeepCopyInner(input.@Religion);
            result.@SecondLanguage = input.@SecondLanguage;
            result.@Status = (global::BrightSkool.Domain.Models.ApplicationStatus)global::Orleans.Serialization.SerializationManager.@DeepCopyInner(input.@Status);
            result.@StudentAddress = (global::BrightSkool.Domain.Models.CommonModel.Address)global::Orleans.Serialization.SerializationManager.@DeepCopyInner(input.@StudentAddress);
            result.@StudentId = input.@StudentId;
            result.@UploadedDocuments = (global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.StudentDocuments>)global::Orleans.Serialization.SerializationManager.@DeepCopyInner(input.@UploadedDocuments);
            return result;
        }

        [global::Orleans.CodeGeneration.SerializerMethodAttribute]
        public static void Serializer(global::System.Object untypedInput, global::Orleans.Serialization.BinaryTokenStreamWriter stream, global::System.Type expected)
        {
            global::BrightSkool.Domain.Models.RegistrationModel input = (global::BrightSkool.Domain.Models.RegistrationModel)untypedInput;
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@AllergicTo, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@AllergyType, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@Applicant, stream, typeof (global::BrightSkool.Domain.Models.CommonModel.Person));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@ApplicationNumber, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@Email, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@GuardianDetails, stream, typeof (global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Guardian>));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@Id, stream, typeof (global::System.Guid));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@IsSiblingStudyingInSameSchool, stream, typeof (global::System.Boolean));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@IsTermsAndConditionAccept, stream, typeof (global::System.Boolean));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@IsUsageOfDataAccept, stream, typeof (global::System.Boolean));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@Level, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@MotherTongue, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@Nationality, stream, typeof (global::BrightSkool.Domain.Models.CommonModel.Nationality));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@PIN, stream, typeof (global::BrightSkool.Domain.Models.CommonModel.PIN));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@PhoneNumber, stream, typeof (global::BrightSkool.Domain.Models.CommonModel.Phone));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@RegisteredDate, stream, typeof (global::System.DateTime));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@Religion, stream, typeof (global::BrightSkool.Domain.Models.CommonModel.Religion));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@SecondLanguage, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@Status, stream, typeof (global::BrightSkool.Domain.Models.ApplicationStatus));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@StudentAddress, stream, typeof (global::BrightSkool.Domain.Models.CommonModel.Address));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@StudentId, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@UploadedDocuments, stream, typeof (global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.StudentDocuments>));
        }

        [global::Orleans.CodeGeneration.DeserializerMethodAttribute]
        public static global::System.Object Deserializer(global::System.Type expected, global::Orleans.Serialization.BinaryTokenStreamReader stream)
        {
            global::BrightSkool.Domain.Models.RegistrationModel result = new global::BrightSkool.Domain.Models.RegistrationModel();
            global::Orleans.@Serialization.@DeserializationContext.@Current.@RecordObject(result);
            result.@AllergicTo = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@AllergyType = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@Applicant = (global::BrightSkool.Domain.Models.CommonModel.Person)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::BrightSkool.Domain.Models.CommonModel.Person), stream);
            result.@ApplicationNumber = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@Email = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@GuardianDetails = (global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Guardian>)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Guardian>), stream);
            result.@Id = (global::System.Guid)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.Guid), stream);
            result.@IsSiblingStudyingInSameSchool = (global::System.Boolean)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.Boolean), stream);
            result.@IsTermsAndConditionAccept = (global::System.Boolean)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.Boolean), stream);
            result.@IsUsageOfDataAccept = (global::System.Boolean)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.Boolean), stream);
            result.@Level = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@MotherTongue = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@Nationality = (global::BrightSkool.Domain.Models.CommonModel.Nationality)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::BrightSkool.Domain.Models.CommonModel.Nationality), stream);
            result.@PIN = (global::BrightSkool.Domain.Models.CommonModel.PIN)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::BrightSkool.Domain.Models.CommonModel.PIN), stream);
            result.@PhoneNumber = (global::BrightSkool.Domain.Models.CommonModel.Phone)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::BrightSkool.Domain.Models.CommonModel.Phone), stream);
            result.@RegisteredDate = (global::System.DateTime)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.DateTime), stream);
            result.@Religion = (global::BrightSkool.Domain.Models.CommonModel.Religion)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::BrightSkool.Domain.Models.CommonModel.Religion), stream);
            result.@SecondLanguage = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@Status = (global::BrightSkool.Domain.Models.ApplicationStatus)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::BrightSkool.Domain.Models.ApplicationStatus), stream);
            result.@StudentAddress = (global::BrightSkool.Domain.Models.CommonModel.Address)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::BrightSkool.Domain.Models.CommonModel.Address), stream);
            result.@StudentId = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@UploadedDocuments = (global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.StudentDocuments>)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.StudentDocuments>), stream);
            return (global::BrightSkool.Domain.Models.RegistrationModel)result;
        }

        public static void Register()
        {
            global::Orleans.Serialization.SerializationManager.@Register(typeof (global::BrightSkool.Domain.Models.RegistrationModel), DeepCopier, Serializer, Deserializer);
        }

        static OrleansCodeGenBrightSkool_Domain_Models_RegistrationModelSerializer()
        {
            Register();
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.SerializerAttribute(typeof (global::BrightSkool.Domain.Models.CommonModel.Guardian)), global::Orleans.CodeGeneration.RegisterSerializerAttribute]
    internal class OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_GuardianSerializer
    {
        [global::Orleans.CodeGeneration.CopierMethodAttribute]
        public static global::System.Object DeepCopier(global::System.Object original)
        {
            global::BrightSkool.Domain.Models.CommonModel.Guardian input = ((global::BrightSkool.Domain.Models.CommonModel.Guardian)original);
            global::BrightSkool.Domain.Models.CommonModel.Guardian result = new global::BrightSkool.Domain.Models.CommonModel.Guardian();
            global::Orleans.@Serialization.@SerializationContext.@Current.@RecordObject(original, result);
            result.@BusinessEmail = input.@BusinessEmail;
            result.@Email = input.@Email;
            result.@FirstName = input.@FirstName;
            result.@GuardianAddress = (global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Address>)global::Orleans.Serialization.SerializationManager.@DeepCopyInner(input.@GuardianAddress);
            result.@GuardianBusinessPhone = (global::BrightSkool.Domain.Models.CommonModel.Phone)global::Orleans.Serialization.SerializationManager.@DeepCopyInner(input.@GuardianBusinessPhone);
            result.@GuardianPhone = (global::BrightSkool.Domain.Models.CommonModel.Phone)global::Orleans.Serialization.SerializationManager.@DeepCopyInner(input.@GuardianPhone);
            result.@LastName = input.@LastName;
            result.@MiddleName = input.@MiddleName;
            result.@RelationShip = input.@RelationShip;
            return result;
        }

        [global::Orleans.CodeGeneration.SerializerMethodAttribute]
        public static void Serializer(global::System.Object untypedInput, global::Orleans.Serialization.BinaryTokenStreamWriter stream, global::System.Type expected)
        {
            global::BrightSkool.Domain.Models.CommonModel.Guardian input = (global::BrightSkool.Domain.Models.CommonModel.Guardian)untypedInput;
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@BusinessEmail, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@Email, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@FirstName, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@GuardianAddress, stream, typeof (global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Address>));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@GuardianBusinessPhone, stream, typeof (global::BrightSkool.Domain.Models.CommonModel.Phone));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@GuardianPhone, stream, typeof (global::BrightSkool.Domain.Models.CommonModel.Phone));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@LastName, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@MiddleName, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@RelationShip, stream, typeof (global::System.String));
        }

        [global::Orleans.CodeGeneration.DeserializerMethodAttribute]
        public static global::System.Object Deserializer(global::System.Type expected, global::Orleans.Serialization.BinaryTokenStreamReader stream)
        {
            global::BrightSkool.Domain.Models.CommonModel.Guardian result = new global::BrightSkool.Domain.Models.CommonModel.Guardian();
            global::Orleans.@Serialization.@DeserializationContext.@Current.@RecordObject(result);
            result.@BusinessEmail = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@Email = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@FirstName = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@GuardianAddress = (global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Address>)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Address>), stream);
            result.@GuardianBusinessPhone = (global::BrightSkool.Domain.Models.CommonModel.Phone)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::BrightSkool.Domain.Models.CommonModel.Phone), stream);
            result.@GuardianPhone = (global::BrightSkool.Domain.Models.CommonModel.Phone)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::BrightSkool.Domain.Models.CommonModel.Phone), stream);
            result.@LastName = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@MiddleName = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@RelationShip = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            return (global::BrightSkool.Domain.Models.CommonModel.Guardian)result;
        }

        public static void Register()
        {
            global::Orleans.Serialization.SerializationManager.@Register(typeof (global::BrightSkool.Domain.Models.CommonModel.Guardian), DeepCopier, Serializer, Deserializer);
        }

        static OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_GuardianSerializer()
        {
            Register();
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.SerializerAttribute(typeof (global::BrightSkool.Domain.Models.CommonModel.Person)), global::Orleans.CodeGeneration.RegisterSerializerAttribute]
    internal class OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_PersonSerializer
    {
        [global::Orleans.CodeGeneration.CopierMethodAttribute]
        public static global::System.Object DeepCopier(global::System.Object original)
        {
            global::BrightSkool.Domain.Models.CommonModel.Person input = ((global::BrightSkool.Domain.Models.CommonModel.Person)original);
            global::BrightSkool.Domain.Models.CommonModel.Person result = new global::BrightSkool.Domain.Models.CommonModel.Person();
            global::Orleans.@Serialization.@SerializationContext.@Current.@RecordObject(original, result);
            result.@DOB = (global::BrightSkool.Domain.Models.CommonModel.DOB)global::Orleans.Serialization.SerializationManager.@DeepCopyInner(input.@DOB);
            result.@FirstName = input.@FirstName;
            result.@Gender = input.@Gender;
            result.@LastName = input.@LastName;
            result.@MiddleName = input.@MiddleName;
            result.@ProfilePicFilePath = input.@ProfilePicFilePath;
            return result;
        }

        [global::Orleans.CodeGeneration.SerializerMethodAttribute]
        public static void Serializer(global::System.Object untypedInput, global::Orleans.Serialization.BinaryTokenStreamWriter stream, global::System.Type expected)
        {
            global::BrightSkool.Domain.Models.CommonModel.Person input = (global::BrightSkool.Domain.Models.CommonModel.Person)untypedInput;
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@DOB, stream, typeof (global::BrightSkool.Domain.Models.CommonModel.DOB));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@FirstName, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@Gender, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@LastName, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@MiddleName, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@ProfilePicFilePath, stream, typeof (global::System.String));
        }

        [global::Orleans.CodeGeneration.DeserializerMethodAttribute]
        public static global::System.Object Deserializer(global::System.Type expected, global::Orleans.Serialization.BinaryTokenStreamReader stream)
        {
            global::BrightSkool.Domain.Models.CommonModel.Person result = new global::BrightSkool.Domain.Models.CommonModel.Person();
            global::Orleans.@Serialization.@DeserializationContext.@Current.@RecordObject(result);
            result.@DOB = (global::BrightSkool.Domain.Models.CommonModel.DOB)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::BrightSkool.Domain.Models.CommonModel.DOB), stream);
            result.@FirstName = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@Gender = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@LastName = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@MiddleName = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@ProfilePicFilePath = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            return (global::BrightSkool.Domain.Models.CommonModel.Person)result;
        }

        public static void Register()
        {
            global::Orleans.Serialization.SerializationManager.@Register(typeof (global::BrightSkool.Domain.Models.CommonModel.Person), DeepCopier, Serializer, Deserializer);
        }

        static OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_PersonSerializer()
        {
            Register();
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.SerializerAttribute(typeof (global::BrightSkool.Domain.Models.CommonModel.DOB)), global::Orleans.CodeGeneration.RegisterSerializerAttribute]
    internal class OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_DOBSerializer
    {
        [global::Orleans.CodeGeneration.CopierMethodAttribute]
        public static global::System.Object DeepCopier(global::System.Object original)
        {
            global::BrightSkool.Domain.Models.CommonModel.DOB input = ((global::BrightSkool.Domain.Models.CommonModel.DOB)original);
            global::BrightSkool.Domain.Models.CommonModel.DOB result = new global::BrightSkool.Domain.Models.CommonModel.DOB();
            global::Orleans.@Serialization.@SerializationContext.@Current.@RecordObject(original, result);
            result.@Date = input.@Date;
            result.@Month = input.@Month;
            result.@Year = input.@Year;
            return result;
        }

        [global::Orleans.CodeGeneration.SerializerMethodAttribute]
        public static void Serializer(global::System.Object untypedInput, global::Orleans.Serialization.BinaryTokenStreamWriter stream, global::System.Type expected)
        {
            global::BrightSkool.Domain.Models.CommonModel.DOB input = (global::BrightSkool.Domain.Models.CommonModel.DOB)untypedInput;
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@Date, stream, typeof (global::System.Int32));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@Month, stream, typeof (global::System.Int32));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@Year, stream, typeof (global::System.Int32));
        }

        [global::Orleans.CodeGeneration.DeserializerMethodAttribute]
        public static global::System.Object Deserializer(global::System.Type expected, global::Orleans.Serialization.BinaryTokenStreamReader stream)
        {
            global::BrightSkool.Domain.Models.CommonModel.DOB result = new global::BrightSkool.Domain.Models.CommonModel.DOB();
            global::Orleans.@Serialization.@DeserializationContext.@Current.@RecordObject(result);
            result.@Date = (global::System.Int32)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.Int32), stream);
            result.@Month = (global::System.Int32)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.Int32), stream);
            result.@Year = (global::System.Int32)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.Int32), stream);
            return (global::BrightSkool.Domain.Models.CommonModel.DOB)result;
        }

        public static void Register()
        {
            global::Orleans.Serialization.SerializationManager.@Register(typeof (global::BrightSkool.Domain.Models.CommonModel.DOB), DeepCopier, Serializer, Deserializer);
        }

        static OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_DOBSerializer()
        {
            Register();
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.SerializerAttribute(typeof (global::Patterns.EventSourcing.Interface.TimestampedValue<>))]
    internal class OrleansCodeGenPatterns_EventSourcing_Interface_TimestampedValueSerializer<TValue>
    {
        [global::Orleans.CodeGeneration.CopierMethodAttribute]
        public static global::System.Object DeepCopier(global::System.Object original)
        {
            global::Patterns.EventSourcing.Interface.TimestampedValue<TValue> input = ((global::Patterns.EventSourcing.Interface.TimestampedValue<TValue>)original);
            global::Patterns.EventSourcing.Interface.TimestampedValue<TValue> result = (global::Patterns.EventSourcing.Interface.TimestampedValue<TValue>)global::System.Runtime.Serialization.FormatterServices.@GetUninitializedObject(typeof (global::Patterns.EventSourcing.Interface.TimestampedValue<TValue>));
            global::Orleans.@Serialization.@SerializationContext.@Current.@RecordObject(original, result);
            result.@Timestamp = input.@Timestamp;
            result.@Value = (TValue)global::Orleans.Serialization.SerializationManager.@DeepCopyInner(input.@Value);
            return result;
        }

        [global::Orleans.CodeGeneration.SerializerMethodAttribute]
        public static void Serializer(global::System.Object untypedInput, global::Orleans.Serialization.BinaryTokenStreamWriter stream, global::System.Type expected)
        {
            global::Patterns.EventSourcing.Interface.TimestampedValue<TValue> input = (global::Patterns.EventSourcing.Interface.TimestampedValue<TValue>)untypedInput;
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@Timestamp, stream, typeof (global::System.DateTime));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@Value, stream, typeof (TValue));
        }

        [global::Orleans.CodeGeneration.DeserializerMethodAttribute]
        public static global::System.Object Deserializer(global::System.Type expected, global::Orleans.Serialization.BinaryTokenStreamReader stream)
        {
            global::Patterns.EventSourcing.Interface.TimestampedValue<TValue> result = (global::Patterns.EventSourcing.Interface.TimestampedValue<TValue>)global::System.Runtime.Serialization.FormatterServices.@GetUninitializedObject(typeof (global::Patterns.EventSourcing.Interface.TimestampedValue<TValue>));
            global::Orleans.@Serialization.@DeserializationContext.@Current.@RecordObject(result);
            result.@Timestamp = (global::System.DateTime)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.DateTime), stream);
            result.@Value = (TValue)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (TValue), stream);
            return (global::Patterns.EventSourcing.Interface.TimestampedValue<TValue>)result;
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.RegisterSerializerAttribute]
    internal class OrleansCodeGenPatterns_EventSourcing_Interface_TimestampedValueSerializer_TValue_Registerer
    {
        public static void Register()
        {
            global::Orleans.Serialization.SerializationManager.@Register(typeof (global::Patterns.EventSourcing.Interface.TimestampedValue<>), typeof (OrleansCodeGenPatterns_EventSourcing_Interface_TimestampedValueSerializer<>));
        }

        static OrleansCodeGenPatterns_EventSourcing_Interface_TimestampedValueSerializer_TValue_Registerer()
        {
            Register();
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.SerializerAttribute(typeof (global::BrightSkool.Domain.Models.CommonModel.Nationality)), global::Orleans.CodeGeneration.RegisterSerializerAttribute]
    internal class OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_NationalitySerializer
    {
        [global::Orleans.CodeGeneration.CopierMethodAttribute]
        public static global::System.Object DeepCopier(global::System.Object original)
        {
            global::BrightSkool.Domain.Models.CommonModel.Nationality input = ((global::BrightSkool.Domain.Models.CommonModel.Nationality)original);
            global::BrightSkool.Domain.Models.CommonModel.Nationality result = new global::BrightSkool.Domain.Models.CommonModel.Nationality();
            global::Orleans.@Serialization.@SerializationContext.@Current.@RecordObject(original, result);
            result.@CountryNationality = input.@CountryNationality;
            result.@ISONationality = input.@ISONationality;
            return result;
        }

        [global::Orleans.CodeGeneration.SerializerMethodAttribute]
        public static void Serializer(global::System.Object untypedInput, global::Orleans.Serialization.BinaryTokenStreamWriter stream, global::System.Type expected)
        {
            global::BrightSkool.Domain.Models.CommonModel.Nationality input = (global::BrightSkool.Domain.Models.CommonModel.Nationality)untypedInput;
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@CountryNationality, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@ISONationality, stream, typeof (global::System.String));
        }

        [global::Orleans.CodeGeneration.DeserializerMethodAttribute]
        public static global::System.Object Deserializer(global::System.Type expected, global::Orleans.Serialization.BinaryTokenStreamReader stream)
        {
            global::BrightSkool.Domain.Models.CommonModel.Nationality result = new global::BrightSkool.Domain.Models.CommonModel.Nationality();
            global::Orleans.@Serialization.@DeserializationContext.@Current.@RecordObject(result);
            result.@CountryNationality = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@ISONationality = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            return (global::BrightSkool.Domain.Models.CommonModel.Nationality)result;
        }

        public static void Register()
        {
            global::Orleans.Serialization.SerializationManager.@Register(typeof (global::BrightSkool.Domain.Models.CommonModel.Nationality), DeepCopier, Serializer, Deserializer);
        }

        static OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_NationalitySerializer()
        {
            Register();
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.SerializerAttribute(typeof (global::BrightSkool.Domain.Models.CommonModel.PIN)), global::Orleans.CodeGeneration.RegisterSerializerAttribute]
    internal class OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_PINSerializer
    {
        [global::Orleans.CodeGeneration.CopierMethodAttribute]
        public static global::System.Object DeepCopier(global::System.Object original)
        {
            global::BrightSkool.Domain.Models.CommonModel.PIN input = ((global::BrightSkool.Domain.Models.CommonModel.PIN)original);
            global::BrightSkool.Domain.Models.CommonModel.PIN result = new global::BrightSkool.Domain.Models.CommonModel.PIN();
            global::Orleans.@Serialization.@SerializationContext.@Current.@RecordObject(original, result);
            result.@FederalDocumentType = input.@FederalDocumentType;
            result.@PersonalIDNumber = input.@PersonalIDNumber;
            return result;
        }

        [global::Orleans.CodeGeneration.SerializerMethodAttribute]
        public static void Serializer(global::System.Object untypedInput, global::Orleans.Serialization.BinaryTokenStreamWriter stream, global::System.Type expected)
        {
            global::BrightSkool.Domain.Models.CommonModel.PIN input = (global::BrightSkool.Domain.Models.CommonModel.PIN)untypedInput;
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@FederalDocumentType, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@PersonalIDNumber, stream, typeof (global::System.String));
        }

        [global::Orleans.CodeGeneration.DeserializerMethodAttribute]
        public static global::System.Object Deserializer(global::System.Type expected, global::Orleans.Serialization.BinaryTokenStreamReader stream)
        {
            global::BrightSkool.Domain.Models.CommonModel.PIN result = new global::BrightSkool.Domain.Models.CommonModel.PIN();
            global::Orleans.@Serialization.@DeserializationContext.@Current.@RecordObject(result);
            result.@FederalDocumentType = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@PersonalIDNumber = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            return (global::BrightSkool.Domain.Models.CommonModel.PIN)result;
        }

        public static void Register()
        {
            global::Orleans.Serialization.SerializationManager.@Register(typeof (global::BrightSkool.Domain.Models.CommonModel.PIN), DeepCopier, Serializer, Deserializer);
        }

        static OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_PINSerializer()
        {
            Register();
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.SerializerAttribute(typeof (global::BrightSkool.Domain.Models.CommonModel.Phone)), global::Orleans.CodeGeneration.RegisterSerializerAttribute]
    internal class OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_PhoneSerializer
    {
        [global::Orleans.CodeGeneration.CopierMethodAttribute]
        public static global::System.Object DeepCopier(global::System.Object original)
        {
            global::BrightSkool.Domain.Models.CommonModel.Phone input = ((global::BrightSkool.Domain.Models.CommonModel.Phone)original);
            global::BrightSkool.Domain.Models.CommonModel.Phone result = new global::BrightSkool.Domain.Models.CommonModel.Phone();
            global::Orleans.@Serialization.@SerializationContext.@Current.@RecordObject(original, result);
            result.@AreaCode = input.@AreaCode;
            result.@CountryCode = input.@CountryCode;
            result.@PhoneNumber = input.@PhoneNumber;
            return result;
        }

        [global::Orleans.CodeGeneration.SerializerMethodAttribute]
        public static void Serializer(global::System.Object untypedInput, global::Orleans.Serialization.BinaryTokenStreamWriter stream, global::System.Type expected)
        {
            global::BrightSkool.Domain.Models.CommonModel.Phone input = (global::BrightSkool.Domain.Models.CommonModel.Phone)untypedInput;
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@AreaCode, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@CountryCode, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@PhoneNumber, stream, typeof (global::System.String));
        }

        [global::Orleans.CodeGeneration.DeserializerMethodAttribute]
        public static global::System.Object Deserializer(global::System.Type expected, global::Orleans.Serialization.BinaryTokenStreamReader stream)
        {
            global::BrightSkool.Domain.Models.CommonModel.Phone result = new global::BrightSkool.Domain.Models.CommonModel.Phone();
            global::Orleans.@Serialization.@DeserializationContext.@Current.@RecordObject(result);
            result.@AreaCode = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@CountryCode = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@PhoneNumber = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            return (global::BrightSkool.Domain.Models.CommonModel.Phone)result;
        }

        public static void Register()
        {
            global::Orleans.Serialization.SerializationManager.@Register(typeof (global::BrightSkool.Domain.Models.CommonModel.Phone), DeepCopier, Serializer, Deserializer);
        }

        static OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_PhoneSerializer()
        {
            Register();
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.SerializerAttribute(typeof (global::BrightSkool.Domain.Models.CommonModel.Religion)), global::Orleans.CodeGeneration.RegisterSerializerAttribute]
    internal class OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_ReligionSerializer
    {
        [global::Orleans.CodeGeneration.CopierMethodAttribute]
        public static global::System.Object DeepCopier(global::System.Object original)
        {
            global::BrightSkool.Domain.Models.CommonModel.Religion input = ((global::BrightSkool.Domain.Models.CommonModel.Religion)original);
            global::BrightSkool.Domain.Models.CommonModel.Religion result = new global::BrightSkool.Domain.Models.CommonModel.Religion();
            global::Orleans.@Serialization.@SerializationContext.@Current.@RecordObject(original, result);
            result.@ReligionName = input.@ReligionName;
            return result;
        }

        [global::Orleans.CodeGeneration.SerializerMethodAttribute]
        public static void Serializer(global::System.Object untypedInput, global::Orleans.Serialization.BinaryTokenStreamWriter stream, global::System.Type expected)
        {
            global::BrightSkool.Domain.Models.CommonModel.Religion input = (global::BrightSkool.Domain.Models.CommonModel.Religion)untypedInput;
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@ReligionName, stream, typeof (global::System.String));
        }

        [global::Orleans.CodeGeneration.DeserializerMethodAttribute]
        public static global::System.Object Deserializer(global::System.Type expected, global::Orleans.Serialization.BinaryTokenStreamReader stream)
        {
            global::BrightSkool.Domain.Models.CommonModel.Religion result = new global::BrightSkool.Domain.Models.CommonModel.Religion();
            global::Orleans.@Serialization.@DeserializationContext.@Current.@RecordObject(result);
            result.@ReligionName = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            return (global::BrightSkool.Domain.Models.CommonModel.Religion)result;
        }

        public static void Register()
        {
            global::Orleans.Serialization.SerializationManager.@Register(typeof (global::BrightSkool.Domain.Models.CommonModel.Religion), DeepCopier, Serializer, Deserializer);
        }

        static OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_ReligionSerializer()
        {
            Register();
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.SerializerAttribute(typeof (global::BrightSkool.Domain.Models.ApplicationStatus)), global::Orleans.CodeGeneration.RegisterSerializerAttribute]
    internal class OrleansCodeGenBrightSkool_Domain_Models_ApplicationStatusSerializer
    {
        [global::Orleans.CodeGeneration.CopierMethodAttribute]
        public static global::System.Object DeepCopier(global::System.Object original)
        {
            global::BrightSkool.Domain.Models.ApplicationStatus input = ((global::BrightSkool.Domain.Models.ApplicationStatus)original);
            global::BrightSkool.Domain.Models.ApplicationStatus result = new global::BrightSkool.Domain.Models.ApplicationStatus();
            global::Orleans.@Serialization.@SerializationContext.@Current.@RecordObject(original, result);
            result.@Code = input.@Code;
            result.@Status = input.@Status;
            return result;
        }

        [global::Orleans.CodeGeneration.SerializerMethodAttribute]
        public static void Serializer(global::System.Object untypedInput, global::Orleans.Serialization.BinaryTokenStreamWriter stream, global::System.Type expected)
        {
            global::BrightSkool.Domain.Models.ApplicationStatus input = (global::BrightSkool.Domain.Models.ApplicationStatus)untypedInput;
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@Code, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@Status, stream, typeof (global::System.String));
        }

        [global::Orleans.CodeGeneration.DeserializerMethodAttribute]
        public static global::System.Object Deserializer(global::System.Type expected, global::Orleans.Serialization.BinaryTokenStreamReader stream)
        {
            global::BrightSkool.Domain.Models.ApplicationStatus result = new global::BrightSkool.Domain.Models.ApplicationStatus();
            global::Orleans.@Serialization.@DeserializationContext.@Current.@RecordObject(result);
            result.@Code = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@Status = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            return (global::BrightSkool.Domain.Models.ApplicationStatus)result;
        }

        public static void Register()
        {
            global::Orleans.Serialization.SerializationManager.@Register(typeof (global::BrightSkool.Domain.Models.ApplicationStatus), DeepCopier, Serializer, Deserializer);
        }

        static OrleansCodeGenBrightSkool_Domain_Models_ApplicationStatusSerializer()
        {
            Register();
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.SerializerAttribute(typeof (global::BrightSkool.Domain.Models.CommonModel.Address)), global::Orleans.CodeGeneration.RegisterSerializerAttribute]
    internal class OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_AddressSerializer
    {
        [global::Orleans.CodeGeneration.CopierMethodAttribute]
        public static global::System.Object DeepCopier(global::System.Object original)
        {
            global::BrightSkool.Domain.Models.CommonModel.Address input = ((global::BrightSkool.Domain.Models.CommonModel.Address)original);
            global::BrightSkool.Domain.Models.CommonModel.Address result = new global::BrightSkool.Domain.Models.CommonModel.Address();
            global::Orleans.@Serialization.@SerializationContext.@Current.@RecordObject(original, result);
            result.@AddressLine1 = input.@AddressLine1;
            result.@AddressLine2 = input.@AddressLine2;
            result.@AddressLine3 = input.@AddressLine3;
            result.@City = input.@City;
            result.@Country = input.@Country;
            result.@CountryDetail = (global::BrightSkool.Domain.Models.CommonModel.Country)global::Orleans.Serialization.SerializationManager.@DeepCopyInner(input.@CountryDetail);
            result.@State = input.@State;
            result.@ZipCode = input.@ZipCode;
            return result;
        }

        [global::Orleans.CodeGeneration.SerializerMethodAttribute]
        public static void Serializer(global::System.Object untypedInput, global::Orleans.Serialization.BinaryTokenStreamWriter stream, global::System.Type expected)
        {
            global::BrightSkool.Domain.Models.CommonModel.Address input = (global::BrightSkool.Domain.Models.CommonModel.Address)untypedInput;
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@AddressLine1, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@AddressLine2, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@AddressLine3, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@City, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@Country, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@CountryDetail, stream, typeof (global::BrightSkool.Domain.Models.CommonModel.Country));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@State, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@ZipCode, stream, typeof (global::System.String));
        }

        [global::Orleans.CodeGeneration.DeserializerMethodAttribute]
        public static global::System.Object Deserializer(global::System.Type expected, global::Orleans.Serialization.BinaryTokenStreamReader stream)
        {
            global::BrightSkool.Domain.Models.CommonModel.Address result = new global::BrightSkool.Domain.Models.CommonModel.Address();
            global::Orleans.@Serialization.@DeserializationContext.@Current.@RecordObject(result);
            result.@AddressLine1 = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@AddressLine2 = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@AddressLine3 = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@City = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@Country = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@CountryDetail = (global::BrightSkool.Domain.Models.CommonModel.Country)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::BrightSkool.Domain.Models.CommonModel.Country), stream);
            result.@State = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@ZipCode = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            return (global::BrightSkool.Domain.Models.CommonModel.Address)result;
        }

        public static void Register()
        {
            global::Orleans.Serialization.SerializationManager.@Register(typeof (global::BrightSkool.Domain.Models.CommonModel.Address), DeepCopier, Serializer, Deserializer);
        }

        static OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_AddressSerializer()
        {
            Register();
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.SerializerAttribute(typeof (global::BrightSkool.Domain.Models.CommonModel.Country)), global::Orleans.CodeGeneration.RegisterSerializerAttribute]
    internal class OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_CountrySerializer
    {
        [global::Orleans.CodeGeneration.CopierMethodAttribute]
        public static global::System.Object DeepCopier(global::System.Object original)
        {
            global::BrightSkool.Domain.Models.CommonModel.Country input = ((global::BrightSkool.Domain.Models.CommonModel.Country)original);
            global::BrightSkool.Domain.Models.CommonModel.Country result = new global::BrightSkool.Domain.Models.CommonModel.Country();
            global::Orleans.@Serialization.@SerializationContext.@Current.@RecordObject(original, result);
            result.@CountryCode = input.@CountryCode;
            result.@CountryCodeShort = input.@CountryCodeShort;
            result.@CountryName = input.@CountryName;
            result.@CountryStates = (global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.State>)global::Orleans.Serialization.SerializationManager.@DeepCopyInner(input.@CountryStates);
            return result;
        }

        [global::Orleans.CodeGeneration.SerializerMethodAttribute]
        public static void Serializer(global::System.Object untypedInput, global::Orleans.Serialization.BinaryTokenStreamWriter stream, global::System.Type expected)
        {
            global::BrightSkool.Domain.Models.CommonModel.Country input = (global::BrightSkool.Domain.Models.CommonModel.Country)untypedInput;
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@CountryCode, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@CountryCodeShort, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@CountryName, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@CountryStates, stream, typeof (global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.State>));
        }

        [global::Orleans.CodeGeneration.DeserializerMethodAttribute]
        public static global::System.Object Deserializer(global::System.Type expected, global::Orleans.Serialization.BinaryTokenStreamReader stream)
        {
            global::BrightSkool.Domain.Models.CommonModel.Country result = new global::BrightSkool.Domain.Models.CommonModel.Country();
            global::Orleans.@Serialization.@DeserializationContext.@Current.@RecordObject(result);
            result.@CountryCode = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@CountryCodeShort = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@CountryName = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@CountryStates = (global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.State>)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.State>), stream);
            return (global::BrightSkool.Domain.Models.CommonModel.Country)result;
        }

        public static void Register()
        {
            global::Orleans.Serialization.SerializationManager.@Register(typeof (global::BrightSkool.Domain.Models.CommonModel.Country), DeepCopier, Serializer, Deserializer);
        }

        static OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_CountrySerializer()
        {
            Register();
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.SerializerAttribute(typeof (global::BrightSkool.Domain.Models.CommonModel.State)), global::Orleans.CodeGeneration.RegisterSerializerAttribute]
    internal class OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_StateSerializer
    {
        [global::Orleans.CodeGeneration.CopierMethodAttribute]
        public static global::System.Object DeepCopier(global::System.Object original)
        {
            global::BrightSkool.Domain.Models.CommonModel.State input = ((global::BrightSkool.Domain.Models.CommonModel.State)original);
            global::BrightSkool.Domain.Models.CommonModel.State result = new global::BrightSkool.Domain.Models.CommonModel.State();
            global::Orleans.@Serialization.@SerializationContext.@Current.@RecordObject(original, result);
            result.@CountryName = input.@CountryName;
            result.@StateCity = (global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.City>)global::Orleans.Serialization.SerializationManager.@DeepCopyInner(input.@StateCity);
            result.@StateName = input.@StateName;
            return result;
        }

        [global::Orleans.CodeGeneration.SerializerMethodAttribute]
        public static void Serializer(global::System.Object untypedInput, global::Orleans.Serialization.BinaryTokenStreamWriter stream, global::System.Type expected)
        {
            global::BrightSkool.Domain.Models.CommonModel.State input = (global::BrightSkool.Domain.Models.CommonModel.State)untypedInput;
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@CountryName, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@StateCity, stream, typeof (global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.City>));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@StateName, stream, typeof (global::System.String));
        }

        [global::Orleans.CodeGeneration.DeserializerMethodAttribute]
        public static global::System.Object Deserializer(global::System.Type expected, global::Orleans.Serialization.BinaryTokenStreamReader stream)
        {
            global::BrightSkool.Domain.Models.CommonModel.State result = new global::BrightSkool.Domain.Models.CommonModel.State();
            global::Orleans.@Serialization.@DeserializationContext.@Current.@RecordObject(result);
            result.@CountryName = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@StateCity = (global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.City>)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.City>), stream);
            result.@StateName = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            return (global::BrightSkool.Domain.Models.CommonModel.State)result;
        }

        public static void Register()
        {
            global::Orleans.Serialization.SerializationManager.@Register(typeof (global::BrightSkool.Domain.Models.CommonModel.State), DeepCopier, Serializer, Deserializer);
        }

        static OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_StateSerializer()
        {
            Register();
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.SerializerAttribute(typeof (global::BrightSkool.Domain.Models.CommonModel.City)), global::Orleans.CodeGeneration.RegisterSerializerAttribute]
    internal class OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_CitySerializer
    {
        [global::Orleans.CodeGeneration.CopierMethodAttribute]
        public static global::System.Object DeepCopier(global::System.Object original)
        {
            global::BrightSkool.Domain.Models.CommonModel.City input = ((global::BrightSkool.Domain.Models.CommonModel.City)original);
            global::BrightSkool.Domain.Models.CommonModel.City result = new global::BrightSkool.Domain.Models.CommonModel.City();
            global::Orleans.@Serialization.@SerializationContext.@Current.@RecordObject(original, result);
            result.@CityName = input.@CityName;
            result.@StateName = input.@StateName;
            return result;
        }

        [global::Orleans.CodeGeneration.SerializerMethodAttribute]
        public static void Serializer(global::System.Object untypedInput, global::Orleans.Serialization.BinaryTokenStreamWriter stream, global::System.Type expected)
        {
            global::BrightSkool.Domain.Models.CommonModel.City input = (global::BrightSkool.Domain.Models.CommonModel.City)untypedInput;
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@CityName, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@StateName, stream, typeof (global::System.String));
        }

        [global::Orleans.CodeGeneration.DeserializerMethodAttribute]
        public static global::System.Object Deserializer(global::System.Type expected, global::Orleans.Serialization.BinaryTokenStreamReader stream)
        {
            global::BrightSkool.Domain.Models.CommonModel.City result = new global::BrightSkool.Domain.Models.CommonModel.City();
            global::Orleans.@Serialization.@DeserializationContext.@Current.@RecordObject(result);
            result.@CityName = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@StateName = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            return (global::BrightSkool.Domain.Models.CommonModel.City)result;
        }

        public static void Register()
        {
            global::Orleans.Serialization.SerializationManager.@Register(typeof (global::BrightSkool.Domain.Models.CommonModel.City), DeepCopier, Serializer, Deserializer);
        }

        static OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_CitySerializer()
        {
            Register();
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.SerializerAttribute(typeof (global::BrightSkool.Domain.Models.StudentDocuments)), global::Orleans.CodeGeneration.RegisterSerializerAttribute]
    internal class OrleansCodeGenBrightSkool_Domain_Models_StudentDocumentsSerializer
    {
        [global::Orleans.CodeGeneration.CopierMethodAttribute]
        public static global::System.Object DeepCopier(global::System.Object original)
        {
            global::BrightSkool.Domain.Models.StudentDocuments input = ((global::BrightSkool.Domain.Models.StudentDocuments)original);
            global::BrightSkool.Domain.Models.StudentDocuments result = new global::BrightSkool.Domain.Models.StudentDocuments();
            global::Orleans.@Serialization.@SerializationContext.@Current.@RecordObject(original, result);
            result.@FilePath = input.@FilePath;
            result.@Name = input.@Name;
            return result;
        }

        [global::Orleans.CodeGeneration.SerializerMethodAttribute]
        public static void Serializer(global::System.Object untypedInput, global::Orleans.Serialization.BinaryTokenStreamWriter stream, global::System.Type expected)
        {
            global::BrightSkool.Domain.Models.StudentDocuments input = (global::BrightSkool.Domain.Models.StudentDocuments)untypedInput;
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@FilePath, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@Name, stream, typeof (global::System.String));
        }

        [global::Orleans.CodeGeneration.DeserializerMethodAttribute]
        public static global::System.Object Deserializer(global::System.Type expected, global::Orleans.Serialization.BinaryTokenStreamReader stream)
        {
            global::BrightSkool.Domain.Models.StudentDocuments result = new global::BrightSkool.Domain.Models.StudentDocuments();
            global::Orleans.@Serialization.@DeserializationContext.@Current.@RecordObject(result);
            result.@FilePath = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@Name = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            return (global::BrightSkool.Domain.Models.StudentDocuments)result;
        }

        public static void Register()
        {
            global::Orleans.Serialization.SerializationManager.@Register(typeof (global::BrightSkool.Domain.Models.StudentDocuments), DeepCopier, Serializer, Deserializer);
        }

        static OrleansCodeGenBrightSkool_Domain_Models_StudentDocumentsSerializer()
        {
            Register();
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.SerializableAttribute, global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.GrainReferenceAttribute(typeof (global::BrightSkool.GrainInterface.Registration.IRegistrationAggregateGrain))]
    internal class OrleansCodeGenRegistrationAggregateGrainReference : global::Orleans.Runtime.GrainReference, global::BrightSkool.GrainInterface.Registration.IRegistrationAggregateGrain
    {
        protected @OrleansCodeGenRegistrationAggregateGrainReference(global::Orleans.Runtime.GrainReference @other): base (@other)
        {
        }

        protected @OrleansCodeGenRegistrationAggregateGrainReference(global::System.Runtime.Serialization.SerializationInfo @info, global::System.Runtime.Serialization.StreamingContext @context): base (@info, @context)
        {
        }

        protected override global::System.Int32 InterfaceId
        {
            get
            {
                return -81878734;
            }
        }

        public override global::System.String InterfaceName
        {
            get
            {
                return "global::BrightSkool.GrainInterface.Registration.IRegistrationAggregateGrain";
            }
        }

        public override global::System.Boolean @IsCompatible(global::System.Int32 @interfaceId)
        {
            return @interfaceId == -81878734 || @interfaceId == -2075186181 || @interfaceId == -655805272 || @interfaceId == -1277021679;
        }

        protected override global::System.String @GetMethodName(global::System.Int32 @interfaceId, global::System.Int32 @methodId)
        {
            switch (@interfaceId)
            {
                case -81878734:
                    switch (@methodId)
                    {
                        case 884161819:
                            return "GetAggregateValue";
                        case -525452160:
                            return "GetAggregateValues";
                        case -1527409266:
                            return "RegisterGrain";
                        case -867620834:
                            return "GetRegisteredGrains";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -81878734 + ",methodId=" + @methodId);
                    }

                case -2075186181:
                    switch (@methodId)
                    {
                        case 884161819:
                            return "GetAggregateValue";
                        case -525452160:
                            return "GetAggregateValues";
                        case -1527409266:
                            return "RegisterGrain";
                        case -867620834:
                            return "GetRegisteredGrains";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -2075186181 + ",methodId=" + @methodId);
                    }

                case -655805272:
                    switch (@methodId)
                    {
                        case -1527409266:
                            return "RegisterGrain";
                        case -867620834:
                            return "GetRegisteredGrains";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -655805272 + ",methodId=" + @methodId);
                    }

                case -1277021679:
                    switch (@methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + @methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + @interfaceId);
            }
        }

        public global::System.Threading.Tasks.Task<global::Patterns.EventSourcing.Interface.TimestampedValue<global::BrightSkool.GrainInterface.Registration.RegistrationAggregate>> @GetAggregateValue()
        {
            return base.@InvokeMethodAsync<global::Patterns.EventSourcing.Interface.TimestampedValue<global::BrightSkool.GrainInterface.Registration.RegistrationAggregate>>(884161819, null);
        }

        public global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::BrightSkool.GrainInterface.Registration.RegistrationState>> @GetAggregateValues()
        {
            return base.@InvokeMethodAsync<global::System.Collections.Generic.List<global::BrightSkool.GrainInterface.Registration.RegistrationState>>(-525452160, null);
        }

        public global::System.Threading.Tasks.Task<global::BrightSkool.GrainInterface.Registration.IRegistration> @RegisterGrain(global::BrightSkool.GrainInterface.Registration.IRegistration @item)
        {
            return base.@InvokeMethodAsync<global::BrightSkool.GrainInterface.Registration.IRegistration>(-1527409266, new global::System.Object[]{@item is global::Orleans.Grain ? @item.@AsReference<global::BrightSkool.GrainInterface.Registration.IRegistration>() : @item});
        }

        public global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::BrightSkool.GrainInterface.Registration.IRegistration>> @GetRegisteredGrains()
        {
            return base.@InvokeMethodAsync<global::System.Collections.Generic.List<global::BrightSkool.GrainInterface.Registration.IRegistration>>(-867620834, null);
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::Orleans.CodeGeneration.MethodInvokerAttribute("global::BrightSkool.GrainInterface.Registration.IRegistrationAggregateGrain", -81878734, typeof (global::BrightSkool.GrainInterface.Registration.IRegistrationAggregateGrain)), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute]
    internal class OrleansCodeGenRegistrationAggregateGrainMethodInvoker : global::Orleans.CodeGeneration.IGrainMethodInvoker
    {
        public global::System.Threading.Tasks.Task<global::System.Object> @Invoke(global::Orleans.Runtime.IAddressable @grain, global::Orleans.CodeGeneration.InvokeMethodRequest @request)
        {
            global::System.Int32 interfaceId = @request.@InterfaceId;
            global::System.Int32 methodId = @request.@MethodId;
            global::System.Object[] arguments = @request.@Arguments;
            if (@grain == null)
                throw new global::System.ArgumentNullException("grain");
            switch (interfaceId)
            {
                case -81878734:
                    switch (methodId)
                    {
                        case 884161819:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistrationAggregateGrain)@grain).@GetAggregateValue().@Box();
                        case -525452160:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistrationAggregateGrain)@grain).@GetAggregateValues().@Box();
                        case -1527409266:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistrationAggregateGrain)@grain).@RegisterGrain((global::BrightSkool.GrainInterface.Registration.IRegistration)arguments[0]).@Box();
                        case -867620834:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistrationAggregateGrain)@grain).@GetRegisteredGrains().@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -81878734 + ",methodId=" + methodId);
                    }

                case -2075186181:
                    switch (methodId)
                    {
                        case 884161819:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistrationAggregateGrain)@grain).@GetAggregateValue().@Box();
                        case -525452160:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistrationAggregateGrain)@grain).@GetAggregateValues().@Box();
                        case -1527409266:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistrationAggregateGrain)@grain).@RegisterGrain((global::BrightSkool.GrainInterface.Registration.IRegistration)arguments[0]).@Box();
                        case -867620834:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistrationAggregateGrain)@grain).@GetRegisteredGrains().@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -2075186181 + ",methodId=" + methodId);
                    }

                case -655805272:
                    switch (methodId)
                    {
                        case -1527409266:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistrationAggregateGrain)@grain).@RegisterGrain((global::BrightSkool.GrainInterface.Registration.IRegistration)arguments[0]).@Box();
                        case -867620834:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistrationAggregateGrain)@grain).@GetRegisteredGrains().@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -655805272 + ",methodId=" + methodId);
                    }

                case -1277021679:
                    switch (methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + interfaceId);
            }
        }

        public global::System.Int32 InterfaceId
        {
            get
            {
                return -81878734;
            }
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.SerializerAttribute(typeof (global::BrightSkool.GrainInterface.Registration.RegistrationAggregate)), global::Orleans.CodeGeneration.RegisterSerializerAttribute]
    internal class OrleansCodeGenBrightSkool_GrainInterface_Registration_RegistrationAggregateSerializer
    {
        private static readonly global::System.Reflection.FieldInfo field0 = typeof (global::BrightSkool.GrainInterface.Registration.RegistrationAggregate).@GetTypeInfo().@GetField("_status", (System.@Reflection.@BindingFlags.@Instance | System.@Reflection.@BindingFlags.@NonPublic | System.@Reflection.@BindingFlags.@Public));
        private static readonly global::System.Func<global::BrightSkool.GrainInterface.Registration.RegistrationAggregate, global::System.String> getField0 = (global::System.Func<global::BrightSkool.GrainInterface.Registration.RegistrationAggregate, global::System.String>)global::Orleans.Serialization.SerializationManager.@GetGetter(field0);
        private static readonly global::System.Action<global::BrightSkool.GrainInterface.Registration.RegistrationAggregate, global::System.String> setField0 = (global::System.Action<global::BrightSkool.GrainInterface.Registration.RegistrationAggregate, global::System.String>)global::Orleans.Serialization.SerializationManager.@GetReferenceSetter(field0);
        [global::Orleans.CodeGeneration.CopierMethodAttribute]
        public static global::System.Object DeepCopier(global::System.Object original)
        {
            global::BrightSkool.GrainInterface.Registration.RegistrationAggregate input = ((global::BrightSkool.GrainInterface.Registration.RegistrationAggregate)original);
            global::BrightSkool.GrainInterface.Registration.RegistrationAggregate result = new global::BrightSkool.GrainInterface.Registration.RegistrationAggregate();
            global::Orleans.@Serialization.@SerializationContext.@Current.@RecordObject(original, result);
            result.@_registrationModel = (global::BrightSkool.Domain.Models.RegistrationModel)global::Orleans.Serialization.SerializationManager.@DeepCopyInner(input.@_registrationModel);
            setField0(result, getField0(input));
            return result;
        }

        [global::Orleans.CodeGeneration.SerializerMethodAttribute]
        public static void Serializer(global::System.Object untypedInput, global::Orleans.Serialization.BinaryTokenStreamWriter stream, global::System.Type expected)
        {
            global::BrightSkool.GrainInterface.Registration.RegistrationAggregate input = (global::BrightSkool.GrainInterface.Registration.RegistrationAggregate)untypedInput;
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@_registrationModel, stream, typeof (global::BrightSkool.Domain.Models.RegistrationModel));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(getField0(input), stream, typeof (global::System.String));
        }

        [global::Orleans.CodeGeneration.DeserializerMethodAttribute]
        public static global::System.Object Deserializer(global::System.Type expected, global::Orleans.Serialization.BinaryTokenStreamReader stream)
        {
            global::BrightSkool.GrainInterface.Registration.RegistrationAggregate result = new global::BrightSkool.GrainInterface.Registration.RegistrationAggregate();
            global::Orleans.@Serialization.@DeserializationContext.@Current.@RecordObject(result);
            result.@_registrationModel = (global::BrightSkool.Domain.Models.RegistrationModel)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::BrightSkool.Domain.Models.RegistrationModel), stream);
            setField0(result, (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream));
            return (global::BrightSkool.GrainInterface.Registration.RegistrationAggregate)result;
        }

        public static void Register()
        {
            global::Orleans.Serialization.SerializationManager.@Register(typeof (global::BrightSkool.GrainInterface.Registration.RegistrationAggregate), DeepCopier, Serializer, Deserializer);
        }

        static OrleansCodeGenBrightSkool_GrainInterface_Registration_RegistrationAggregateSerializer()
        {
            Register();
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.SerializableAttribute, global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.GrainReferenceAttribute(typeof (global::BrightSkool.GrainInterface.Registration.IRegistrationRegistryGrain))]
    internal class OrleansCodeGenRegistrationRegistryGrainReference : global::Orleans.Runtime.GrainReference, global::BrightSkool.GrainInterface.Registration.IRegistrationRegistryGrain
    {
        protected @OrleansCodeGenRegistrationRegistryGrainReference(global::Orleans.Runtime.GrainReference @other): base (@other)
        {
        }

        protected @OrleansCodeGenRegistrationRegistryGrainReference(global::System.Runtime.Serialization.SerializationInfo @info, global::System.Runtime.Serialization.StreamingContext @context): base (@info, @context)
        {
        }

        protected override global::System.Int32 InterfaceId
        {
            get
            {
                return 1156662441;
            }
        }

        public override global::System.String InterfaceName
        {
            get
            {
                return "global::BrightSkool.GrainInterface.Registration.IRegistrationRegistryGrain";
            }
        }

        public override global::System.Boolean @IsCompatible(global::System.Int32 @interfaceId)
        {
            return @interfaceId == 1156662441 || @interfaceId == 1483676406 || @interfaceId == -1277021679;
        }

        protected override global::System.String @GetMethodName(global::System.Int32 @interfaceId, global::System.Int32 @methodId)
        {
            switch (@interfaceId)
            {
                case 1156662441:
                    switch (@methodId)
                    {
                        case -1057953460:
                            return "RegisterGrain";
                        case -867620834:
                            return "GetRegisteredGrains";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 1156662441 + ",methodId=" + @methodId);
                    }

                case 1483676406:
                    switch (@methodId)
                    {
                        case -1057953460:
                            return "RegisterGrain";
                        case -867620834:
                            return "GetRegisteredGrains";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 1483676406 + ",methodId=" + @methodId);
                    }

                case -1277021679:
                    switch (@methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + @methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + @interfaceId);
            }
        }

        public global::System.Threading.Tasks.Task<global::BrightSkool.GrainInterface.Registration.IRegistrationItemGrain> @RegisterGrain(global::BrightSkool.GrainInterface.Registration.IRegistrationItemGrain @item)
        {
            return base.@InvokeMethodAsync<global::BrightSkool.GrainInterface.Registration.IRegistrationItemGrain>(-1057953460, new global::System.Object[]{@item is global::Orleans.Grain ? @item.@AsReference<global::BrightSkool.GrainInterface.Registration.IRegistrationItemGrain>() : @item});
        }

        public global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::BrightSkool.GrainInterface.Registration.IRegistrationItemGrain>> @GetRegisteredGrains()
        {
            return base.@InvokeMethodAsync<global::System.Collections.Generic.List<global::BrightSkool.GrainInterface.Registration.IRegistrationItemGrain>>(-867620834, null);
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::Orleans.CodeGeneration.MethodInvokerAttribute("global::BrightSkool.GrainInterface.Registration.IRegistrationRegistryGrain", 1156662441, typeof (global::BrightSkool.GrainInterface.Registration.IRegistrationRegistryGrain)), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute]
    internal class OrleansCodeGenRegistrationRegistryGrainMethodInvoker : global::Orleans.CodeGeneration.IGrainMethodInvoker
    {
        public global::System.Threading.Tasks.Task<global::System.Object> @Invoke(global::Orleans.Runtime.IAddressable @grain, global::Orleans.CodeGeneration.InvokeMethodRequest @request)
        {
            global::System.Int32 interfaceId = @request.@InterfaceId;
            global::System.Int32 methodId = @request.@MethodId;
            global::System.Object[] arguments = @request.@Arguments;
            if (@grain == null)
                throw new global::System.ArgumentNullException("grain");
            switch (interfaceId)
            {
                case 1156662441:
                    switch (methodId)
                    {
                        case -1057953460:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistrationRegistryGrain)@grain).@RegisterGrain((global::BrightSkool.GrainInterface.Registration.IRegistrationItemGrain)arguments[0]).@Box();
                        case -867620834:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistrationRegistryGrain)@grain).@GetRegisteredGrains().@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 1156662441 + ",methodId=" + methodId);
                    }

                case 1483676406:
                    switch (methodId)
                    {
                        case -1057953460:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistrationRegistryGrain)@grain).@RegisterGrain((global::BrightSkool.GrainInterface.Registration.IRegistrationItemGrain)arguments[0]).@Box();
                        case -867620834:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistrationRegistryGrain)@grain).@GetRegisteredGrains().@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 1483676406 + ",methodId=" + methodId);
                    }

                case -1277021679:
                    switch (methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + interfaceId);
            }
        }

        public global::System.Int32 InterfaceId
        {
            get
            {
                return 1156662441;
            }
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.SerializableAttribute, global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.GrainReferenceAttribute(typeof (global::BrightSkool.GrainInterface.Registration.IRegistrationItemGrain))]
    internal class OrleansCodeGenRegistrationItemGrainReference : global::Orleans.Runtime.GrainReference, global::BrightSkool.GrainInterface.Registration.IRegistrationItemGrain
    {
        protected @OrleansCodeGenRegistrationItemGrainReference(global::Orleans.Runtime.GrainReference @other): base (@other)
        {
        }

        protected @OrleansCodeGenRegistrationItemGrainReference(global::System.Runtime.Serialization.SerializationInfo @info, global::System.Runtime.Serialization.StreamingContext @context): base (@info, @context)
        {
        }

        protected override global::System.Int32 InterfaceId
        {
            get
            {
                return 1685351385;
            }
        }

        public override global::System.String InterfaceName
        {
            get
            {
                return "global::BrightSkool.GrainInterface.Registration.IRegistrationItemGrain";
            }
        }

        public override global::System.Boolean @IsCompatible(global::System.Int32 @interfaceId)
        {
            return @interfaceId == 1685351385 || @interfaceId == -775899969;
        }

        protected override global::System.String @GetMethodName(global::System.Int32 @interfaceId, global::System.Int32 @methodId)
        {
            switch (@interfaceId)
            {
                case 1685351385:
                    switch (@methodId)
                    {
                        case 2058492049:
                            return "GetItem";
                        case -292788372:
                            return "SetItem";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 1685351385 + ",methodId=" + @methodId);
                    }

                case -775899969:
                    switch (@methodId)
                    {
                        case 2058492049:
                            return "GetItem";
                        case -292788372:
                            return "SetItem";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -775899969 + ",methodId=" + @methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + @interfaceId);
            }
        }

        public global::System.Threading.Tasks.Task<global::BrightSkool.GrainInterface.Registration.RegistrationCatalogItem> @GetItem()
        {
            return base.@InvokeMethodAsync<global::BrightSkool.GrainInterface.Registration.RegistrationCatalogItem>(2058492049, null);
        }

        public global::System.Threading.Tasks.Task<global::BrightSkool.GrainInterface.Registration.RegistrationCatalogItem> @SetItem(global::BrightSkool.GrainInterface.Registration.RegistrationCatalogItem @item)
        {
            return base.@InvokeMethodAsync<global::BrightSkool.GrainInterface.Registration.RegistrationCatalogItem>(-292788372, new global::System.Object[]{@item});
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::Orleans.CodeGeneration.MethodInvokerAttribute("global::BrightSkool.GrainInterface.Registration.IRegistrationItemGrain", 1685351385, typeof (global::BrightSkool.GrainInterface.Registration.IRegistrationItemGrain)), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute]
    internal class OrleansCodeGenRegistrationItemGrainMethodInvoker : global::Orleans.CodeGeneration.IGrainMethodInvoker
    {
        public global::System.Threading.Tasks.Task<global::System.Object> @Invoke(global::Orleans.Runtime.IAddressable @grain, global::Orleans.CodeGeneration.InvokeMethodRequest @request)
        {
            global::System.Int32 interfaceId = @request.@InterfaceId;
            global::System.Int32 methodId = @request.@MethodId;
            global::System.Object[] arguments = @request.@Arguments;
            if (@grain == null)
                throw new global::System.ArgumentNullException("grain");
            switch (interfaceId)
            {
                case 1685351385:
                    switch (methodId)
                    {
                        case 2058492049:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistrationItemGrain)@grain).@GetItem().@Box();
                        case -292788372:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistrationItemGrain)@grain).@SetItem((global::BrightSkool.GrainInterface.Registration.RegistrationCatalogItem)arguments[0]).@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 1685351385 + ",methodId=" + methodId);
                    }

                case -775899969:
                    switch (methodId)
                    {
                        case 2058492049:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistrationItemGrain)@grain).@GetItem().@Box();
                        case -292788372:
                            return ((global::BrightSkool.GrainInterface.Registration.IRegistrationItemGrain)@grain).@SetItem((global::BrightSkool.GrainInterface.Registration.RegistrationCatalogItem)arguments[0]).@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -775899969 + ",methodId=" + methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + interfaceId);
            }
        }

        public global::System.Int32 InterfaceId
        {
            get
            {
                return 1685351385;
            }
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.SerializerAttribute(typeof (global::BrightSkool.GrainInterface.Registration.RegistrationCatalogItem)), global::Orleans.CodeGeneration.RegisterSerializerAttribute]
    internal class OrleansCodeGenBrightSkool_GrainInterface_Registration_RegistrationCatalogItemSerializer
    {
        [global::Orleans.CodeGeneration.CopierMethodAttribute]
        public static global::System.Object DeepCopier(global::System.Object original)
        {
            return original;
        }

        [global::Orleans.CodeGeneration.SerializerMethodAttribute]
        public static void Serializer(global::System.Object untypedInput, global::Orleans.Serialization.BinaryTokenStreamWriter stream, global::System.Type expected)
        {
            global::BrightSkool.GrainInterface.Registration.RegistrationCatalogItem input = (global::BrightSkool.GrainInterface.Registration.RegistrationCatalogItem)untypedInput;
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@Id, stream, typeof (global::System.Guid));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@Nationality, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@SiblingId, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@Status, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@StudentName, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@standard, stream, typeof (global::System.String));
        }

        [global::Orleans.CodeGeneration.DeserializerMethodAttribute]
        public static global::System.Object Deserializer(global::System.Type expected, global::Orleans.Serialization.BinaryTokenStreamReader stream)
        {
            global::BrightSkool.GrainInterface.Registration.RegistrationCatalogItem result = new global::BrightSkool.GrainInterface.Registration.RegistrationCatalogItem();
            global::Orleans.@Serialization.@DeserializationContext.@Current.@RecordObject(result);
            result.@Id = (global::System.Guid)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.Guid), stream);
            result.@Nationality = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@SiblingId = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@Status = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@StudentName = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@standard = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            return (global::BrightSkool.GrainInterface.Registration.RegistrationCatalogItem)result;
        }

        public static void Register()
        {
            global::Orleans.Serialization.SerializationManager.@Register(typeof (global::BrightSkool.GrainInterface.Registration.RegistrationCatalogItem), DeepCopier, Serializer, Deserializer);
        }

        static OrleansCodeGenBrightSkool_GrainInterface_Registration_RegistrationCatalogItemSerializer()
        {
            Register();
        }
    }
}

namespace BrightSkool.GrainInterface.Masters
{
    using global::Orleans.Async;
    using global::Orleans;
    using global::System.Reflection;

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.SerializableAttribute, global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.GrainReferenceAttribute(typeof (global::BrightSkool.GrainInterface.Masters.IApplicationStatus))]
    internal class OrleansCodeGenApplicationStatusReference : global::Orleans.Runtime.GrainReference, global::BrightSkool.GrainInterface.Masters.IApplicationStatus
    {
        protected @OrleansCodeGenApplicationStatusReference(global::Orleans.Runtime.GrainReference @other): base (@other)
        {
        }

        protected @OrleansCodeGenApplicationStatusReference(global::System.Runtime.Serialization.SerializationInfo @info, global::System.Runtime.Serialization.StreamingContext @context): base (@info, @context)
        {
        }

        protected override global::System.Int32 InterfaceId
        {
            get
            {
                return -1813035842;
            }
        }

        public override global::System.String InterfaceName
        {
            get
            {
                return "global::BrightSkool.GrainInterface.Masters.IApplicationStatus";
            }
        }

        public override global::System.Boolean @IsCompatible(global::System.Int32 @interfaceId)
        {
            return @interfaceId == -1813035842 || @interfaceId == 526204983 || @interfaceId == -1277021679;
        }

        protected override global::System.String @GetMethodName(global::System.Int32 @interfaceId, global::System.Int32 @methodId)
        {
            switch (@interfaceId)
            {
                case -1813035842:
                    switch (@methodId)
                    {
                        case 2058492049:
                            return "GetItem";
                        case 949196431:
                            return "SetItem";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1813035842 + ",methodId=" + @methodId);
                    }

                case 526204983:
                    switch (@methodId)
                    {
                        case 2058492049:
                            return "GetItem";
                        case 949196431:
                            return "SetItem";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 526204983 + ",methodId=" + @methodId);
                    }

                case -1277021679:
                    switch (@methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + @methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + @interfaceId);
            }
        }

        public global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.ApplicationStatus>> @GetItem()
        {
            return base.@InvokeMethodAsync<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.ApplicationStatus>>(2058492049, null);
        }

        public global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.ApplicationStatus>> @SetItem(global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.ApplicationStatus> @item)
        {
            return base.@InvokeMethodAsync<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.ApplicationStatus>>(949196431, new global::System.Object[]{@item});
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::Orleans.CodeGeneration.MethodInvokerAttribute("global::BrightSkool.GrainInterface.Masters.IApplicationStatus", -1813035842, typeof (global::BrightSkool.GrainInterface.Masters.IApplicationStatus)), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute]
    internal class OrleansCodeGenApplicationStatusMethodInvoker : global::Orleans.CodeGeneration.IGrainMethodInvoker
    {
        public global::System.Threading.Tasks.Task<global::System.Object> @Invoke(global::Orleans.Runtime.IAddressable @grain, global::Orleans.CodeGeneration.InvokeMethodRequest @request)
        {
            global::System.Int32 interfaceId = @request.@InterfaceId;
            global::System.Int32 methodId = @request.@MethodId;
            global::System.Object[] arguments = @request.@Arguments;
            if (@grain == null)
                throw new global::System.ArgumentNullException("grain");
            switch (interfaceId)
            {
                case -1813035842:
                    switch (methodId)
                    {
                        case 2058492049:
                            return ((global::BrightSkool.GrainInterface.Masters.IApplicationStatus)@grain).@GetItem().@Box();
                        case 949196431:
                            return ((global::BrightSkool.GrainInterface.Masters.IApplicationStatus)@grain).@SetItem((global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.ApplicationStatus>)arguments[0]).@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1813035842 + ",methodId=" + methodId);
                    }

                case 526204983:
                    switch (methodId)
                    {
                        case 2058492049:
                            return ((global::BrightSkool.GrainInterface.Masters.IApplicationStatus)@grain).@GetItem().@Box();
                        case 949196431:
                            return ((global::BrightSkool.GrainInterface.Masters.IApplicationStatus)@grain).@SetItem((global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.ApplicationStatus>)arguments[0]).@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 526204983 + ",methodId=" + methodId);
                    }

                case -1277021679:
                    switch (methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + interfaceId);
            }
        }

        public global::System.Int32 InterfaceId
        {
            get
            {
                return -1813035842;
            }
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.SerializableAttribute, global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.GrainReferenceAttribute(typeof (global::BrightSkool.GrainInterface.Masters.ICountry))]
    internal class OrleansCodeGenCountryReference : global::Orleans.Runtime.GrainReference, global::BrightSkool.GrainInterface.Masters.ICountry
    {
        protected @OrleansCodeGenCountryReference(global::Orleans.Runtime.GrainReference @other): base (@other)
        {
        }

        protected @OrleansCodeGenCountryReference(global::System.Runtime.Serialization.SerializationInfo @info, global::System.Runtime.Serialization.StreamingContext @context): base (@info, @context)
        {
        }

        protected override global::System.Int32 InterfaceId
        {
            get
            {
                return 1044054890;
            }
        }

        public override global::System.String InterfaceName
        {
            get
            {
                return "global::BrightSkool.GrainInterface.Masters.ICountry";
            }
        }

        public override global::System.Boolean @IsCompatible(global::System.Int32 @interfaceId)
        {
            return @interfaceId == 1044054890 || @interfaceId == 1610156508 || @interfaceId == -1277021679;
        }

        protected override global::System.String @GetMethodName(global::System.Int32 @interfaceId, global::System.Int32 @methodId)
        {
            switch (@interfaceId)
            {
                case 1044054890:
                    switch (@methodId)
                    {
                        case 2058492049:
                            return "GetItem";
                        case -223071493:
                            return "SetItem";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 1044054890 + ",methodId=" + @methodId);
                    }

                case 1610156508:
                    switch (@methodId)
                    {
                        case 2058492049:
                            return "GetItem";
                        case -223071493:
                            return "SetItem";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 1610156508 + ",methodId=" + @methodId);
                    }

                case -1277021679:
                    switch (@methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + @methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + @interfaceId);
            }
        }

        public global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Country>> @GetItem()
        {
            return base.@InvokeMethodAsync<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Country>>(2058492049, null);
        }

        public global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Country>> @SetItem(global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Country> @item)
        {
            return base.@InvokeMethodAsync<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Country>>(-223071493, new global::System.Object[]{@item});
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::Orleans.CodeGeneration.MethodInvokerAttribute("global::BrightSkool.GrainInterface.Masters.ICountry", 1044054890, typeof (global::BrightSkool.GrainInterface.Masters.ICountry)), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute]
    internal class OrleansCodeGenCountryMethodInvoker : global::Orleans.CodeGeneration.IGrainMethodInvoker
    {
        public global::System.Threading.Tasks.Task<global::System.Object> @Invoke(global::Orleans.Runtime.IAddressable @grain, global::Orleans.CodeGeneration.InvokeMethodRequest @request)
        {
            global::System.Int32 interfaceId = @request.@InterfaceId;
            global::System.Int32 methodId = @request.@MethodId;
            global::System.Object[] arguments = @request.@Arguments;
            if (@grain == null)
                throw new global::System.ArgumentNullException("grain");
            switch (interfaceId)
            {
                case 1044054890:
                    switch (methodId)
                    {
                        case 2058492049:
                            return ((global::BrightSkool.GrainInterface.Masters.ICountry)@grain).@GetItem().@Box();
                        case -223071493:
                            return ((global::BrightSkool.GrainInterface.Masters.ICountry)@grain).@SetItem((global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Country>)arguments[0]).@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 1044054890 + ",methodId=" + methodId);
                    }

                case 1610156508:
                    switch (methodId)
                    {
                        case 2058492049:
                            return ((global::BrightSkool.GrainInterface.Masters.ICountry)@grain).@GetItem().@Box();
                        case -223071493:
                            return ((global::BrightSkool.GrainInterface.Masters.ICountry)@grain).@SetItem((global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Country>)arguments[0]).@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 1610156508 + ",methodId=" + methodId);
                    }

                case -1277021679:
                    switch (methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + interfaceId);
            }
        }

        public global::System.Int32 InterfaceId
        {
            get
            {
                return 1044054890;
            }
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.SerializableAttribute, global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.GrainReferenceAttribute(typeof (global::BrightSkool.GrainInterface.Masters.ICurrency))]
    internal class OrleansCodeGenCurrencyReference : global::Orleans.Runtime.GrainReference, global::BrightSkool.GrainInterface.Masters.ICurrency
    {
        protected @OrleansCodeGenCurrencyReference(global::Orleans.Runtime.GrainReference @other): base (@other)
        {
        }

        protected @OrleansCodeGenCurrencyReference(global::System.Runtime.Serialization.SerializationInfo @info, global::System.Runtime.Serialization.StreamingContext @context): base (@info, @context)
        {
        }

        protected override global::System.Int32 InterfaceId
        {
            get
            {
                return 788516508;
            }
        }

        public override global::System.String InterfaceName
        {
            get
            {
                return "global::BrightSkool.GrainInterface.Masters.ICurrency";
            }
        }

        public override global::System.Boolean @IsCompatible(global::System.Int32 @interfaceId)
        {
            return @interfaceId == 788516508 || @interfaceId == -1358622703 || @interfaceId == -1277021679;
        }

        protected override global::System.String @GetMethodName(global::System.Int32 @interfaceId, global::System.Int32 @methodId)
        {
            switch (@interfaceId)
            {
                case 788516508:
                    switch (@methodId)
                    {
                        case 2058492049:
                            return "GetItem";
                        case -1284532113:
                            return "SetItem";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 788516508 + ",methodId=" + @methodId);
                    }

                case -1358622703:
                    switch (@methodId)
                    {
                        case 2058492049:
                            return "GetItem";
                        case -1284532113:
                            return "SetItem";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1358622703 + ",methodId=" + @methodId);
                    }

                case -1277021679:
                    switch (@methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + @methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + @interfaceId);
            }
        }

        public global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Currency>> @GetItem()
        {
            return base.@InvokeMethodAsync<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Currency>>(2058492049, null);
        }

        public global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Currency>> @SetItem(global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Currency> @item)
        {
            return base.@InvokeMethodAsync<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Currency>>(-1284532113, new global::System.Object[]{@item});
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::Orleans.CodeGeneration.MethodInvokerAttribute("global::BrightSkool.GrainInterface.Masters.ICurrency", 788516508, typeof (global::BrightSkool.GrainInterface.Masters.ICurrency)), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute]
    internal class OrleansCodeGenCurrencyMethodInvoker : global::Orleans.CodeGeneration.IGrainMethodInvoker
    {
        public global::System.Threading.Tasks.Task<global::System.Object> @Invoke(global::Orleans.Runtime.IAddressable @grain, global::Orleans.CodeGeneration.InvokeMethodRequest @request)
        {
            global::System.Int32 interfaceId = @request.@InterfaceId;
            global::System.Int32 methodId = @request.@MethodId;
            global::System.Object[] arguments = @request.@Arguments;
            if (@grain == null)
                throw new global::System.ArgumentNullException("grain");
            switch (interfaceId)
            {
                case 788516508:
                    switch (methodId)
                    {
                        case 2058492049:
                            return ((global::BrightSkool.GrainInterface.Masters.ICurrency)@grain).@GetItem().@Box();
                        case -1284532113:
                            return ((global::BrightSkool.GrainInterface.Masters.ICurrency)@grain).@SetItem((global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Currency>)arguments[0]).@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 788516508 + ",methodId=" + methodId);
                    }

                case -1358622703:
                    switch (methodId)
                    {
                        case 2058492049:
                            return ((global::BrightSkool.GrainInterface.Masters.ICurrency)@grain).@GetItem().@Box();
                        case -1284532113:
                            return ((global::BrightSkool.GrainInterface.Masters.ICurrency)@grain).@SetItem((global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Currency>)arguments[0]).@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1358622703 + ",methodId=" + methodId);
                    }

                case -1277021679:
                    switch (methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + interfaceId);
            }
        }

        public global::System.Int32 InterfaceId
        {
            get
            {
                return 788516508;
            }
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.SerializerAttribute(typeof (global::BrightSkool.Domain.Models.CommonModel.Currency)), global::Orleans.CodeGeneration.RegisterSerializerAttribute]
    internal class OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_CurrencySerializer
    {
        [global::Orleans.CodeGeneration.CopierMethodAttribute]
        public static global::System.Object DeepCopier(global::System.Object original)
        {
            global::BrightSkool.Domain.Models.CommonModel.Currency input = ((global::BrightSkool.Domain.Models.CommonModel.Currency)original);
            global::BrightSkool.Domain.Models.CommonModel.Currency result = new global::BrightSkool.Domain.Models.CommonModel.Currency();
            global::Orleans.@Serialization.@SerializationContext.@Current.@RecordObject(original, result);
            result.@CountryName = input.@CountryName;
            result.@CurrencyCode = input.@CurrencyCode;
            result.@CurrencyName = input.@CurrencyName;
            return result;
        }

        [global::Orleans.CodeGeneration.SerializerMethodAttribute]
        public static void Serializer(global::System.Object untypedInput, global::Orleans.Serialization.BinaryTokenStreamWriter stream, global::System.Type expected)
        {
            global::BrightSkool.Domain.Models.CommonModel.Currency input = (global::BrightSkool.Domain.Models.CommonModel.Currency)untypedInput;
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@CountryName, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@CurrencyCode, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@CurrencyName, stream, typeof (global::System.String));
        }

        [global::Orleans.CodeGeneration.DeserializerMethodAttribute]
        public static global::System.Object Deserializer(global::System.Type expected, global::Orleans.Serialization.BinaryTokenStreamReader stream)
        {
            global::BrightSkool.Domain.Models.CommonModel.Currency result = new global::BrightSkool.Domain.Models.CommonModel.Currency();
            global::Orleans.@Serialization.@DeserializationContext.@Current.@RecordObject(result);
            result.@CountryName = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@CurrencyCode = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@CurrencyName = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            return (global::BrightSkool.Domain.Models.CommonModel.Currency)result;
        }

        public static void Register()
        {
            global::Orleans.Serialization.SerializationManager.@Register(typeof (global::BrightSkool.Domain.Models.CommonModel.Currency), DeepCopier, Serializer, Deserializer);
        }

        static OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_CurrencySerializer()
        {
            Register();
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.SerializableAttribute, global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.GrainReferenceAttribute(typeof (global::BrightSkool.GrainInterface.Masters.ILanguage))]
    internal class OrleansCodeGenLanguageReference : global::Orleans.Runtime.GrainReference, global::BrightSkool.GrainInterface.Masters.ILanguage
    {
        protected @OrleansCodeGenLanguageReference(global::Orleans.Runtime.GrainReference @other): base (@other)
        {
        }

        protected @OrleansCodeGenLanguageReference(global::System.Runtime.Serialization.SerializationInfo @info, global::System.Runtime.Serialization.StreamingContext @context): base (@info, @context)
        {
        }

        protected override global::System.Int32 InterfaceId
        {
            get
            {
                return -1434819069;
            }
        }

        public override global::System.String InterfaceName
        {
            get
            {
                return "global::BrightSkool.GrainInterface.Masters.ILanguage";
            }
        }

        public override global::System.Boolean @IsCompatible(global::System.Int32 @interfaceId)
        {
            return @interfaceId == -1434819069 || @interfaceId == -364671876 || @interfaceId == -1277021679;
        }

        protected override global::System.String @GetMethodName(global::System.Int32 @interfaceId, global::System.Int32 @methodId)
        {
            switch (@interfaceId)
            {
                case -1434819069:
                    switch (@methodId)
                    {
                        case 2058492049:
                            return "GetItem";
                        case -1244623853:
                            return "SetItem";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1434819069 + ",methodId=" + @methodId);
                    }

                case -364671876:
                    switch (@methodId)
                    {
                        case 2058492049:
                            return "GetItem";
                        case -1244623853:
                            return "SetItem";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -364671876 + ",methodId=" + @methodId);
                    }

                case -1277021679:
                    switch (@methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + @methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + @interfaceId);
            }
        }

        public global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Language>> @GetItem()
        {
            return base.@InvokeMethodAsync<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Language>>(2058492049, null);
        }

        public global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Language>> @SetItem(global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Language> @item)
        {
            return base.@InvokeMethodAsync<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Language>>(-1244623853, new global::System.Object[]{@item});
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::Orleans.CodeGeneration.MethodInvokerAttribute("global::BrightSkool.GrainInterface.Masters.ILanguage", -1434819069, typeof (global::BrightSkool.GrainInterface.Masters.ILanguage)), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute]
    internal class OrleansCodeGenLanguageMethodInvoker : global::Orleans.CodeGeneration.IGrainMethodInvoker
    {
        public global::System.Threading.Tasks.Task<global::System.Object> @Invoke(global::Orleans.Runtime.IAddressable @grain, global::Orleans.CodeGeneration.InvokeMethodRequest @request)
        {
            global::System.Int32 interfaceId = @request.@InterfaceId;
            global::System.Int32 methodId = @request.@MethodId;
            global::System.Object[] arguments = @request.@Arguments;
            if (@grain == null)
                throw new global::System.ArgumentNullException("grain");
            switch (interfaceId)
            {
                case -1434819069:
                    switch (methodId)
                    {
                        case 2058492049:
                            return ((global::BrightSkool.GrainInterface.Masters.ILanguage)@grain).@GetItem().@Box();
                        case -1244623853:
                            return ((global::BrightSkool.GrainInterface.Masters.ILanguage)@grain).@SetItem((global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Language>)arguments[0]).@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1434819069 + ",methodId=" + methodId);
                    }

                case -364671876:
                    switch (methodId)
                    {
                        case 2058492049:
                            return ((global::BrightSkool.GrainInterface.Masters.ILanguage)@grain).@GetItem().@Box();
                        case -1244623853:
                            return ((global::BrightSkool.GrainInterface.Masters.ILanguage)@grain).@SetItem((global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Language>)arguments[0]).@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -364671876 + ",methodId=" + methodId);
                    }

                case -1277021679:
                    switch (methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + interfaceId);
            }
        }

        public global::System.Int32 InterfaceId
        {
            get
            {
                return -1434819069;
            }
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.SerializerAttribute(typeof (global::BrightSkool.Domain.Models.CommonModel.Language)), global::Orleans.CodeGeneration.RegisterSerializerAttribute]
    internal class OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_LanguageSerializer
    {
        [global::Orleans.CodeGeneration.CopierMethodAttribute]
        public static global::System.Object DeepCopier(global::System.Object original)
        {
            global::BrightSkool.Domain.Models.CommonModel.Language input = ((global::BrightSkool.Domain.Models.CommonModel.Language)original);
            global::BrightSkool.Domain.Models.CommonModel.Language result = new global::BrightSkool.Domain.Models.CommonModel.Language();
            global::Orleans.@Serialization.@SerializationContext.@Current.@RecordObject(original, result);
            result.@LangaugeCode = input.@LangaugeCode;
            result.@LanguageName = input.@LanguageName;
            return result;
        }

        [global::Orleans.CodeGeneration.SerializerMethodAttribute]
        public static void Serializer(global::System.Object untypedInput, global::Orleans.Serialization.BinaryTokenStreamWriter stream, global::System.Type expected)
        {
            global::BrightSkool.Domain.Models.CommonModel.Language input = (global::BrightSkool.Domain.Models.CommonModel.Language)untypedInput;
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@LangaugeCode, stream, typeof (global::System.String));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@LanguageName, stream, typeof (global::System.String));
        }

        [global::Orleans.CodeGeneration.DeserializerMethodAttribute]
        public static global::System.Object Deserializer(global::System.Type expected, global::Orleans.Serialization.BinaryTokenStreamReader stream)
        {
            global::BrightSkool.Domain.Models.CommonModel.Language result = new global::BrightSkool.Domain.Models.CommonModel.Language();
            global::Orleans.@Serialization.@DeserializationContext.@Current.@RecordObject(result);
            result.@LangaugeCode = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            result.@LanguageName = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            return (global::BrightSkool.Domain.Models.CommonModel.Language)result;
        }

        public static void Register()
        {
            global::Orleans.Serialization.SerializationManager.@Register(typeof (global::BrightSkool.Domain.Models.CommonModel.Language), DeepCopier, Serializer, Deserializer);
        }

        static OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_LanguageSerializer()
        {
            Register();
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.SerializableAttribute, global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.GrainReferenceAttribute(typeof (global::BrightSkool.GrainInterface.Masters.INationality))]
    internal class OrleansCodeGenNationalityReference : global::Orleans.Runtime.GrainReference, global::BrightSkool.GrainInterface.Masters.INationality
    {
        protected @OrleansCodeGenNationalityReference(global::Orleans.Runtime.GrainReference @other): base (@other)
        {
        }

        protected @OrleansCodeGenNationalityReference(global::System.Runtime.Serialization.SerializationInfo @info, global::System.Runtime.Serialization.StreamingContext @context): base (@info, @context)
        {
        }

        protected override global::System.Int32 InterfaceId
        {
            get
            {
                return -1336298381;
            }
        }

        public override global::System.String InterfaceName
        {
            get
            {
                return "global::BrightSkool.GrainInterface.Masters.INationality";
            }
        }

        public override global::System.Boolean @IsCompatible(global::System.Int32 @interfaceId)
        {
            return @interfaceId == -1336298381 || @interfaceId == 2025221331 || @interfaceId == -1277021679;
        }

        protected override global::System.String @GetMethodName(global::System.Int32 @interfaceId, global::System.Int32 @methodId)
        {
            switch (@interfaceId)
            {
                case -1336298381:
                    switch (@methodId)
                    {
                        case 2058492049:
                            return "GetItem";
                        case 1514597504:
                            return "SetItem";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1336298381 + ",methodId=" + @methodId);
                    }

                case 2025221331:
                    switch (@methodId)
                    {
                        case 2058492049:
                            return "GetItem";
                        case 1514597504:
                            return "SetItem";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 2025221331 + ",methodId=" + @methodId);
                    }

                case -1277021679:
                    switch (@methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + @methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + @interfaceId);
            }
        }

        public global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Nationality>> @GetItem()
        {
            return base.@InvokeMethodAsync<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Nationality>>(2058492049, null);
        }

        public global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Nationality>> @SetItem(global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Nationality> @item)
        {
            return base.@InvokeMethodAsync<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Nationality>>(1514597504, new global::System.Object[]{@item});
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::Orleans.CodeGeneration.MethodInvokerAttribute("global::BrightSkool.GrainInterface.Masters.INationality", -1336298381, typeof (global::BrightSkool.GrainInterface.Masters.INationality)), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute]
    internal class OrleansCodeGenNationalityMethodInvoker : global::Orleans.CodeGeneration.IGrainMethodInvoker
    {
        public global::System.Threading.Tasks.Task<global::System.Object> @Invoke(global::Orleans.Runtime.IAddressable @grain, global::Orleans.CodeGeneration.InvokeMethodRequest @request)
        {
            global::System.Int32 interfaceId = @request.@InterfaceId;
            global::System.Int32 methodId = @request.@MethodId;
            global::System.Object[] arguments = @request.@Arguments;
            if (@grain == null)
                throw new global::System.ArgumentNullException("grain");
            switch (interfaceId)
            {
                case -1336298381:
                    switch (methodId)
                    {
                        case 2058492049:
                            return ((global::BrightSkool.GrainInterface.Masters.INationality)@grain).@GetItem().@Box();
                        case 1514597504:
                            return ((global::BrightSkool.GrainInterface.Masters.INationality)@grain).@SetItem((global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Nationality>)arguments[0]).@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1336298381 + ",methodId=" + methodId);
                    }

                case 2025221331:
                    switch (methodId)
                    {
                        case 2058492049:
                            return ((global::BrightSkool.GrainInterface.Masters.INationality)@grain).@GetItem().@Box();
                        case 1514597504:
                            return ((global::BrightSkool.GrainInterface.Masters.INationality)@grain).@SetItem((global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Nationality>)arguments[0]).@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 2025221331 + ",methodId=" + methodId);
                    }

                case -1277021679:
                    switch (methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + interfaceId);
            }
        }

        public global::System.Int32 InterfaceId
        {
            get
            {
                return -1336298381;
            }
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.SerializableAttribute, global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.GrainReferenceAttribute(typeof (global::BrightSkool.GrainInterface.Masters.IRace))]
    internal class OrleansCodeGenRaceReference : global::Orleans.Runtime.GrainReference, global::BrightSkool.GrainInterface.Masters.IRace
    {
        protected @OrleansCodeGenRaceReference(global::Orleans.Runtime.GrainReference @other): base (@other)
        {
        }

        protected @OrleansCodeGenRaceReference(global::System.Runtime.Serialization.SerializationInfo @info, global::System.Runtime.Serialization.StreamingContext @context): base (@info, @context)
        {
        }

        protected override global::System.Int32 InterfaceId
        {
            get
            {
                return -1527665332;
            }
        }

        public override global::System.String InterfaceName
        {
            get
            {
                return "global::BrightSkool.GrainInterface.Masters.IRace";
            }
        }

        public override global::System.Boolean @IsCompatible(global::System.Int32 @interfaceId)
        {
            return @interfaceId == -1527665332 || @interfaceId == 893688332 || @interfaceId == -1277021679;
        }

        protected override global::System.String @GetMethodName(global::System.Int32 @interfaceId, global::System.Int32 @methodId)
        {
            switch (@interfaceId)
            {
                case -1527665332:
                    switch (@methodId)
                    {
                        case 2058492049:
                            return "GetItem";
                        case -1955688359:
                            return "SetItem";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1527665332 + ",methodId=" + @methodId);
                    }

                case 893688332:
                    switch (@methodId)
                    {
                        case 2058492049:
                            return "GetItem";
                        case -1955688359:
                            return "SetItem";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 893688332 + ",methodId=" + @methodId);
                    }

                case -1277021679:
                    switch (@methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + @methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + @interfaceId);
            }
        }

        public global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Race>> @GetItem()
        {
            return base.@InvokeMethodAsync<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Race>>(2058492049, null);
        }

        public global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Race>> @SetItem(global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Race> @item)
        {
            return base.@InvokeMethodAsync<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Race>>(-1955688359, new global::System.Object[]{@item});
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::Orleans.CodeGeneration.MethodInvokerAttribute("global::BrightSkool.GrainInterface.Masters.IRace", -1527665332, typeof (global::BrightSkool.GrainInterface.Masters.IRace)), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute]
    internal class OrleansCodeGenRaceMethodInvoker : global::Orleans.CodeGeneration.IGrainMethodInvoker
    {
        public global::System.Threading.Tasks.Task<global::System.Object> @Invoke(global::Orleans.Runtime.IAddressable @grain, global::Orleans.CodeGeneration.InvokeMethodRequest @request)
        {
            global::System.Int32 interfaceId = @request.@InterfaceId;
            global::System.Int32 methodId = @request.@MethodId;
            global::System.Object[] arguments = @request.@Arguments;
            if (@grain == null)
                throw new global::System.ArgumentNullException("grain");
            switch (interfaceId)
            {
                case -1527665332:
                    switch (methodId)
                    {
                        case 2058492049:
                            return ((global::BrightSkool.GrainInterface.Masters.IRace)@grain).@GetItem().@Box();
                        case -1955688359:
                            return ((global::BrightSkool.GrainInterface.Masters.IRace)@grain).@SetItem((global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Race>)arguments[0]).@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1527665332 + ",methodId=" + methodId);
                    }

                case 893688332:
                    switch (methodId)
                    {
                        case 2058492049:
                            return ((global::BrightSkool.GrainInterface.Masters.IRace)@grain).@GetItem().@Box();
                        case -1955688359:
                            return ((global::BrightSkool.GrainInterface.Masters.IRace)@grain).@SetItem((global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Race>)arguments[0]).@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 893688332 + ",methodId=" + methodId);
                    }

                case -1277021679:
                    switch (methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + interfaceId);
            }
        }

        public global::System.Int32 InterfaceId
        {
            get
            {
                return -1527665332;
            }
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.SerializerAttribute(typeof (global::BrightSkool.Domain.Models.CommonModel.Race)), global::Orleans.CodeGeneration.RegisterSerializerAttribute]
    internal class OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_RaceSerializer
    {
        [global::Orleans.CodeGeneration.CopierMethodAttribute]
        public static global::System.Object DeepCopier(global::System.Object original)
        {
            global::BrightSkool.Domain.Models.CommonModel.Race input = ((global::BrightSkool.Domain.Models.CommonModel.Race)original);
            global::BrightSkool.Domain.Models.CommonModel.Race result = new global::BrightSkool.Domain.Models.CommonModel.Race();
            global::Orleans.@Serialization.@SerializationContext.@Current.@RecordObject(original, result);
            result.@RaceName = input.@RaceName;
            return result;
        }

        [global::Orleans.CodeGeneration.SerializerMethodAttribute]
        public static void Serializer(global::System.Object untypedInput, global::Orleans.Serialization.BinaryTokenStreamWriter stream, global::System.Type expected)
        {
            global::BrightSkool.Domain.Models.CommonModel.Race input = (global::BrightSkool.Domain.Models.CommonModel.Race)untypedInput;
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@RaceName, stream, typeof (global::System.String));
        }

        [global::Orleans.CodeGeneration.DeserializerMethodAttribute]
        public static global::System.Object Deserializer(global::System.Type expected, global::Orleans.Serialization.BinaryTokenStreamReader stream)
        {
            global::BrightSkool.Domain.Models.CommonModel.Race result = new global::BrightSkool.Domain.Models.CommonModel.Race();
            global::Orleans.@Serialization.@DeserializationContext.@Current.@RecordObject(result);
            result.@RaceName = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            return (global::BrightSkool.Domain.Models.CommonModel.Race)result;
        }

        public static void Register()
        {
            global::Orleans.Serialization.SerializationManager.@Register(typeof (global::BrightSkool.Domain.Models.CommonModel.Race), DeepCopier, Serializer, Deserializer);
        }

        static OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_RaceSerializer()
        {
            Register();
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.SerializableAttribute, global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.GrainReferenceAttribute(typeof (global::BrightSkool.GrainInterface.Masters.IRelationship))]
    internal class OrleansCodeGenRelationshipReference : global::Orleans.Runtime.GrainReference, global::BrightSkool.GrainInterface.Masters.IRelationship
    {
        protected @OrleansCodeGenRelationshipReference(global::Orleans.Runtime.GrainReference @other): base (@other)
        {
        }

        protected @OrleansCodeGenRelationshipReference(global::System.Runtime.Serialization.SerializationInfo @info, global::System.Runtime.Serialization.StreamingContext @context): base (@info, @context)
        {
        }

        protected override global::System.Int32 InterfaceId
        {
            get
            {
                return 1412002569;
            }
        }

        public override global::System.String InterfaceName
        {
            get
            {
                return "global::BrightSkool.GrainInterface.Masters.IRelationship";
            }
        }

        public override global::System.Boolean @IsCompatible(global::System.Int32 @interfaceId)
        {
            return @interfaceId == 1412002569 || @interfaceId == 2038910808 || @interfaceId == -1277021679;
        }

        protected override global::System.String @GetMethodName(global::System.Int32 @interfaceId, global::System.Int32 @methodId)
        {
            switch (@interfaceId)
            {
                case 1412002569:
                    switch (@methodId)
                    {
                        case 2058492049:
                            return "GetItem";
                        case -543610106:
                            return "SetItem";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 1412002569 + ",methodId=" + @methodId);
                    }

                case 2038910808:
                    switch (@methodId)
                    {
                        case 2058492049:
                            return "GetItem";
                        case -543610106:
                            return "SetItem";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 2038910808 + ",methodId=" + @methodId);
                    }

                case -1277021679:
                    switch (@methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + @methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + @interfaceId);
            }
        }

        public global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Relationship>> @GetItem()
        {
            return base.@InvokeMethodAsync<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Relationship>>(2058492049, null);
        }

        public global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Relationship>> @SetItem(global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Relationship> @item)
        {
            return base.@InvokeMethodAsync<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Relationship>>(-543610106, new global::System.Object[]{@item});
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::Orleans.CodeGeneration.MethodInvokerAttribute("global::BrightSkool.GrainInterface.Masters.IRelationship", 1412002569, typeof (global::BrightSkool.GrainInterface.Masters.IRelationship)), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute]
    internal class OrleansCodeGenRelationshipMethodInvoker : global::Orleans.CodeGeneration.IGrainMethodInvoker
    {
        public global::System.Threading.Tasks.Task<global::System.Object> @Invoke(global::Orleans.Runtime.IAddressable @grain, global::Orleans.CodeGeneration.InvokeMethodRequest @request)
        {
            global::System.Int32 interfaceId = @request.@InterfaceId;
            global::System.Int32 methodId = @request.@MethodId;
            global::System.Object[] arguments = @request.@Arguments;
            if (@grain == null)
                throw new global::System.ArgumentNullException("grain");
            switch (interfaceId)
            {
                case 1412002569:
                    switch (methodId)
                    {
                        case 2058492049:
                            return ((global::BrightSkool.GrainInterface.Masters.IRelationship)@grain).@GetItem().@Box();
                        case -543610106:
                            return ((global::BrightSkool.GrainInterface.Masters.IRelationship)@grain).@SetItem((global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Relationship>)arguments[0]).@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 1412002569 + ",methodId=" + methodId);
                    }

                case 2038910808:
                    switch (methodId)
                    {
                        case 2058492049:
                            return ((global::BrightSkool.GrainInterface.Masters.IRelationship)@grain).@GetItem().@Box();
                        case -543610106:
                            return ((global::BrightSkool.GrainInterface.Masters.IRelationship)@grain).@SetItem((global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Relationship>)arguments[0]).@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 2038910808 + ",methodId=" + methodId);
                    }

                case -1277021679:
                    switch (methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + interfaceId);
            }
        }

        public global::System.Int32 InterfaceId
        {
            get
            {
                return 1412002569;
            }
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.SerializerAttribute(typeof (global::BrightSkool.Domain.Models.CommonModel.Relationship)), global::Orleans.CodeGeneration.RegisterSerializerAttribute]
    internal class OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_RelationshipSerializer
    {
        [global::Orleans.CodeGeneration.CopierMethodAttribute]
        public static global::System.Object DeepCopier(global::System.Object original)
        {
            global::BrightSkool.Domain.Models.CommonModel.Relationship input = ((global::BrightSkool.Domain.Models.CommonModel.Relationship)original);
            global::BrightSkool.Domain.Models.CommonModel.Relationship result = new global::BrightSkool.Domain.Models.CommonModel.Relationship();
            global::Orleans.@Serialization.@SerializationContext.@Current.@RecordObject(original, result);
            result.@Relation = input.@Relation;
            return result;
        }

        [global::Orleans.CodeGeneration.SerializerMethodAttribute]
        public static void Serializer(global::System.Object untypedInput, global::Orleans.Serialization.BinaryTokenStreamWriter stream, global::System.Type expected)
        {
            global::BrightSkool.Domain.Models.CommonModel.Relationship input = (global::BrightSkool.Domain.Models.CommonModel.Relationship)untypedInput;
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@Relation, stream, typeof (global::System.String));
        }

        [global::Orleans.CodeGeneration.DeserializerMethodAttribute]
        public static global::System.Object Deserializer(global::System.Type expected, global::Orleans.Serialization.BinaryTokenStreamReader stream)
        {
            global::BrightSkool.Domain.Models.CommonModel.Relationship result = new global::BrightSkool.Domain.Models.CommonModel.Relationship();
            global::Orleans.@Serialization.@DeserializationContext.@Current.@RecordObject(result);
            result.@Relation = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), stream);
            return (global::BrightSkool.Domain.Models.CommonModel.Relationship)result;
        }

        public static void Register()
        {
            global::Orleans.Serialization.SerializationManager.@Register(typeof (global::BrightSkool.Domain.Models.CommonModel.Relationship), DeepCopier, Serializer, Deserializer);
        }

        static OrleansCodeGenBrightSkool_Domain_Models_CommonModelSerializer_RelationshipSerializer()
        {
            Register();
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::System.SerializableAttribute, global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.GrainReferenceAttribute(typeof (global::BrightSkool.GrainInterface.Masters.IReligion))]
    internal class OrleansCodeGenReligionReference : global::Orleans.Runtime.GrainReference, global::BrightSkool.GrainInterface.Masters.IReligion
    {
        protected @OrleansCodeGenReligionReference(global::Orleans.Runtime.GrainReference @other): base (@other)
        {
        }

        protected @OrleansCodeGenReligionReference(global::System.Runtime.Serialization.SerializationInfo @info, global::System.Runtime.Serialization.StreamingContext @context): base (@info, @context)
        {
        }

        protected override global::System.Int32 InterfaceId
        {
            get
            {
                return 147030756;
            }
        }

        public override global::System.String InterfaceName
        {
            get
            {
                return "global::BrightSkool.GrainInterface.Masters.IReligion";
            }
        }

        public override global::System.Boolean @IsCompatible(global::System.Int32 @interfaceId)
        {
            return @interfaceId == 147030756 || @interfaceId == 387197620 || @interfaceId == -1277021679;
        }

        protected override global::System.String @GetMethodName(global::System.Int32 @interfaceId, global::System.Int32 @methodId)
        {
            switch (@interfaceId)
            {
                case 147030756:
                    switch (@methodId)
                    {
                        case 2058492049:
                            return "GetItem";
                        case 1716547891:
                            return "SetItem";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 147030756 + ",methodId=" + @methodId);
                    }

                case 387197620:
                    switch (@methodId)
                    {
                        case 2058492049:
                            return "GetItem";
                        case 1716547891:
                            return "SetItem";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 387197620 + ",methodId=" + @methodId);
                    }

                case -1277021679:
                    switch (@methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + @methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + @interfaceId);
            }
        }

        public global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Religion>> @GetItem()
        {
            return base.@InvokeMethodAsync<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Religion>>(2058492049, null);
        }

        public global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Religion>> @SetItem(global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Religion> @item)
        {
            return base.@InvokeMethodAsync<global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Religion>>(1716547891, new global::System.Object[]{@item});
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.1.0"), global::Orleans.CodeGeneration.MethodInvokerAttribute("global::BrightSkool.GrainInterface.Masters.IReligion", 147030756, typeof (global::BrightSkool.GrainInterface.Masters.IReligion)), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute]
    internal class OrleansCodeGenReligionMethodInvoker : global::Orleans.CodeGeneration.IGrainMethodInvoker
    {
        public global::System.Threading.Tasks.Task<global::System.Object> @Invoke(global::Orleans.Runtime.IAddressable @grain, global::Orleans.CodeGeneration.InvokeMethodRequest @request)
        {
            global::System.Int32 interfaceId = @request.@InterfaceId;
            global::System.Int32 methodId = @request.@MethodId;
            global::System.Object[] arguments = @request.@Arguments;
            if (@grain == null)
                throw new global::System.ArgumentNullException("grain");
            switch (interfaceId)
            {
                case 147030756:
                    switch (methodId)
                    {
                        case 2058492049:
                            return ((global::BrightSkool.GrainInterface.Masters.IReligion)@grain).@GetItem().@Box();
                        case 1716547891:
                            return ((global::BrightSkool.GrainInterface.Masters.IReligion)@grain).@SetItem((global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Religion>)arguments[0]).@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 147030756 + ",methodId=" + methodId);
                    }

                case 387197620:
                    switch (methodId)
                    {
                        case 2058492049:
                            return ((global::BrightSkool.GrainInterface.Masters.IReligion)@grain).@GetItem().@Box();
                        case 1716547891:
                            return ((global::BrightSkool.GrainInterface.Masters.IReligion)@grain).@SetItem((global::System.Collections.Generic.List<global::BrightSkool.Domain.Models.CommonModel.Religion>)arguments[0]).@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 387197620 + ",methodId=" + methodId);
                    }

                case -1277021679:
                    switch (methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + interfaceId);
            }
        }

        public global::System.Int32 InterfaceId
        {
            get
            {
                return 147030756;
            }
        }
    }
}
#pragma warning restore 162
#pragma warning restore 219
#pragma warning restore 414
#pragma warning restore 649
#pragma warning restore 693
#pragma warning restore 1591
#pragma warning restore 1998
#endif
