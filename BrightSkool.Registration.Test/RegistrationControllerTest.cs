﻿using BrightSkool.BusinessLayer.BAL.FileUpload;
using BrightSkool.BusinessLayer.BAL.Masters;
using BrightSkool.BusinessLayer.BAL.Registration;
using BrightSkool.Domain.Dto;
using BrightSkool.Domain.Models;
using BrightSkool.Registration.Controllers;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Xunit;

namespace BrightSkool.Registration.Test
{
    public class RegistrationControllerTest
    {
        [Fact]
        public void ShouldGetRegistrationWithEmptyObject ()
        {
            //Arrange
            var registrationService = new Mock<IRegistrationService>();
            var countryService = new Mock<ICountryService>();
            var fileUploadService = new Mock<IFileUploadService>();
            var nationalityService = new Mock<INationalityService>();
            var relationshipService = new Mock<IRelationshipService>();
            var languageService = new Mock<ILanguageService>();
            var religionService = new Mock<IReligionService>();
            RegistrationController registrationController = new RegistrationController(registrationService.Object, countryService.Object, fileUploadService.Object, nationalityService.Object, relationshipService.Object, languageService.Object, religionService.Object);
            //registrationService.Setup(s => s.GetRegistrationbyID(It.IsAny<Guid>())).Returns(Task.Run(() => new RegistrationModel()));

            countryService.Setup(s => s.GetCountries()).Returns(Task.Run(() => new List<CommonModel.Country> {
                 new CommonModel.Country(){ CountryCode = "IND", CountryName="India" },
                 new CommonModel.Country(){ CountryCode = "PAK", CountryName="Pakistan" },
                 new CommonModel.Country(){ CountryCode = "BAN", CountryName="Bangladesh" },
            }));

            //Act
            var viewResult = registrationController.Registration().Result as ViewResult;
            var result = registrationController.Registration().Result;
            var model = viewResult.ViewData.Model;
            var registrationModel = (RegistrationModel)model;

            //Assert
            //Checking Base Type for Action Method. 
            Assert.IsAssignableFrom<ActionResult>(result);
            //Checking the particular return type. 
            Assert.IsType<ViewResult>(result);
            //Checking the model passed to the view.
            Assert.IsType<RegistrationModel>(model);
            //Checking the Id is Empty
            Assert.True(registrationModel.Id == Guid.Empty);
            //Checking For NotNull for Countries
            Assert.NotNull(viewResult.ViewBag.Countries);
        }

        //[Fact]
        //public void ShouldGetRegistrationWithRegistrationModelObject()
        //{
        //    //Arrange
        //    var registrationService = new Mock<IRegistrationService>();
        //    var countryService = new Mock<ICountryService>();
        //    var fileUploadService = new Mock<IFileUploadService>();
        //    var nationalityService = new Mock<INationalityService>();
        //    var relationshipService = new Mock<IRelationshipService>();
        //    var languageService = new Mock<ILanguageService>();
        //    RegistrationController registrationController = new RegistrationController(registrationService.Object, countryService.Object, fileUploadService.Object, nationalityService.Object, relationshipService.Object, languageService.Object);
        //    registrationService.Setup(s => s.GetRegistrationbyID(It.IsAny<Guid>())).Returns(Task.Run(() => new RegistrationModel
        //    {
        //        Id = Guid.NewGuid(),
        //        Email = "newmail@gmail.com",
        //    }));

        //    countryService.Setup(s => s.GetCountries()).Returns(Task.Run(() => new List<CommonModel.Country> {
        //         new CommonModel.Country(){ CountryCode = "IND", CountryName="India" },
        //         new CommonModel.Country(){ CountryCode = "PAK", CountryName="Pakistan" },
        //         new CommonModel.Country(){ CountryCode = "BAN", CountryName="Bangladesh" },
        //    }));

        //    //Act
        //    var viewResult = registrationController.Registration().Result as ViewResult;
        //    var result = registrationController.Registration().Result;
        //    var model = viewResult.ViewData.Model;
        //    var registrationModel = (RegistrationModel)model;

        //    //Assert
        //    //Checking Base Type for Action Method. 
        //    Assert.IsAssignableFrom<ActionResult>(result);
        //    //Checking the particular return type. 
        //    Assert.IsType<ViewResult>(result);
        //    //Checking the model passed to the view.
        //    Assert.IsType<RegistrationModel>(model);
        //    //Checking the Id is Empty
        //    Assert.True(registrationModel.Id != Guid.Empty);
        //    //Checking For NotNull for Countries
        //    Assert.NotNull(viewResult.ViewBag.Countries);
        //}

        [Fact]
        public void ShouldSaveRegistration ()
        {
            //Arrange
            RegistrationDto registrationDto = new RegistrationDto();
            //RegistrationModel registrationModel = new RegistrationModel();
            CommonModel.Person person = new CommonModel.Person();
            person.FirstName = "Ramesh";
            person.LastName = "Kumar";
            person.Gender = "Male";
            registrationDto.Applicant = person;

            //create mock of RegistrationService
            var registrationService = new Mock<IRegistrationService>();
            var countryService = new Mock<ICountryService>();
            var fileUpoladService = new Mock<IFileUploadService>();
            var nationalityService = new Mock<INationalityService>();
            var relationshipService = new Mock<IRelationshipService>();
            var languageService = new Mock<ILanguageService>();
            var religionService = new Mock<IReligionService>();
            registrationService.Setup(f => f.SaveRegistration(registrationDto)).Returns(Task.Run(() => 1 == 1 ? true : false));

            RegistrationController registrationController = new RegistrationController(registrationService.Object, countryService.Object, fileUpoladService.Object, nationalityService.Object, relationshipService.Object, languageService.Object, religionService.Object);

            //create mock of HttpServerUtilityBase
            var server = new Mock<HttpServerUtilityBase>();
            //set up mock to return a value on Server.MapPath Call.
            server.Setup(x => x.MapPath("~/Temp")).Returns("c:\\temp\\");
            var httpContext = new Mock<HttpContextBase>();
            httpContext.Setup(x => x.Server).Returns(server.Object);
            registrationController.ControllerContext = new ControllerContext(httpContext.Object, new RouteData(), registrationController);

            //create mock of HttpServerUtilityBase
            var postedfile = new Mock<HttpPostedFileBase>();

            //Act
            var redirectResult = registrationController.SaveOrUpdateModel(registrationDto, postedfile.Object).Result as RedirectToRouteResult;

            //Assert
            Assert.Equal("ReadModel", redirectResult.RouteValues["action"]);
        }

        [Fact]
        public void ShouldSaveRegistrationWithImage ()
        {
            //Arrange
            RegistrationDto registrationDto = new RegistrationDto();
            //RegistrationModel registrationModel = new RegistrationModel();
            CommonModel.Person person = new CommonModel.Person();
            person.FirstName = "Ramesh";
            person.LastName = "Kumar";
            person.Gender = "Male";
            registrationDto.Applicant = person;

            //create mock of RegistrationService
            var registrationService = new Mock<IRegistrationService>();
            var countryService = new Mock<ICountryService>();
            var fileUpoladService = new Mock<IFileUploadService>();
            var nationalityService = new Mock<INationalityService>();
            var relationshipService = new Mock<IRelationshipService>();
            var languageService = new Mock<ILanguageService>();
            var religionService = new Mock<IReligionService>();
            registrationService.Setup(f => f.SaveRegistration(registrationDto)).Returns(Task.Run(() => 1 == 1 ? true : false));

            RegistrationController registrationController = new RegistrationController(registrationService.Object, countryService.Object, fileUpoladService.Object, nationalityService.Object, relationshipService.Object, languageService.Object, religionService.Object);

            //create mock of HttpServerUtilityBase
            var server = new Mock<HttpServerUtilityBase>();
            //set up mock to return a value on Server.MapPath Call.
            server.Setup(x => x.MapPath("~/Temp")).Returns("c:\\temp\\");
            var httpContext = new Mock<HttpContextBase>();
            httpContext.Setup(x => x.Server).Returns(server.Object);
            registrationController.ControllerContext = new ControllerContext(httpContext.Object, new RouteData(), registrationController);

            //create mock of HttpServerUtilityBase
            var postedfile = new Mock<HttpPostedFileBase>();
            postedfile.Setup(f => f.ContentLength).Returns(8192);
            postedfile.Setup(f => f.FileName).Returns("Test.jpeg");
            postedfile.Setup(f => f.ContentType).Returns("image/jpeg");
            postedfile.Setup(f => f.InputStream).Returns(OpenReadStream());
            //postedfile.Setup(f => f.SaveAs(It.IsAny<string>())).Verifiable();

            fileUpoladService.Setup(f => f.SavePicture(postedfile.Object, It.IsAny<string>(), It.IsAny<string>())).Returns(new FileUploadModel
            {
                FileName = postedfile.Object.FileName,
                FileVirtualPath = Guid.NewGuid().ToString() + "-" + postedfile.Object.FileName,
            });

            //Act
            var redirectResult = registrationController.SaveOrUpdateModel(registrationDto, postedfile.Object).Result as RedirectToRouteResult;

            //Assert
            Assert.Equal("ReadModel", redirectResult.RouteValues["action"]);
        }

        [Fact]
        public void ShouldFailSaveRegistration ()
        {
            //Arrange
            RegistrationDto registrationDto = new RegistrationDto();
            //RegistrationModel registrationModel = new RegistrationModel();
            CommonModel.Person person = new CommonModel.Person();
            person.FirstName = "Ramesh";
            person.LastName = "Kumar";
            person.Gender = "Male";
            registrationDto.Applicant = person;

            //create mock of RegistrationService
            var registrationService = new Mock<IRegistrationService>();
            var countryService = new Mock<ICountryService>();
            var fileUpoladService = new Mock<IFileUploadService>();
            var nationalityService = new Mock<INationalityService>();
            var relationshipService = new Mock<IRelationshipService>();
            var languageService = new Mock<ILanguageService>();
            var religionService = new Mock<IReligionService>();
            registrationService.Setup(f => f.SaveRegistration(registrationDto)).Returns(Task.Run(() => 1 == 2 ? true : false));

            RegistrationController registrationController = new RegistrationController(registrationService.Object, countryService.Object, fileUpoladService.Object, nationalityService.Object, relationshipService.Object, languageService.Object, religionService.Object);

            //create mock of HttpServerUtilityBase
            var server = new Mock<HttpServerUtilityBase>();
            //set up mock to return a value on Server.MapPath Call.
            server.Setup(x => x.MapPath("~/Temp")).Returns("c:\\temp\\");
            var httpContext = new Mock<HttpContextBase>();
            httpContext.Setup(x => x.Server).Returns(server.Object);
            registrationController.ControllerContext = new ControllerContext(httpContext.Object, new RouteData(), registrationController);

            //create mock of HttpServerUtilityBase
            var postedfile = new Mock<HttpPostedFileBase>();

            //Act
            var redirectResult = registrationController.SaveOrUpdateModel(registrationDto, postedfile.Object).Result as RedirectToRouteResult;

            //Assert
            Assert.Equal("Registration", redirectResult.RouteValues["action"]);
        }

        [Fact]
        public void ShouldGetProcessRegistrationWithRegistrationModelObject ()
        {
            //Arrange
            var iregistrationService = new Mock<IRegistrationService>();
            var countryService = new Mock<ICountryService>();
            var fileUpoladService = new Mock<IFileUploadService>();
            var nationalityService = new Mock<INationalityService>();
            var relationshipService = new Mock<IRelationshipService>();
            var languageService = new Mock<ILanguageService>();
            var religionService = new Mock<IReligionService>();
            List<RegistrationDto> processRegistrations = new List<RegistrationDto>();
            for (int i = 0; i < 20; i++)
            {
                if (i < 5)
                    processRegistrations.Add(new RegistrationDto { Applicant = new CommonModel.Person { FirstName = "Madhu", LastName = "Babu", MiddleName = "Chowlur" }, Level = "10th", Age = 12, DistanceToSchool = 5, Nationality = new CommonModel.Nationality { CountryNationality = "Indian" }, RegisteredDate = DateTime.Now, Sibbling = string.Empty });
                if (i < 12)
                    processRegistrations.Add(new RegistrationDto { Applicant = new CommonModel.Person { FirstName = "Kishor", LastName = "Kumar", MiddleName = "Chowlur" }, Level = "5th", Age = 8, DistanceToSchool = 8, Nationality = new CommonModel.Nationality { CountryNationality = "Indian" }, RegisteredDate = DateTime.Now, Sibbling = string.Empty });
                if (i < 8)
                    processRegistrations.Add(new RegistrationDto { Applicant = new CommonModel.Person { FirstName = "Lakshmi", LastName = "Sathyanarayana", MiddleName = "" }, Level = "8th", Age = 10, DistanceToSchool = 12, Nationality = new CommonModel.Nationality { CountryNationality = "Indian" }, RegisteredDate = DateTime.Now, Sibbling = string.Empty });
                processRegistrations.Add(new RegistrationDto { Applicant = new CommonModel.Person { FirstName = "Sanju", LastName = "Sathyanarayana", MiddleName = "" }, Level = "5th", Age = 8, DistanceToSchool = 15, Nationality = new CommonModel.Nationality { CountryNationality = "Indian" }, RegisteredDate = DateTime.Now, Sibbling = string.Empty });
            }

            iregistrationService.Setup(s => s.ProcessRegistrations()).Returns(Task.Run(() => new List<RegistrationDto>(processRegistrations)));

            RegistrationController registrationService = new RegistrationController(iregistrationService.Object, countryService.Object, fileUpoladService.Object, nationalityService.Object, relationshipService.Object, languageService.Object, religionService.Object);

            //Act
            var viewResult = registrationService.ProcessRegistration().Result as ViewResult;
            var model = viewResult.ViewData.Model;
            var processRegistrationDtoModel = (List<RegistrationDto>)model;
            //Assert
            Assert.True(processRegistrationDtoModel.Count > 0);
        }

        [Fact]
        public void ShouldProcessRegistrationApplicantsIfListNotEmpty ()
        {
            //Arrange
            List<RegistrationModel> lstRegistrationModel = GetRegistrations();

            //create mock of RegistrationService
            var registrationService = new Mock<IRegistrationService>();
            var countryService = new Mock<ICountryService>();
            var fileUpoladService = new Mock<IFileUploadService>();
            var nationalityService = new Mock<INationalityService>();
            var relationshipService = new Mock<IRelationshipService>();
            var languageService = new Mock<ILanguageService>();
            var religionService = new Mock<IReligionService>();
            var applicantGuids = AccountGrainIds;
            registrationService.Setup(f => f.ProcessRegistrationApplicants(applicantGuids, "Selected")).Returns(Task.Run(() => 1 == 1 ? true : false));

            RegistrationController registrationController = new RegistrationController(registrationService.Object, countryService.Object, fileUpoladService.Object, nationalityService.Object, relationshipService.Object, languageService.Object, religionService.Object);

            //Act
            var jsonResult = registrationController.ProcessRegistrationApplicants(applicantGuids, "Selected").Result as JsonResult;

            //Assert
            Assert.Equal(true, jsonResult.Data);
        }

        [Fact]
        public void ShouldNotProcessRegistrationApplicantsIfListIsEmpty ()
        {
            //Arrange
            List<RegistrationModel> lstRegistrationModel = new List<RegistrationModel>();

            //create mock of RegistrationService
            var registrationService = new Mock<IRegistrationService>();
            var countryService = new Mock<ICountryService>();
            var fileUpoladService = new Mock<IFileUploadService>();
            var nationalityService = new Mock<INationalityService>();
            var relationshipService = new Mock<IRelationshipService>();
            var languageService = new Mock<ILanguageService>();
            var religionService = new Mock<IReligionService>();
            var applicantGuids = AccountGrainIds;
            registrationService.Setup(f => f.ProcessRegistrationApplicants(applicantGuids, "Selected")).Returns(Task.Run(() => 1 == 2 ? true : false));

            RegistrationController registrationController = new RegistrationController(registrationService.Object, countryService.Object, fileUpoladService.Object, nationalityService.Object, relationshipService.Object, languageService.Object, religionService.Object);

            //Act
            var jsonResult = registrationController.ProcessRegistrationApplicants(applicantGuids, "Selected").Result as JsonResult;

            //Assert
            Assert.Equal(false, jsonResult.Data);
        }

        public List<RegistrationModel> GetRegistrations ()
        {
            List<RegistrationModel> lstresitrationModels = new List<RegistrationModel>();
            RegistrationModel registrationModel = new RegistrationModel();
            CommonModel.Person person = new CommonModel.Person();
            person.FirstName = "Ramesh";
            person.LastName = "Kumar";
            person.Gender = "Male";
            registrationModel.Applicant = person;

            RegistrationModel registrationModel1 = new RegistrationModel();
            CommonModel.Person person1 = new CommonModel.Person();
            person1.FirstName = "Amit";
            person1.LastName = "Kumar";
            person1.Gender = "Male";
            registrationModel1.Applicant = person;
            lstresitrationModels.Add(registrationModel);
            lstresitrationModels.Add(registrationModel1);

            return lstresitrationModels;
        }

        public Stream OpenReadStream ()
        {
            var text = "File uploaded successfully";
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(text);
            writer.Flush();
            ms.Position = 0;
            return ms;
        }

        public static readonly List<Guid> AccountGrainIds = new List<Guid>
        {
            new Guid("{6b66f0ea-b329-4483-b78b-108f4df62eee}"),
            new Guid("{9c3d599b-0cac-4882-9244-12df92dc4da5}"),
            new Guid("{22447804-74e6-4a7f-aa8c-e8cd0dc851aa}"),
            new Guid("{c0f72fa5-da95-4166-8a60-1fac49820f3a}"),
            new Guid("{1a5f0a25-7965-4e22-92cf-127f0c462048}"),
        };
    }
}
