﻿using BrightSkool.BusinessLayer.OrleansClient.AppUtils;
using BrightSkool.Domain.Models;
using BrightSkool.GrainInterface.Registration;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Threading;
using System.Linq;

namespace BrightSkool.BusinessLayer.OrleansClient.Registration
{
    public class RegistrationClient : IRegistrationClient
    {

        public async Task<List<RegistrationModel>> RegistrationsList ()
        {
            List<RegistrationModel> Registrations = new List<RegistrationModel>();
            try
            {
                var registrationAggregate = GrainClient.GrainFactory.GetGrain<IRegistrationAggregateGrain>(Constants.RegistrationList);
                //var registeredgrains = await registrationAggregate.GetAggregateValues();
                var registereditems = await registrationAggregate.GetRegisteredGrains();

                if (registereditems == null) return await Task.FromResult(Registrations);
                foreach (var registeredItem in registereditems)
                {
                    var registration = await registeredItem.GetRegistration();
                    if (registration != null)
                        Registrations.Add(registration);
                }
            }
            catch (Exception)
            {
                //_exceptionHandler.HandleException(ex);
            }
            return await Task.FromResult(Registrations);
        }

        public async Task<RegistrationModel> GetRegistrationbyID (Guid Id)
        {
            RegistrationModel registration = new RegistrationModel();
            if (Id != Guid.Empty)
            {
                try
                {
                    var registrationAggregate = GrainClient.GrainFactory.GetGrain<IRegistrationAggregateGrain>(Constants.RegistrationList);
                    var registeredGrains = await registrationAggregate.GetRegisteredGrains();

                    Predicate<IRegistration> pre = (IRegistration p) =>
                    { return p.GetRegistration().Result.Id == Id; };
                    var registrationGrain = registeredGrains.Find(pre);
                    if (registrationGrain != null)
                        registration = await registrationGrain.GetRegistration();
                }
                catch (Exception)
                {
                    //_exceptionHandler.HandleException(ex);
                }
            }
            return await Task.FromResult(registration);
        }

        public async Task<bool> SaveRegistration (RegistrationModel registration)
        {
            try
            {
                if (Isvalid(registration))
                {
                    registration.Id = registration.Id.Equals(Guid.Empty) ? Guid.NewGuid() : registration.Id;
                    var registrationGrain = GrainClient.GrainFactory.GetGrain<IRegistration>(registration.Id);

                    var task = registrationGrain.SaveRegistration(registration);
                    while (!task.IsCompleted) ;

                    var aggrigategrain = GrainClient.GrainFactory.GetGrain<IRegistrationAggregateGrain>(Constants.RegistrationList);
                    var task3 = aggrigategrain.RegisterGrain(registrationGrain);
                    while (!task3.IsCompleted) ;

                    if (task.IsCompleted)
                    {
                        return await Task.FromResult(true);
                    }
                    else
                        return await Task.FromResult(false);
                }
                else
                    return await Task.FromResult(false);
            }
            catch (Exception)
            {
                //_exceptionHandler.HandleException(Ex);
                return await Task.FromResult(false);
            }
        }

        private bool Isvalid (RegistrationModel Registration)
        {
            bool valid = true;
            if (Registration.PIN.PersonalIDNumber == string.Empty)
                valid = false;
            if (Registration.Applicant.FirstName == string.Empty)
                valid = false;
            if (Registration.Applicant.DOB.Year == 1)
                valid = false;
            if (Registration.Nationality.CountryNationality == string.Empty)
                valid = false;
            if (Registration.StudentAddress.AddressLine1 == string.Empty)
                valid = false;
            if (Registration.StudentAddress.City == string.Empty)
                valid = false;
            if (Registration.StudentAddress.ZipCode == string.Empty)
                valid = false;
            if (Registration.StudentAddress.Country == string.Empty)
                valid = false;
            if (Registration.PhoneNumber.PhoneNumber == string.Empty)
                valid = false;
            if (Registration.Email == string.Empty)
                valid = false;
            if (Registration.Level == string.Empty)
                valid = false;
            if (Registration.SecondLanguage == string.Empty)
                valid = false;

            return valid;
        }

        public async Task<bool> ProcessRegisteredApplicants (List<Guid> applicantIds, string status)
        {
            try
            {
                var registrationGrains =
                applicantIds.Select(id => GrainClient.GrainFactory.GetGrain<IRegistration>(id)).ToList();

                var tasks = registrationGrains.Select(grain => grain.ProcessRegistration(status));
                await Task.WhenAll(tasks);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}
