﻿using BrightSkool.BusinessLayer.Dependency;
using BrightSkool.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrightSkool.BusinessLayer.OrleansClient.Registration
{
    public interface IRegistrationClient : ITransiantDependency
    {
        Task<List<RegistrationModel>> RegistrationsList();

        Task<RegistrationModel> GetRegistrationbyID(Guid Id);

        Task<bool> SaveRegistration(RegistrationModel registration);

        Task<bool> ProcessRegisteredApplicants(List<Guid> applicants, string status);

    }
}
