﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrightSkool.BusinessLayer.OrleansClient.AppUtils
{
    public class Constants
    {
        public const string SchoolProfileList = "SchoolProfileList";
        public const string RegistrationList = "RegisteredStudentsList";

        public const string CountryList = "CountryList";
        public const string CurrencyList = "CurrencyList";
        public const string RaceList = "RaceList";
        public const string ReligionList = "ReligionList";
        public const string NationalityList = "NationalityList";
        public const string LanguageList = "LanguageList";
        public const string RelationshipList = "RelationshipList";
    }
}
