﻿using BrightSkool.BusinessLayer.OrleansClient.AppUtils;
using BrightSkool.GrainInterface.Masters;
using Orleans;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static BrightSkool.Domain.Models.CommonModel;

namespace BrightSkool.BusinessLayer.OrleansClient.Masters
{
    public class LanguageClient : ILanguageClient
    {
        public async Task<List<Language>> GetLanguages()
        {
            List<Language> languages = new List<Language>();
            try
            {
                var languagesGrain = GrainClient.GrainFactory.GetGrain<ILanguage>(Constants.LanguageList);
                if (languagesGrain != null)
                    languages = await languagesGrain.GetItem();
            }
            catch (Exception)
            {
                //_exceptionHandler.HandleException(ex);
            }
            return languages;
        }
       
    }
}
