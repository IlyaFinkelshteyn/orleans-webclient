﻿using BrightSkool.BusinessLayer.OrleansClient.AppUtils;
using BrightSkool.GrainInterface.Masters;
using Orleans;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static BrightSkool.Domain.Models.CommonModel;

namespace BrightSkool.BusinessLayer.OrleansClient.Masters
{
    public class CountryClient : ICountryClient
    {
        public async Task<List<Country>> GetCountries()
        {
            List<Country> countries = new List<Country>();
            try
            {
                var countriesGrain = GrainClient.GrainFactory.GetGrain<ICountry>(Constants.CountryList);
                if (countriesGrain != null)
                    countries = await countriesGrain.GetItem();
            }
            catch (Exception)
            {
                //_exceptionHandler.HandleException(ex);
            }
            return countries;
        }
    }
}
