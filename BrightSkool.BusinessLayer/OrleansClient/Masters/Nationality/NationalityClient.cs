﻿using BrightSkool.BusinessLayer.OrleansClient.AppUtils;
using BrightSkool.GrainInterface.Masters;
using Orleans;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static BrightSkool.Domain.Models.CommonModel;

namespace BrightSkool.BusinessLayer.OrleansClient.Masters
{
    public class NationalityClient : INationalityClient
    {
        public async Task<List<Nationality>> GetNationalities()
        {
            List<Nationality> nationalities = new List<Nationality>();
            try
            {
                var nationalitiesGrain = GrainClient.GrainFactory.GetGrain<INationality>(Constants.NationalityList);
                if (nationalitiesGrain != null)
                    nationalities = await nationalitiesGrain.GetItem();
            }
            catch (Exception)
            {
                //_exceptionHandler.HandleException(ex);
            }
            return nationalities;
        }
    }
}
