﻿using BrightSkool.BusinessLayer.Dependency;
using System.Collections.Generic;
using System.Threading.Tasks;
using static BrightSkool.Domain.Models.CommonModel;

namespace BrightSkool.BusinessLayer.OrleansClient.Masters
{
    public interface INationalityClient : ITransiantDependency
    {
        Task<List<Nationality>> GetNationalities();
    }
}
