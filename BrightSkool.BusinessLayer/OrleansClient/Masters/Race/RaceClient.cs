﻿using BrightSkool.BusinessLayer.OrleansClient.AppUtils;
using BrightSkool.GrainInterface.Masters;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BrightSkool.Domain.Models.CommonModel;

namespace BrightSkool.BusinessLayer.OrleansClient.Masters
{
    public class RaceClient : IRaceClient
    {
        public async Task<List<Race>> GetRace()
        {
            List<Race> race = new List<Race>();
            try
            {
                var raceGrain = GrainClient.GrainFactory.GetGrain<IRace>(Constants.RaceList);
                if (raceGrain != null)
                    race = await raceGrain.GetItem();
            }
            catch (Exception)
            {
                //_exceptionHandler.HandleException(ex);
            }
            return race;
        }
    }
}
