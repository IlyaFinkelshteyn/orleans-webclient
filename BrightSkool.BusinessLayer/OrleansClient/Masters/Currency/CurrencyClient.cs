﻿using BrightSkool.BusinessLayer.OrleansClient.AppUtils;
using BrightSkool.GrainInterface.Masters;
using Orleans;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static BrightSkool.Domain.Models.CommonModel;

namespace BrightSkool.BusinessLayer.OrleansClient.Masters
{
    public class CurrencyClient : ICurrencyClient
    {
        public async Task<List<Currency>> GetCurrencies()
        {
            List<Currency> currencies = new List<Currency>();
            try
            {
                var currenciesGrain = GrainClient.GrainFactory.GetGrain<ICurrency>(Constants.CurrencyList);
                if (currenciesGrain != null)
                    currencies = await currenciesGrain.GetItem();
            }
            catch (Exception)
            {
                //_exceptionHandler.HandleException(ex);
            }
            return currencies;
        }
    }
}
