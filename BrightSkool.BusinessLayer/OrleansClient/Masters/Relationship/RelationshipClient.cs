﻿using BrightSkool.BusinessLayer.OrleansClient.AppUtils;
using BrightSkool.GrainInterface.Masters;
using Orleans;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static BrightSkool.Domain.Models.CommonModel;

namespace BrightSkool.BusinessLayer.OrleansClient.Masters
{
    public class RelationshipClient : IRelationshipClient
    {
        public async Task<List<Relationship>> GetRelationships()
        {
            List<Relationship> relationships = new List<Relationship>();
            try
            {
                var relationshipsGrain = GrainClient.GrainFactory.GetGrain<IRelationship>(Constants.RelationshipList);
                if (relationshipsGrain != null)
                    relationships = await relationshipsGrain.GetItem();
            }
            catch (Exception)
            {
                //_exceptionHandler.HandleException(ex);
            }
            return relationships;
        }
    }
}
