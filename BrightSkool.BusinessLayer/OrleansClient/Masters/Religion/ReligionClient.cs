﻿using BrightSkool.BusinessLayer.OrleansClient.AppUtils;
using BrightSkool.GrainInterface.Masters;
using Orleans;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static BrightSkool.Domain.Models.CommonModel;

namespace BrightSkool.BusinessLayer.OrleansClient.Masters
{
    public class ReligionClient : IReligionClient
    {
        public async Task<List<Religion>> GetReligions()
        {
            List<Religion> countries = new List<Religion>();
            try
            {
                var religionsGrain = GrainClient.GrainFactory.GetGrain<IReligion>(Constants.ReligionList);
                if (religionsGrain != null)
                    countries = await religionsGrain.GetItem();
            }
            catch (Exception)
            {
                //_exceptionHandler.HandleException(ex);
            }
            return countries;
        }
    }
}
