﻿using BrightSkool.Domain.Models;
using System;
using System.IO;
using System.Web;
using System.Collections.Generic;

namespace BrightSkool.BusinessLayer.BAL.FileUpload
{
    public class FileUploadService : IFileUploadService
    {
        public List<FileUploadModel> SaveDocuments(HttpFileCollectionBase uploadDocuments, string applicationPath, string virtualPath, int? skipIndex)
        {
            List<FileUploadModel> lstUploadFileModel = new List<FileUploadModel>();
            int counter = 0;
            if (uploadDocuments.Count > 0)
            {
                if (uploadDocuments.Count > 0)
                {
                    for (int i = 0; i < uploadDocuments.Keys.Count; i++)
                    {
                        if (uploadDocuments[i].ContentLength > 0)
                        {
                            if (skipIndex.HasValue && counter == 0)
                            {
                                if (skipIndex.Value == i) { counter++; continue; }
                            }
                            else
                            {
                                FileUploadModel oUploadFileModel = new FileUploadModel();
                                var fileName = Guid.NewGuid().ToString() + "-" + uploadDocuments[i].FileName;
                                uploadDocuments[i].SaveAs(Path.Combine(applicationPath, fileName));
                                oUploadFileModel.FileName = fileName;
                                oUploadFileModel.FileVirtualPath = virtualPath + "/" + fileName;
                                lstUploadFileModel.Add(oUploadFileModel);
                            }
                        }
                    }
                }
            }
            return lstUploadFileModel;
        }

        public FileUploadModel SavePicture(HttpPostedFileBase uploadfile, string applicationPath, string virtualPath)
        {
            FileUploadModel fileUploadModel = null;
            if (uploadfile != null)
            {
                if (uploadfile.ContentLength > 0)
                {
                    fileUploadModel = new FileUploadModel();
                    var fileName = Guid.NewGuid().ToString() + "-" + uploadfile.FileName;
                    uploadfile.SaveAs(Path.Combine(applicationPath, fileName));
                    fileUploadModel.FileName = uploadfile.FileName;
                    fileUploadModel.ModifiedFileName = fileName;
                    fileUploadModel.FileVirtualPath = virtualPath + "/" + fileName;
                }
            }
            return fileUploadModel;
        }
    }
}
