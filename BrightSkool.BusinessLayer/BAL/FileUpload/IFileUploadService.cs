﻿using BrightSkool.BusinessLayer.Dependency;
using BrightSkool.Domain.Models;
using System.Collections.Generic;
using System.Web;

namespace BrightSkool.BusinessLayer.BAL.FileUpload
{
    public interface IFileUploadService:ITransiantDependency
    {
        FileUploadModel SavePicture(HttpPostedFileBase uploadfile, string applicationPath, string virtualPath);

        List<FileUploadModel> SaveDocuments(HttpFileCollectionBase uploadDocuments, string applicationPath, string virtualPath, int? skipIndex);
    }
}
