﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrightSkool.Domain.Models;
using BrightSkool.BusinessLayer.OrleansClient.Registration;
using BrightSkool.BusinessLayer.OrleansClient.Masters;

namespace BrightSkool.BusinessLayer.BAL.Masters
{
    public class LanguageService : ILanguageService
    {
        ILanguageClient _languageClient;
        public LanguageService(ILanguageClient languageClient)
        {
            _languageClient = languageClient;
        }

        public async Task<List<CommonModel.Language>> GetLanguages()
        {
            var result = await _languageClient.GetLanguages();
            return result;
        }
    }
}
