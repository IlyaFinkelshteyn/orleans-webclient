﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrightSkool.Domain.Models;
using BrightSkool.BusinessLayer.OrleansClient.Registration;
using BrightSkool.BusinessLayer.OrleansClient.Masters;

namespace BrightSkool.BusinessLayer.BAL.Masters
{
    public class NationalityService : INationalityService
    {
        INationalityClient _nationalityClient;
        public NationalityService(INationalityClient nationalityClient)
        {
            _nationalityClient = nationalityClient;
        }

        public async Task<List<CommonModel.Nationality>> GetNationalities()
        {
            var result = await _nationalityClient.GetNationalities();
            return result;
        }
    }
}
