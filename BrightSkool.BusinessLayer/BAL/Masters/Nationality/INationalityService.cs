﻿using BrightSkool.BusinessLayer.Dependency;
using BrightSkool.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrightSkool.BusinessLayer.BAL.Masters
{
    public interface INationalityService : ITransiantDependency
    {
        Task<List<CommonModel.Nationality>> GetNationalities();
    }
}
