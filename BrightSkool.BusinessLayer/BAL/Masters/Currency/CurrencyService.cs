﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrightSkool.Domain.Models;
using BrightSkool.BusinessLayer.OrleansClient.Registration;
using BrightSkool.BusinessLayer.OrleansClient.Masters;

namespace BrightSkool.BusinessLayer.BAL.Masters
{
    public class CurrencyService : ICurrencyService
    {
        ICurrencyClient _currencyClient;
        public CurrencyService(ICurrencyClient currencyClient)
        {
            _currencyClient = currencyClient;
        }

        public async Task<List<CommonModel.Currency>> GetCurrencies()
        {
            var result = await _currencyClient.GetCurrencies();
            return result;
        }
    }
}
