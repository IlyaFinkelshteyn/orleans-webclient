﻿using BrightSkool.BusinessLayer.Dependency;
using BrightSkool.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrightSkool.BusinessLayer.BAL.Masters
{
    public interface ICurrencyService : ITransiantDependency
    {
        Task<List<CommonModel.Currency>> GetCurrencies();
    }
}
