﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrightSkool.Domain.Models;
using BrightSkool.BusinessLayer.OrleansClient.Registration;
using BrightSkool.BusinessLayer.OrleansClient.Masters;

namespace BrightSkool.BusinessLayer.BAL.Masters
{
    public class ReligionService : IReligionService
    {
        IReligionClient _religionClient;
        public ReligionService(IReligionClient religionClient)
        {
            _religionClient = religionClient;
        }

        public async Task<List<CommonModel.Religion>> GetReligions()
        {
            var result = await _religionClient.GetReligions();
            return result;
        }
    }
}
