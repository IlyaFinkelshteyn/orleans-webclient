﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrightSkool.Domain.Models;
using BrightSkool.BusinessLayer.OrleansClient.Registration;
using BrightSkool.BusinessLayer.OrleansClient.Masters;

namespace BrightSkool.BusinessLayer.BAL.Masters
{
    public class RaceService : IRaceService
    {
        IRaceClient _raceClient;
        public RaceService(IRaceClient raceClient)
        {
            _raceClient = raceClient;
        }

        public async Task<List<CommonModel.Race>> GetRace()
        {
            var result = await _raceClient.GetRace();
            return result;
        }
    }
}
