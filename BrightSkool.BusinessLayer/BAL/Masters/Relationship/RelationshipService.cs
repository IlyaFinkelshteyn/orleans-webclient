﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrightSkool.Domain.Models;
using BrightSkool.BusinessLayer.OrleansClient.Registration;
using BrightSkool.BusinessLayer.OrleansClient.Masters;

namespace BrightSkool.BusinessLayer.BAL.Masters
{
    public class RelationshipService : IRelationshipService
    {
        IRelationshipClient _relationshipClient;
        public RelationshipService(IRelationshipClient relationshipClient)
        {
            _relationshipClient = relationshipClient;
        }

        public async Task<List<CommonModel.Relationship>> GetRelationships()
        {
            var result = await _relationshipClient.GetRelationships();
            return result;
        }
    }
}
