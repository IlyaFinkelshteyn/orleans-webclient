﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrightSkool.Domain.Models;
using BrightSkool.BusinessLayer.OrleansClient.Registration;
using BrightSkool.BusinessLayer.OrleansClient.Masters;

namespace BrightSkool.BusinessLayer.BAL.Masters
{
    public class CountryService : ICountryService
    {
        ICountryClient _countryClient;
        public CountryService(ICountryClient countryClient)
        {
            _countryClient = countryClient;
        }

        public async Task<List<CommonModel.Country>> GetCountries()
        {
            var result = await _countryClient.GetCountries();
            return result;
        }
    }
}
