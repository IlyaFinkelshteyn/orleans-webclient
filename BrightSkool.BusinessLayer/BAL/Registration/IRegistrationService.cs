﻿using BrightSkool.BusinessLayer.Dependency;
using BrightSkool.Domain.Dto;
using BrightSkool.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrightSkool.BusinessLayer.BAL.Registration
{
    public interface IRegistrationService : ITransiantDependency
    {
        Task<List<RegistrationModel>> RegistrationsList ();

        Task<RegistrationModel> GetRegistrationbyID (Guid Id);

        Task<bool> SaveRegistration (RegistrationModel registration);

        Task<bool> ProcessRegistrationApplicants (List<Guid> registrations, string applicantStatus);

        Task<List<RegistrationDto>> ProcessRegistrations ();
    }
}
