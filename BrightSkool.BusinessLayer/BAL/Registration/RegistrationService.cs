﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrightSkool.Domain.Models;
using BrightSkool.BusinessLayer.OrleansClient.Registration;
using BrightSkool.Domain.Dto;
using BrightSkool.BusinessLayer.BAL.AppUtils;

namespace BrightSkool.BusinessLayer.BAL.Registration
{
    public class RegistrationService : IRegistrationService
    {
        IRegistrationClient _registrationClient;
        public RegistrationService (IRegistrationClient registrationClient)
        {
            _registrationClient = registrationClient;
        }
        public async Task<RegistrationModel> GetRegistrationbyID (Guid Id)
        {
            var result = await _registrationClient.GetRegistrationbyID(Id);
            return result;
        }

        public async Task<List<RegistrationModel>> RegistrationsList ()
        {
            var result = await _registrationClient.RegistrationsList();
            return result;
        }

        public async Task<bool> SaveRegistration (RegistrationModel registration)
        {
            //Assigning current date to registration model
            registration.RegisteredDate = DateTime.Now;
            registration.Status.Status = Constants.newlyRegistered;
            registration.Status.Code = Constants.newlyRegistered;
            var result = await _registrationClient.SaveRegistration(registration);
            return result;
        }

        //To get process registration detail.
        public async Task<List<RegistrationDto>> ProcessRegistrations ()
        {
            List<RegistrationDto> processRegistrations = new List<RegistrationDto>();
            List<RegistrationModel> lstRegistrationModel = await _registrationClient.RegistrationsList();
            foreach (var items in lstRegistrationModel)
            {
                if (items.Status != null)
                {
                    processRegistrations.Add(new RegistrationDto { Id = items.Id, Applicant = new CommonModel.Person { FirstName = items.Applicant.FirstName, LastName = items.Applicant.LastName, MiddleName = items.Applicant.MiddleName }, Level = items.Level, Age = DateTime.Now.Year - items.Applicant.DOB.Year, DistanceToSchool = 5, Nationality = new CommonModel.Nationality { CountryNationality = items.Nationality.CountryNationality }, RegisteredDate = items.RegisteredDate, Sibbling = items.StudentId });
                }

            }

            return processRegistrations;
        }

        //public async Task<bool> ProcessRegistrationApplicants(List<RegistrationModel> registrations, string applicantStatus)
        public async Task<bool> ProcessRegistrationApplicants (List<Guid> applicantsIds, string applicantStatus)
        {
            bool result = true;
            result = await _registrationClient.ProcessRegisteredApplicants(applicantsIds, applicantStatus);
            return result;
        }
    }
}
