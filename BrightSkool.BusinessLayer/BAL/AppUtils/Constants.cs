﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrightSkool.BusinessLayer.BAL.AppUtils
{
    public static class Constants
    {
        public const string newlyRegistered= "New";
        public const string selected = "Selected";
        public const string waitListed = "Waitlisted";
        public const string rejected = "Rejected";
    }
}
