﻿using BrightSkool.BusinessLayer.BAL.FileUpload;
using BrightSkool.BusinessLayer.BAL.Masters;
using BrightSkool.BusinessLayer.BAL.Registration;
using BrightSkool.BusinessLayer.OrleansClient.Registration;
using BrightSkool.Domain.Dto;
using BrightSkool.Domain.Models;
using BrightSkool.Registration.AppUtils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace BrightSkool.Registration.Controllers
{
    public class RegistrationController : Controller
    {
        IRegistrationService _registrationService;
        ICountryService _countryService;
        IFileUploadService _fileUploadService;
        //IGenderService _genderService;
        INationalityService _nationalityService;
        IRelationshipService _relationshipService;
        ILanguageService _languageService;
        IReligionService _religionService;
        //public RegistrationController(IRegistrationService registrationService, ICountryService countryService, IFileUploadService fileUploadService)
        public RegistrationController (IRegistrationService registrationService, ICountryService countryService, IFileUploadService fileUploadService, INationalityService nationalityService, IRelationshipService relationShipService, ILanguageService languageService, IReligionService religionService)
        {
            _registrationService = registrationService;
            _countryService = countryService;
            _fileUploadService = fileUploadService;
            _nationalityService = nationalityService;
            _relationshipService = relationShipService;
            _languageService = languageService;
            _religionService = religionService;
        }
        public ActionResult Index ()
        {
            return View();
        }

        public async Task<ActionResult> Registration ()
        {
            Task<List<CommonModel.Country>> GetCountriesTask = _countryService.GetCountries();
            Task<List<CommonModel.Nationality>> GetNationalitiesTask = _nationalityService.GetNationalities();
            Task<List<CommonModel.Relationship>> GetRelationshipsTask = _relationshipService.GetRelationships();
            Task<List<CommonModel.Religion>> GetReligionsTask = _religionService.GetReligions();
            //Task<List<CommonModel.Religion>> GetReligionsTask = _religionService.GetReligions();
            //List of Gender to fill Gender dropdown in Registration Form -- Added by Rajesh Gengatharan 07/11/16
            ViewBag.Gender =
            new List<SelectListItem> {
            new SelectListItem { Text="Male", Value = "Male"},
            new SelectListItem { Text="Female", Value = "Female"}
            };
            ViewBag.Countries = await GetCountriesTask;
            ViewBag.Nationalities = await GetNationalitiesTask;
            ViewBag.Relationships = await GetRelationshipsTask;
            ViewBag.Religions = await GetReligionsTask;
            return View("Registration", new RegistrationDto());
        }
        public async Task<ActionResult> SaveOrUpdateModel (RegistrationDto registrationDtoModel, HttpPostedFileBase profilepicture)
        {
            registrationDtoModel.Applicant.DOB.Year = registrationDtoModel.DOB.Year;
            registrationDtoModel.Applicant.DOB.Date = registrationDtoModel.DOB.Day;
            registrationDtoModel.Applicant.DOB.Month = registrationDtoModel.DOB.Month;
            string physicalWebRootPath = Server.MapPath(Constants.RegistrationProfilePictureVirtualPath);
            string physicalDocumentWebRootPath = Server.MapPath(Constants.RegistrationDocumentVirtualPath);
            int? profilePictureIndex = GetProfilePictureIndex(Request.Files);
            FileUploadModel fileUploadModel = new FileUploadModel();
            List<CommonModel.Guardian> lstGuardian = new List<CommonModel.Guardian>();
            List<StudentDocuments> studentDocuments = new List<StudentDocuments>();
            var contactPersonData = Request.Form["contactpersons"];
            var uploadDocumentsData = Request.Form["uploadeddocs"];
            if (uploadDocumentsData != null)
            {
            }
            if (contactPersonData != null)
            {
                lstGuardian = JsonConvert.DeserializeObject<List<CommonModel.Guardian>>(contactPersonData);
                registrationDtoModel.GuardianDetails = lstGuardian;
            }
            //saving profile picture
            if (profilepicture != null && profilepicture.ContentLength > 0)
            {
                fileUploadModel = _fileUploadService.SavePicture(profilepicture, physicalWebRootPath, Constants.RegistrationProfilePictureVirtualPath);
                registrationDtoModel.Applicant.ProfilePicFilePath = fileUploadModel.FileVirtualPath;
            }
            //Save Registration Documents To Physical Path
            List<FileUploadModel> lstUploadFileModel = _fileUploadService.SaveDocuments(Request.Files, physicalDocumentWebRootPath, Constants.RegistrationDocumentVirtualPath, profilePictureIndex);
            if (lstUploadFileModel.Count > 0)
            {
                foreach (var uploadedFile in lstUploadFileModel)
                {
                    StudentDocuments documents = new StudentDocuments();
                    documents.Name = uploadedFile.FileName;
                    documents.FilePath = uploadedFile.FileVirtualPath;
                    studentDocuments.Add(documents);
                }
                registrationDtoModel.UploadedDocuments = studentDocuments;
            }

            var result = await _registrationService.SaveRegistration(registrationDtoModel);
            ViewBag.message = result ? "Success" : "Failed";
            return result ? RedirectToAction("Index", "Home") : RedirectToAction("Registration", registrationDtoModel);
        }

        public async Task<ActionResult> ProcessRegistration ()
        {
            List<RegistrationDto> processRegistrations = new List<RegistrationDto>();
            processRegistrations = await _registrationService.ProcessRegistrations();
            return await Task.Run(() => View(processRegistrations));
        }

        //public async Task<JsonResult> ProcessRegistrationApplicants(List<RegistrationModel> applicants, string applicantStatus)
        [HttpPost]
        public async Task<JsonResult> ProcessRegistrationApplicants (List<Guid> applicantsIds, string applicantStatus)
        {
            bool result = false;
            applicantsIds = applicantsIds ?? new List<Guid>();
            if (applicantsIds.Count > 0)
            {
                result = await _registrationService.ProcessRegistrationApplicants(applicantsIds, applicantStatus);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private int? GetProfilePictureIndex (HttpFileCollectionBase files)
        {
            int? index = null;
            if (Request.Files.Count > 0)
            {
                for (int i = 0; i < Request.Files.Keys.Count; i++)
                {
                    if (Request.Files[i].ContentLength > 0)
                    {
                        if (Request.Files.Keys[i].ToUpper().Equals("PROFILEPICTURE"))
                        {
                            index = i;
                            break;
                        }
                    }
                }
            }
            return index;
        }
    }
}