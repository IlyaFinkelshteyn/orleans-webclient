﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrightSkool.Registration.AppUtils
{
    public class Constants
    {
        public const string RegistrationProfilePictureVirtualPath = "/App_Uploads/Registration_Picture";
        public const string RegistrationDocumentVirtualPath = "/App_Uploads/Registration_Documents";
    }
   
}