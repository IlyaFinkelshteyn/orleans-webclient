@ECHO OFF

REM Setup Browser
IF EXIST %USERPROFILE%\AppData\Local\Google\Chrome\Application\chrome.exe SET Browser=%USERPROFILE%\AppData\Local\Google\Chrome\Application\chrome.exe
IF EXIST "%ProgramFiles(x86)%\Google\Chrome\Application\chrome.exe" SET Browser=%ProgramFiles(x86)%\Google\Chrome\Application\chrome.exe

REM Setup OpenCover & RepportGenerator & xUnit
SET OpenCover=packages\OpenCover.4.6.519\tools\OpenCover.Console.exe
SET ReportGenerator=packages\ReportGenerator.2.5.1\tools\ReportGenerator.exe
SET xUnit=packages\xunit.runner.console.2.1.0\tools\xunit.console.exe

REM Setup Output Directory
IF NOT EXIST codecoverage MKDIR codecoverage
del codecoverage\*.* /Q

ECHO Execute Tests with code coverage...
%OpenCover% -register:"user" ^
			-target:"%xUnit%" ^
			-targetargs:"BrightSkool.BusinessLayer.Test\bin\Debug\BrightSkool.BusinessLayer.Test.dll" ^
			-output:"codecoverage/MyUnitTests.Results.xml" ^
			-searchdirs:"BrightSkool.BusinessLayer.Test\bin\Debug"
			-filter:"+[BrightSkool.BusinessLayer]*"
ECHO.

ECHO Generate Code Coverage Reports...
%ReportGenerator% -reports:"codecoverage/*Results.xml" -targetdir:"codecoverage/"
ECHO.

REM View Report
"%Browser%" "%CD%\codecoverage\index.htm"
PAUSE