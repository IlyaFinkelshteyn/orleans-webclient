﻿var g_CountryIndex = -1;
var g_StateIndex = -1;
(function () {
    $(function () {
        $('#txtStartTime').timepicker();
        //$('#txtStartTime').val('');
        $('#txtEndTime').timepicker();
        //$('#txtEndTime').val('')

        
    });

    $('.schoolProfile-form').validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            "SchoolName": {
                required: true
            },
            "SchoolAddress.AddressLine1": {
                required: true
            },
            "City.CityName": {
                required: true
            },
            "Country.CountryName": {
                required: true
            },
            "SchoolAddress.ZipCode": {
                required: true
            },
            "SchoolPhone[0].PhoneNumber": {
                required: true
            }

        },

        messages: {
            "SchoolName": "Please enter school name.",
            "SchoolAddress.AddressLine1": "Please enter address line 1.",
            "City.CityName": "Please enter city name.",
            "Country.CountryName": "Please enter country name",
            "SchoolAddress.ZipCode": "Please enter zipcode.",
            "SchoolPhone[0].PhoneNumber": "Please enter primary phone number."
        },

        invalidHandler: function (event, validator) {

        },

        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },

        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },

        errorPlacement: function (error, element) {
            if (element.closest('.input-icon').size() === 1) {
                error.insertAfter(element.closest('.input-icon'));
            } else {
                error.insertAfter(element);
            }
        }

        //submitHandler: function (form) {
        //    form.submit();
        //}
    });

    $.fn.regexMask = function (mask) {
        $(this).keypress(function (event) {
            if (!event.charCode) return true;
            var part1 = this.value.substring(0, this.selectionStart);
            var part2 = this.value.substring(this.selectionEnd, this.value.length);
            if (!mask.test(part1 + String.fromCharCode(event.charCode) + part2))
                return false;
        });
    };

    $('#txtSchoolName').regexMask(new RegExp('^[-A-Za-z0-9.,\' ]*$'));
    $('#txtRegistrationNumber').regexMask(new RegExp('^[-A-Za-z0-9.,\' ]*$'));
    $('#txtbusinessregistrationnumber').regexMask(new RegExp('^[-A-Za-z0-9.,\' ]*$'));
    $('#txtotherregistnumber').regexMask(new RegExp('^[-A-Za-z0-9.,\' ]*$'));
    $('#txtCity').regexMask(new RegExp('^[A-Za-z ]*$'));
    $('#txtState').regexMask(new RegExp('^[A-Za-z ]*$'));
    $('#txtcountry').regexMask(new RegExp('^[A-Za-z ]*$'));
    $('#txtZipCode').regexMask(new RegExp('^[0-9 ]*$'));
    $('#SchoolPhone_0__PhoneNumber').regexMask(new RegExp('^[0-9 ]*$'));
    $('#SchoolPhone_1__PhoneNumber').regexMask(new RegExp('^[0-9 ]*$'));
    $('#SchoolFax_0__PhoneNumber').regexMask(new RegExp('^[0-9 ]*$'));
    $('#txtmessage').regexMask(new RegExp('^[-A-Za-z0-9.,\' ]*$'));
    $('#txtaboutus').regexMask(new RegExp('^[-A-Za-z0-9.,\' ]*$'));
    $('#txtmoto').regexMask(new RegExp('^[-A-Za-z0-9.,\' ]*$'));

    $("#txtcountry").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/SchoolProfile/GetCountries",
                type: "POST",
                dataType: "json",
                data: { Prefix: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item.Name, value: item.Name };
                    }))

                }
            })
        },
        messages: {
            noResults: "", results: ""
        }
    });

    $("#txtcurrency").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/SchoolProfile/GetCurrencies",
                type: "POST",
                dataType: "json",
                data: { Prefix: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item.Name, value: item.Name };
                    }))

                }
            })
        },
        messages: {
            noResults: "", results: ""
        }
    });



})();

function getCountryList(countryList) {
    debugger;
    //var countries = JSON.parse(countryList);
    $('#hfCountryList').val(countryList);
}

function FillStateByCountry() {
    debugger;
    var countryName = $("#ddlCountry").val();
    if (countryName != '' || countryName != undefined) {
        $("#ddlState").addClass("edited");
        var procemessage = "<option value=''> Please wait...</option>";
        $("#ddlState").html(procemessage).show();
        procemessage = "<option value=''>--Select--</option>";
        $("#ddlCity").html(procemessage).show();
        $("#ddlCity").removeClass("edited");
        var countryList = JSON.parse($('#hfCountryList').val());
        var stateList = [];
        var countryIndex = -1;
        var markup = "<option value=''>--Select--</option>";
        $.each(countryList, function (key, value) {
            if (value.CountryCode == countryName) {
                countryIndex = key;
                g_CountryIndex = key;
                return false;
            }
        });
        if (countryIndex >= 0) {
            stateList = countryList[countryIndex].CountryStates;
            if (stateList.length > 0) {
                $.each(stateList, function (key, value) {
                    debugger;
                    markup += "<option value='" + value.StateName + "'>" + value.StateName + "</option>";
                });
            }
            $("#ddlState").html(markup).show();
        }
        else {
            $("#ddlState").html(markup).show();
        }
        $("#ddlState").removeClass("edited");
    }
}

function FillCityByState() {
    debugger;
    var stateName = $("#ddlState").val();
    if (stateName != '' || stateName != undefined) {
        $("#ddlCity").addClass("edited");
        var procemessage = "<option value=''> Please wait...</option>";
        $("#ddlCity").html(procemessage).show();
        var countryList = JSON.parse($('#hfCountryList').val());
        var stateList = countryList[g_CountryIndex].CountryStates;
        var cityList = [];
        var stateIndex = -1;
        var markup = "<option value=''>--Select--</option>";
        if (g_CountryIndex>=0) {
            $.each(stateList, function (key, value) {
                if (value.StateName == stateName) {
                    stateIndex = key;
                    g_StateIndex = key;
                    return false;
                }
            });
        }
        //else {
        //    $.each(countryList, function (key, value) {
        //        if (value.CountryCode == countryName) {
        //            stateIndex = key;
        //        }
        //    });
        //}
        if (stateIndex >= 0) {
            cityList = stateList[stateIndex].StateCity;
            if (cityList.length > 0) {
                $.each(cityList, function (key, value) {
                    markup += "<option value=" + value.CityName + ">" + value.CityName + "</option>";
                });
            }
            $("#ddlCity").html(markup).show();
        }
        else {
            $("#ddlCity").html(markup).show();
        }
        $("#ddlCity").removeClass("edited");
    }
}

function checkEmptyGuid(guid)
{
    debugger;
    var emptyGuid = /^({|()?0{8}-(0{4}-){3}0{12}(}|))?$/;
    var result = emptyGuid.test(guid);
    return result;
}

function selectDropdowns(guid, state, city) {
    debugger;
    var result = checkEmptyGuid(guid);
    if (!result) {
        var countryName = $("#ddlCountry").val();
        if (countryName != '' || countryName != undefined) {
            var countryList = JSON.parse($('#hfCountryList').val());
            var countryIndex = -1;
            var markup = "<option value='--Select--'></option>";
            $.each(countryList, function (key, value) {
                if (value.CountryCode == countryName) {
                    countryIndex = key;
                    g_CountryIndex = key;
                    return false;
                }
            });
            FillStateByCountry();
            $("#ddlState").addClass("edited");
            $('#ddlState').val(state);
            FillCityByState();
            $("#ddlCity").addClass("edited");
            $('#ddlCity').val(city);
        }
    }
    else
    {
    }
}