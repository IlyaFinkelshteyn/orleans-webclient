﻿var _$app = angular.module("registrationapp", []);
_$app.controller("registrationcontroller", ["$scope", function ($scope) {
    $scope.details = [];
    $scope.PhoneDetail = {};
    $scope.fileList = [];
    $scope.curFile;
    $scope.ImageProperty = {
        file: ''
    }

    $scope.addRow = function (isValid) {
        debugger;
        if (isValid)
            //if ($scope.firstName != '' && $scope.relationship != '' && $scope.GuardianPhone.PhoneNumber != '' && $scope.Email == '') {
        {
            $scope.details.push({ 'firstName': $scope.firstName, 'middleName': $scope.middleName, 'lastName': $scope.lastName, 'relationship': $scope.relationship, 'GuardianPhone.PhoneNumber': $scope.GuardianPhone.PhoneNumber, 'GuardianBusinessPhone.PhoneNumber': $scope.GuardianBusinessPhone.PhoneNumber, 'Email': $scope.Email });
            //Creating object for Gurdian Personal phone
            $scope.PhoneDetail = {
                PhoneNumber:$scope.GuardianPhone.PhoneNumber
            };
            $scope.details[$scope.details.length - 1].GuardianPhone = $scope.PhoneDetail;
            //Creating object for Gurdian Bussiness phone
            $scope.PhoneDetail = {
                PhoneNumber: $scope.GuardianBusinessPhone.PhoneNumber
            };
            $scope.details[$scope.details.length - 1].GuardianBusinessPhone = $scope.PhoneDetail;

            $scope.firstName = '';
            $scope.middleName = '';
            $scope.lastName = '';
            $scope.relationship = '';
            $scope.GuardianPhone.PhoneNumber = '';
            $scope.GuardianBusinessPhone.PhoneNumber = '';
            $scope.Email = '';


            $('#txtcpfirstname').removeClass('edited');
            $('#txtcpmiddlename').removeClass('edited');
            $('#txtcplastname').removeClass('edited');
            $('#ddlrelationship').removeClass('edited');
            $('#txtcontactmobile').removeClass('edited');
            $('#txtcontactphone').removeClass('edited');
            $('#txtcontactemail').removeClass('edited');
            $("#hfcontactperson").val(JSON.stringify($scope.details))
        }
    }

    $scope.removeRow = function (idx) {
        swal(
            {
                title: "Are you sure?",
                text: "You will not be able to recover this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, remove it!",
                closeOnConfirm: false
            },
            function (isConfirm) {

                if (isConfirm) {
                    swal("Removed!", "Contact Person is been removed.", "success");
                    $scope.details.splice(idx, 1);
                    $("#hfcontactperson").val(JSON.stringify($scope.details))
                }
            });
    };

    $scope.setFile = function (element) {
        debugger;
        $scope.fileList = [];
        // get the files
        var files = element.files;
        for (var i = 0; i < files.length; i++) {
            $scope.ImageProperty.file = files[i];
            $scope.ImageProperty.file.cancel = true;
            $scope.fileList.push($scope.ImageProperty);
            $scope.ImageProperty = {};
            $scope.$apply();

        }
    }

    $scope.UploadFile = function () {
        //var files = $scope.fileList;
        if ($scope.fileList.length === 0) { alert('Please add files to upload.'); }
        for (var file = 0; file < $scope.fileList.length; file++) {
            if (!$scope.fileList[file].file.isUploaded) {
                $scope.fileList[file].file.uploadpercentage = '100';
                $scope.fileList[file].file.cancel = false;
                $scope.fileList[file].file.isUploaded = true;
            }
        }
        $("#hfuploaddocs").val(JSON.stringify($scope.fileList))
    }

    $scope.removeFile = function (idx) {
        $scope.fileList.splice(idx, 1);
        $("#hfuploaddocs").val(JSON.stringify($scope.fileList))
    }

    //$scope.DisplayName = function () { return $scope.firstName + '' + $scope.lastName; }

}]);
