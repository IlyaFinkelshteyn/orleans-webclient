﻿var g_CountryIndex = -1;
var g_StateIndex = -1;
$(document).ready(function ($) {
    $('#txtfirstname').regexMask(new RegExp('^[-A-Za-z0-9.,\' ]*$'));
    $('#txtmiddlename').regexMask(new RegExp('^[-A-Za-z0-9.,\' ]*$'));
    $('#txtlastname').regexMask(new RegExp('^[-A-Za-z0-9.,\' ]*$'));
    $('#txtnric').regexMask(new RegExp('^[-A-Za-z0-9.,\' ]*$'));
    $('#txtadmissionstandard').regexMask(new RegExp('^[-A-Za-z0-9.,\' ]*$'));
    $('#txtsecondlanguage').regexMask(new RegExp('^[-A-Za-z0-9.,\' ]*$'));
    $('#txtpmntcity').regexMask(new RegExp('^[-A-Za-z\' ]*$'));
    $('#txtpmntzipcode').regexMask(new RegExp('^[-0-9\' ]*$'));
    $('#txtmobile').regexMask(new RegExp('^[-0-9+\' ]*$'));
});

(function () {
    $('#txtdob').datepicker();
    $('.registration-form').validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            "Applicant.FirstName": { required: true },
            "Applicant.DOB": { required: true },
            "Applicant.Gender": { required: true },
            "PIN.PersonalIDNumber": { required: true },
            "Nationality.CountryNationality": { required: true },
            "StudentAddress.AddressLine1": { required: true },
            "StudentAddress.City": { required: true },
            "StudentAddress.ZipCode": { required: true },
            "StudentAddress.Country": { required: true },
            "PhoneNumber": { required: true },
            "Email": { email: true, required: true },
            "Level": { required: true },
            "SecondLanguage": { required: true },
            "cpfirstname": { required: true }
        },
        messages: {
            "Applicant.FirstName": "Please enter first name.",
            "Applicant.DOB": "Please select Date of Birth.",
            "Applicant.Gender": "Please select gender.",
            "PIN.PersonalIDNumber": "Please enter Personal ID.",
            "Nationality.CountryNationality": "Please select Nationality.",
            "StudentAddress.AddressLine1": "Please enter communication address.",
            "StudentAddress.City": "Please enter City.",
            "StudentAddress.ZipCode": "Please enter PostalCode.",
            "StudentAddress.Country": "Please select Country.",
            "PhoneNumber": "Please enter mobile.",
            "Email": {
                email: "Please enter valid email address.",
                required: "Please enter email address."
            },
            "Level": "Please enter Standard Seaking Admission.",
            "SecondLanguage": "Please enter second language.",
            "cpfirstname": "Please enter first name."
        },

        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },

        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },

        errorPlacement: function (error, element) {
            if (element.closest('.input-icon').size() === 1) {
                error.insertAfter(element.closest('.input-icon'));
            } else {
                error.insertAfter(element);
            }
        }
    });

    $(function () {
        var _$siblingsTable = $('#siblingTable');

        _$siblingsTable.jtable({
            title: '',
            actions: {
                listAction: 'GetContactPersons'
            },
            fields: {
                StudentId: {
                    title: 'Student Id',
                    width: '20%',
                    display: function (data) {
                        var $span = $('<span></span>');
                        $span.append(data.record.FirstName);
                        return $span;
                    }
                },
                Name: {
                    title: 'Name',
                    width: '20%',
                    display: function (data) {
                        var $span = $('<span></span>');
                        $span.append(data.record.Relationship);
                        return $span;
                    }
                },
                Class: {
                    title: 'Class',
                    width: '20%',
                    display: function (data) {
                        var $span = $('<span></span>');
                        $span.append(data.record.Mobile);
                        return $span;
                    }
                },
                Division: {
                    title: 'Division',
                    width: '20%',
                    display: function (data) {
                        var $span = $('<span></span>');
                        $span.append(data.record.Email);
                        return $span;
                    }
                }
            }
        });

    });
    /* START VALIDATE FORM ON CLICK OF NEXT */
    var _$form = $(".registration-form");

    $('#nxt').on('click', function () {
        var fields = _$form.find("#tab_1_1").find(":input");
        if (fields.valid()) {
            moveTab("Next");
        }
    });

    $('#prv').on('click', function () {
        moveTab("Previous");
    });

    $('#nxt1').on('click', function () {
        var fields = _$form.find("#tab_1_2").find(":input");
            moveTab("Next");
    });
    $('#prv1').on('click', function () {
        moveTab("Previous");
    });

    $('#nxt2').on('click', function () {
        var fields = _$form.find("#tab_1_3").find(":input");
        if (fields.valid()) {
            moveTab("Next");
        }
    });
    $('#prv2').on('click', function () {
        moveTab("Previous");
    });

    function moveTab(nextOrPrev) {
        debugger;
        var currentTab = "";
        var currentwindow = "";
        debugger;
        $('.navigation-tabs li').each(function () {
            if ($(this).hasClass('active')) {
                currentTab = $(this);
            }
        });

        $('.tab-pane').each(function () {
            if ($(this).hasClass('active')) {
                currentwindow = $(this);
            }
        });


        if (nextOrPrev == "Next") {

            if (currentwindow.next().length) {
                currentwindow.removeClass('active');
                currentwindow.next().addClass('active');
            }

            if (currentTab.next().length) {
                currentTab.removeClass('active');
                currentTab.next().addClass('active');
            }
            else { } // do nothing for now

        } else {
            if (currentwindow.prev().length) {
                currentwindow.removeClass('active');
                currentwindow.prev().addClass('active');
            }
            if (currentTab.prev().length) {
                currentTab.removeClass('active');
                currentTab.prev().addClass('active');
            }
            else { } //do nothing for now 
        }
    }
    /* START VALIDATE FORM ON CLICK OF NEXT */
})();

function getCountryList(countryList) {
    debugger;
    //var countries = JSON.parse(countryList);
    $('#hfCountryList').val(countryList);
}

function FillStateByCountry() {
    debugger;
    var countryName = $("#ddlCountry").val();
    if (countryName != '' || countryName != undefined) {
        $("#ddlState").addClass("edited");
        var procemessage = "<option value=''> Please wait...</option>";
        $("#ddlState").html(procemessage).show();
        procemessage = "<option value=''>--Select--</option>";
        $("#ddlCity").html(procemessage).show();
        $("#ddlCity").removeClass("edited");
        var countryList = JSON.parse($('#hfCountryList').val());
        var stateList = [];
        var countryIndex = -1;
        var markup = "<option value=''>--Select--</option>";
        $.each(countryList, function (key, value) {
            if (value.CountryCode == countryName) {
                countryIndex = key;
                g_CountryIndex = key;
                return false;
            }
        });
        if (countryIndex >= 0) {
            stateList = countryList[countryIndex].CountryStates;
            if (stateList.length > 0) {
                $.each(stateList, function (key, value) {
                    debugger;
                    markup += "<option value='" + value.StateName + "'>" + value.StateName + "</option>";
                });
            }
            $("#ddlState").html(markup).show();
        }
        else {
            $("#ddlState").html(markup).show();
        }
        $("#ddlState").removeClass("edited");
    }
}

function FillCityByState() {
    debugger;
    var stateName = $("#ddlState").val();
    if (stateName != '' || stateName != undefined) {
        $("#ddlCity").addClass("edited");
        var procemessage = "<option value=''> Please wait...</option>";
        $("#ddlCity").html(procemessage).show();
        var countryList = JSON.parse($('#hfCountryList').val());
        var stateList = countryList[g_CountryIndex].CountryStates;
        var cityList = [];
        var stateIndex = -1;
        var markup = "<option value=''>--Select--</option>";
        if (g_CountryIndex >= 0) {
            $.each(stateList, function (key, value) {
                if (value.StateName == stateName) {
                    stateIndex = key;
                    g_StateIndex = key;
                    return false;
                }
            });
        }
        //else {
        //    $.each(countryList, function (key, value) {
        //        if (value.CountryCode == countryName) {
        //            stateIndex = key;
        //        }
        //    });
        //}
        if (stateIndex >= 0) {
            cityList = stateList[stateIndex].StateCity;
            if (cityList.length > 0) {
                $.each(cityList, function (key, value) {
                    markup += "<option value=" + value.CityName + ">" + value.CityName + "</option>";
                });
            }
            $("#ddlCity").html(markup).show();
        }
        else {
            $("#ddlCity").html(markup).show();
        }
        $("#ddlCity").removeClass("edited");
    }
}


