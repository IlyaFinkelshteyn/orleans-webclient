﻿/******************************Tabs Navigation**********************************/
$(document).ready(function () {
    $(".nav-tabs a").click(function () {
        $(this).tab('show');
    });
});

/******************************New Applicants DataTable**********************************/

var _$newApplicants = $('#tblRegisteredStudentsNew').DataTable({
    columnDefs: [
   { orderable: false, targets: 0 }
    ],
    order: [[1, 'asc']]
});

$('#txtapplicantname').keyup(function () {
    _$newApplicants.column(1).search($(this).val()).draw();
});
$('#txtapplicantnationality').keyup(function () {
    _$newApplicants.column(3).search($(this).val()).draw();
});

$('#txtapplicantdistance').keyup(function () {
    _$newApplicants.column(4).search($(this).val()).draw();
});

$('#txtapplicantclass').keyup(function () {
    _$newApplicants.column(2).search($(this).val()).draw();
});

$('#txtapplicantage').keyup(function () {
    _$newApplicants.column(5).search($(this).val()).draw();
});

/******************************Selected Applicants DataTable**********************************/

var _$selectedApplicants = $('#tblRegisteredStudentsSelected').DataTable({
    columnDefs: [
   { orderable: false, targets: 0 }
    ],
    order: [[1, 'asc']]
});

$('#txtselectedapplicantname').keyup(function () {
    _$selectedApplicants.column(1).search($(this).val()).draw();
});
$('#txtselectedapplicantnationality').keyup(function () {
    _$selectedApplicants.column(3).search($(this).val()).draw();
});

$('#txtselectedapplicantdistance').keyup(function () {
    _$selectedApplicants.column(4).search($(this).val()).draw();
});

$('#txtselectedapplicantclass').keyup(function () {
    _$selectedApplicants.column(2).search($(this).val()).draw();
});

$('#txtselectedapplicantage').keyup(function () {
    _$selectedApplicants.column(5).search($(this).val()).draw();
});
/******************************Waitlisted Applicants DataTable**********************************/
var _$waitlistedApplicants = $('#tblRegisteredStudentsWaitlisted').DataTable({
    columnDefs: [
   { orderable: false, targets: 0 }
    ],
    order: [[1, 'asc']]
});

$('#txtwaitlistedapplicantname').keyup(function () {
    _$waitlistedApplicants.column(1).search($(this).val()).draw();
});
$('#txtwaitlistedapplicantnationality').keyup(function () {
    _$waitlistedApplicants.column(3).search($(this).val()).draw();
});

$('#txtwaitlistedapplicantdistance').keyup(function () {
    _$waitlistedApplicants.column(4).search($(this).val()).draw();
});

$('#txtwaitlistedapplicantclass').keyup(function () {
    _$waitlistedApplicants.column(2).search($(this).val()).draw();
});

$('#txtwaitlistedapplicantage').keyup(function () {
    _$waitlistedApplicants.column(5).search($(this).val()).draw();
});
/******************************Rejected Applicants DataTable**********************************/
var _$rejectedApplicants = $('#tblRegisteredStudentsRejected').DataTable({
    columnDefs: [
   { orderable: false, targets: 0 }
    ],
    order: [[1, 'asc']]
});

$('#txtrejectedapplicantname').keyup(function () {
    _$rejectedApplicants.column(1).search($(this).val()).draw();
});
$('#txtrejectedapplicantnationality').keyup(function () {
    _$rejectedApplicants.column(3).search($(this).val()).draw();
});

$('#txtrejectedapplicantdistance').keyup(function () {
    _$rejectedApplicants.column(4).search($(this).val()).draw();
});

$('#txtrejectedapplicantclass').keyup(function () {
    _$rejectedApplicants.column(2).search($(this).val()).draw();
});

$('#txtrejectedapplicantage').keyup(function () {
    _$rejectedApplicants.column(5).search($(this).val()).draw();
});
/*******************************CheckAll Checkboxes in List******************************************/
$('#chkselectallnewapplicants').click(function (e) {
    if ($(this).hasClass('checkedAll')) {
        $('#tab_1_1').find(':checkbox').each(function () {
            $(this).prop('checked', false);
            $(this).removeClass('checkedAll');
        });

    } else {
        $('#tab_1_1').find(':checkbox').each(function () {
            $(this).prop('checked', true);
            $(this).addClass('checkedAll');
        });
    }
});

$('#chkselectallselectedapplicants').click(function (e) {
    if ($(this).hasClass('checkedAll')) {
        $('#tab_1_2').find(':checkbox').each(function () {
            $(this).prop('checked', false);
            $(this).removeClass('checkedAll');
        });

    } else {
        $('#tab_1_2').find(':checkbox').each(function () {
            $(this).prop('checked', true);
            $(this).addClass('checkedAll');
        });
    }
});

$('#chkselectallwaitlistedapplicants').click(function (e) {
    if ($(this).hasClass('checkedAll')) {
        $('#tab_1_3').find(':checkbox').each(function () {
            $(this).prop('checked', false);
            $(this).removeClass('checkedAll');
        });

    } else {
        $('#tab_1_3').find(':checkbox').each(function () {
            $(this).prop('checked', true);
            $(this).addClass('checkedAll');
        });
    }
});

$('#chkselectallrejectedapplicants').click(function (e) {
    if ($(this).hasClass('checkedAll')) {
        $('#tab_1_4').find(':checkbox').each(function () {
            $(this).prop('checked', false);
            $(this).removeClass('checkedAll');
        });

    } else {
        $('#tab_1_4').find(':checkbox').each(function () {
            $(this).prop('checked', true);
            $(this).addClass('checkedAll');
        });
    }
});


function GetSelectedGuids(button) {
    debugger;
    var btnId = button.id;
    var tblName = btnId.substring(4, btnId.length);
    var applicantType = $('#ddl_' + tblName).val();
    //alert(tblName);
    registrationGuids = new Array();
    $('#' + tblName + '> tbody > tr').each(function () {
        if ($(this).find("input:checked").is(":checked")) {
            debugger;
            var guid = $(this).find('#spnRegistrationID').text();
            registrationGuids.push(guid);
        }
    });
    if (registrationGuids.length > 0 && applicantType!='') {
        //Ajax call to send all the checked guids for process applicant.
        $.ajax({
            url: '/Registration/ProcessRegistrationApplicants',
            type: "POST",
            dataType: "JSON",
            contentType: "application/json",
            data: JSON.stringify({ 'applicantsIds': registrationGuids, 'applicantStatus': applicantType }),
            success: function (result) {
                window.location.href = '/Registration/ProcessRegistration/';
            }
        });
    }
}