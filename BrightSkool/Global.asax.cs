﻿using BrightSkool.BusinessLayer.Dependency;
using BrightSkool.Installer;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Microsoft.WindowsAzure.ServiceRuntime;
using Orleans;
using Orleans.Runtime.Host;
using System;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

//[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(BrightSkool.MvcApplication), "AddViewEngines")]
namespace BrightSkool
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static IWindsorContainer container;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            if (RoleEnvironment.IsAvailable)
            {
                //azure
                var config = AzureClient.DefaultConfiguration();
                AzureClient.Initialize(config);
                //AzureClient.Initialize();\
                //GrainClient.Initialize(
                //Server.MapPath(@"~/OrleansClientConfiguration.xml")); //If this is added then the Gateway is not found for the GrainClient
            }
            else
            {
                //Local deployment
                // GrainClient.Initialize(
                //Server.MapPath(
                //    @"~/OrleansClientConfiguration.xml"));

                //This code is left here for local dev purposes - Steve
                if (!GrainClient.IsInitialized)
                {
                    try
                    {
                        GrainClient.Initialize(
                           Server.MapPath(
                                @"~/OrleansClientConfiguration.xml"));
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            
            BootstrapContainer();
        }

        private static void BootstrapContainer()
        {
            container = new WindsorContainer()
                .Install(FromAssembly.This());
            var controllerFactory = new WindsorControllerFactory(container.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);
            var allTypesFromBinDir = Classes
                                    .FromAssemblyInDirectory(new AssemblyFilter(HttpRuntime.BinDirectory));

            container.Register(allTypesFromBinDir
                               .BasedOn<ITransiantDependency>()
                               .WithService.DefaultInterfaces()
                               .Configure(s => s.LifestyleTransient()));

            container.Register(allTypesFromBinDir
                               .BasedOn<ISingletonDependency>()
                               .WithService.DefaultInterfaces()
                               .Configure(s => s.LifestyleSingleton()));
        }
    }
}
