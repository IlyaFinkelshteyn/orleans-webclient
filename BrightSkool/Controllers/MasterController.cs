﻿using BrightSkool.BusinessLayer.BAL.Masters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BrightSkool.Controllers
{
    public class MasterController : Controller
    {
        ICountryService _countryService;
        ICurrencyService _currencyService;
        ILanguageService _languageService;
        IReligionService _religionService;
        IRaceService _raceService;
        INationalityService _nationalityService;
        IRelationshipService _relationshipService;

        public MasterController(ICountryService countryService, ICurrencyService currencyService, ILanguageService languageService, IReligionService religionService, IRaceService raceService, INationalityService nationalityService, IRelationshipService relationshipService)
        {
            _countryService = countryService;
            _currencyService = currencyService;
            _languageService = languageService;
            _religionService = religionService;
            _raceService = raceService;
            _nationalityService = nationalityService;
            _relationshipService = relationshipService;
        }
        public async Task<ActionResult> Countries()
        {
            var countryResult = await _countryService.GetCountries();

            return View(countryResult);
        }

        public async Task<ActionResult> Currencies()
        {
            var currencyResult = await _currencyService.GetCurrencies();

            return View(currencyResult);
        }

        public async Task<ActionResult> Languages()
        {
            var languageResult = await _languageService.GetLanguages();

            return View(languageResult);
        }

        public async Task<ActionResult> Religions()
        {
            var religionResult = await _religionService.GetReligions();

            return View(religionResult);
        }

        public async Task<ActionResult> Races()
        {
            var raceResult = await _raceService.GetRace();

            return View(raceResult);
        }

        public async Task<ActionResult> Nationalities()
        {
            var nationalityResult = await _nationalityService.GetNationalities();

            return View(nationalityResult);
        }

        public async Task<ActionResult> Relationships()
        {
            var relationShipResult = await _relationshipService.GetRelationships();

            return View(relationShipResult);
        }
    }
}