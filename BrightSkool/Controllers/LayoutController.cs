﻿using BrightSkool.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BrightSkool.Controllers
{
    public class LayoutController : Controller
    {
        [ChildActionOnly]
        public PartialViewResult Header()
        {
            return PartialView("_Header");
        }

        [ChildActionOnly]
        public PartialViewResult Footer()
        {
            return PartialView("_Footer");
        }

        [ChildActionOnly]
        public PartialViewResult Sidebar(string currentPage)
        {
            var sidebarModel = new SidebarModel
            {
                CurrentPage = currentPage
            };

            return PartialView("_Sidebar", sidebarModel);
        }
    }
}