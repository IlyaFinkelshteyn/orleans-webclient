﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BrightSkool.Startup))]
namespace BrightSkool
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
