﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrightSkool
{
    public static class PageNames
    {
        public const string Dashboard = "Dashboard";
        public const string Registration = "Registration";
        public const string Admission = "Admission";
        public const string Admission_RegisteredStudents = "Admission.RegisteredStudents";
        public const string Admission_Enquiries = "Admission.Enquiries";

        public const string SchoolSettings = "SchoolSettings";
        public const string SchoolSettings_Profile = "SchoolSettings.Profile";


        public const string Settings = "AppSettings";
        public const string Settings_Country = "AppSettings.Country";
        public const string Settings_Currency = "AppSettings.Currency";
        public const string Settings_Language = "AppSettings.Language";
        public const string Settings_Religion = "AppSettings.Religion";
        public const string Settings_Race = "AppSettings.Race";
        public const string Settings_Nationality = "AppSettings.Nationality";
        public const string Settings_Relationship = "AppSettings.Relationship";

        public const string Administration = "Administration";
        public const string Administration_Roles = "Administration.Roles";
        public const string Administration_Users = "Administration.Users";
    }
}