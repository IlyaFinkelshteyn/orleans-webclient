﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrightSkool
{
    public static class ScriptPaths
    {
        public const string Json2 = "~/libs/json2/json2.min.js";

        public const string JQuery = "~/libs/jquery/jquery.min.js";
        public const string JQuery_Migrate = "~/libs/jquery/jquery-migrate.min.js";
        public const string JQuery_UI = "~/libs/jquery-ui/jquery-ui.min.js";

        public const string JQuery_Slimscroll = "~/libs/jquery-slimscroll/jquery.slimscroll.min.js";
        public const string JQuery_BlockUi = "~/libs/jquery-blockui/jquery.blockui.min.js";
        public const string JQuery_Cookie = "~/libs/jquery-cookie/jquery.cookie.min.js";
        public const string JQuery_Uniform = "~/libs/jquery-uniform/jquery.uniform.min.js";
        public const string JQuery_Ajax_Form = "~/libs/jquery-ajax-form/jquery.form.js";
        public const string JQuery_Validation = "~/libs/jquery-validation/js/jquery.validate.min.js";
        public const string JQuery_jTable = "~/libs/jquery-jtable/jquery.jtable.js";

        public const string JsTree = "~/libs/jstree/jstree.js";
        public const string SpinJs = "~/libs/spinjs/spin.js";
        public const string SpinJs_JQuery = "~/libs/spinjs/jquery.spin.js";

        public const string SweetAlert = "~/libs/sweetalert/sweet-alert.min.js";
        public const string Toastr = "~/libs/toastr/toastr.min.js";

        public const string MomentJs = "~/libs/moment/moment-with-locales.min.js";
        public const string MomentTimezoneJs = "~/Scripts/moment-timezone-with-data.min.js";

        public const string Bootstrap = "~/libs/bootstrap/js/bootstrap.min.js";
        public const string Bootstrap_Hover_Dropdown = "~/libs/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js";
        public const string Bootstrap_DateRangePicker = "~/libs/bootstrap-daterangepicker/daterangepicker.js";
        public const string Bootstrap_DatePicker = "~/libs/bootstrap-datepicker/bootstrap-datepicker.min.js";

        public const string Bootstrap_wysihtml5030 = "~/libs/bootstrap-wysihtml5/wysihtml5-0.3.0.js";
        public const string Bootstrap_wysihtml5 = "~/libs/bootstrap-wysihtml5/bootstrap-wysihtml5.js";

        public const string Bootstrap_timepicker = "~/libs/bootstrap-timepicker/js/bootstrap-timepicker.min.js";

        public const string Bootstrap_wizard = "~/libs/bootstrap-wizard/jquery.bootstrap.wizard.min.js";

        public const string Bootstrap_multiselect = "~/libs/bootstrap-multiselect/js/bootstrap-multiselect.js";

        public const string Form_wizard = "~/libs/bootstrap-wizard/jquery.form-wizard.min.js";

        public const string Jquery_FileUpload_UI_Widget = "~/libs/libs/jquery-fileupload/js/vendor/jquery.ui.widget.js";
        public const string Jquery_FileUpload_IframeTransport = "~/libs/libs/jquery-fileupload/js/jquery.iframe-transport.js";
        public const string Jquery_FileUpload = "~/libs/libs/jquery-fileupload/js/jquery.fileupload.js";
        public const string Jquery_FileUpload_Process = "~/libs/libs/jquery-fileupload/js/jquery.fileupload-process.js";
        public const string Jquery_FileUpload_Image = "~/libs/libs/jquery-fileupload/js/jquery.fileupload-image.js";
        public const string Jquery_FileUpload_audio = "~/libs/libs/jquery-fileupload/js/jquery.fileupload-audio.js";
        public const string Jquery_FileUpload_video = "~/libs/libs/jquery-fileupload/js/jquery.fileupload-video.js";
        public const string Jquery_FileUpload_validate = "~/libs/libs/jquery-fileupload/js/jquery.fileupload-validate.js";
        public const string Jquery_FileUpload_UI = "~/libs/libs/jquery-fileupload/js/jquery.fileupload-ui.js";

        public const string JQuery_datatables_min_js = "~/libs/datatables/datatables.min.js";
        public const string JQuery_datatablesjs = "~/libs/datatables/datatables.js";
        public const string JQuery_datatables_bootstrap_js = "~/libs/datatables/datatables.bootstrap.js";

        public const string Angular = "~/libs/angularjs/angular.js";
        public const string Angular_Route = "~/libs/angularjs/angular-route.js";

        public const string regex_Mask = "~/libs/regexMask/regex-mask-plugin.js";


        #region App Scripts

        public const string App_Masters = "~/Scripts/Master/master.js";

        #endregion


    }
}