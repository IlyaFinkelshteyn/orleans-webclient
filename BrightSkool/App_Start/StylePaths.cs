﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrightSkool
{
    public static class StylePaths
    {
        public const string JQuery_UI = "~/libs/jquery-ui/jquery-ui.min.css";
        public const string Simple_Line_Icons = "~/libs/simple-line-icons/simple-line-icons.min.css";
        public const string Bootstrap = "~/libs/bootstrap/css/bootstrap.min.css";
        public const string BootstrapRTL = "~/libs/bootstrap/css/bootstrap-rtl.min.css";
        public const string SweetAlert = "~/libs/sweetalert/sweet-alert.css";
        public const string Toastr = "~/libs/toastr/toastr.min.css";
        public const string FontAwesome = "~/Content/font-awesome.min.css";        
        public const string JQuery_Uniform = "~/libs/jquery-uniform/css/uniform.default.css";
        public const string Morris = "~/libs/morris/morris.css";
        public const string JsTree = "~/libs/jstree/themes/default/style.css";
        public const string Angular_Ui_Grid = "~/libs/angular-ui-grid/ui-grid.min.css";
        public const string Bootstrap_DateRangePicker = "~/libs/bootstrap-daterangepicker/daterangepicker.css";
        public const string Bootstrap_DatePicker = "~/libs/bootstrap-datepicker/bootstrap-datepicker3.min.css";
        public const string Bootstrap_Select = "~/libs/bootstrap-select/bootstrap-select.min.css";
        public const string Bootstrap_Switch = "~/libs/bootstrap-switch/css/bootstrap-switch.min.css";
        public const string Bootstrap_timePicker = "~/libs/bootstrap-timepicker/css/bootstrap-timepicker.min.css";
        public const string Bootstrap_wysihtml5 = "~/libs/bootstrap-wysihtml5/bootstrap-wysihtml5.css";
        public const string Bootstrap_multiselect = "~/libs/bootstrap-multiselect/js/bootstrap-multiselect.css";
        public const string JQuery_jTable_Theme = "~/libs/jquery-jtable/themes/metro/blue/jtable.min.css";
        public const string JQuery_Jcrop = "~/libs/jcrop/css/jquery.Jcrop.min.css";
        public const string JQuery_datatablescss = "~/libs/datatables/datatables.min.css";
        public const string JQuery_datatables_bootstrap_css = "~/libs/datatables/datatables.bootstrap.css";
    }
}