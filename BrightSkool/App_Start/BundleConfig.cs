﻿using System.Web;
using System.Web.Optimization;

namespace BrightSkool
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(
                 new ScriptBundle("~/Bundles/Frontend/libs/js")
                     .Include(
                         ScriptPaths.Json2,
                         ScriptPaths.JQuery,
                         ScriptPaths.JQuery_UI,
                         ScriptPaths.JQuery_Validation,
                         ScriptPaths.Bootstrap_wysihtml5030,
                         ScriptPaths.Bootstrap,
                         ScriptPaths.Bootstrap_Hover_Dropdown,
                         ScriptPaths.MomentJs,
                         ScriptPaths.MomentTimezoneJs,
                         ScriptPaths.Bootstrap_DateRangePicker,
                         ScriptPaths.Bootstrap_DatePicker,
                         ScriptPaths.Bootstrap_timepicker,
                         ScriptPaths.Bootstrap_wysihtml5,
                         ScriptPaths.JQuery_Slimscroll,
                         ScriptPaths.JQuery_BlockUi,
                         ScriptPaths.JQuery_Cookie,
                         ScriptPaths.JQuery_Uniform,
                         ScriptPaths.JQuery_Ajax_Form,
                         //ScriptPaths.JQuery_jTable,
                         ScriptPaths.SpinJs,
                         ScriptPaths.SpinJs_JQuery,
                         ScriptPaths.SweetAlert,
                         ScriptPaths.Toastr,
                         ScriptPaths.JsTree
                 ).ForceOrder()
              );

            AddFrontendCssLibs(bundles);
            AddFrontendCssMetronic(bundles);
            AddAppMetrinicCss(bundles);
            AddappCssLibs(bundles);
            bundles.Add(
                new ScriptBundle("~/Bundles/Frontend/metronic/js")
                    .Include(
                        "~/App_Themes/metronic/assets/frontend/layout/scripts/back-to-top.js",
                        "~/App_Themes/metronic/assets/frontend/layout/scripts/layout.js"
                    ).ForceOrder()
                );

            bundles.Add(
             new ScriptBundle("~/Bundles/app/metronic/js")
                 .Include(
                     "~/App_Themes/metronic/assets/global/scripts/app.js",
                     "~/App_Themes/metronic/assets/admin/layout4/scripts/layout.js"
                 ).ForceOrder()
             );

            //bundles.Add(
            //    new ScriptBundle("~/Bundles/Common/js")
            //        .IncludeDirectory("~/Scripts", "*.js", true)
            //    );

            bundles.Add(
                new ScriptBundle("~/Bundles/Roles")
                    .Include("~/Views/Roles/Index.js")
                );

            bundles.Add(
                new ScriptBundle("~/Bundles/AdmissionEnquiry")
                    .Include("~/Views/Admission/Enquiry.js")
                );

            bundles.Add(
                new ScriptBundle("~/Bundles/AdmissionEnquiryList")
                    .Include("~/Views/Admission/Index.js")
                );

            bundles.Add(
                new ScriptBundle("~/Bundles/EnquiryView")
                    .Include("~/Views/Admission/EnquiryView.js")
                );

            bundles.Add(
                new ScriptBundle("~/Bundles/ReplyEmailToEnquiry")
                    .Include("~/Views/Admission/ReplyEmailToEnquiry.js")
                );

            bundles.Add(
                new ScriptBundle("~/Bundles/SchoolProfile")
                    .Include("~/libs/bootstrap-fileinput/bootstrap-fileinput.js")
                    .Include("~/Scripts/SchoolProfile/CreateOrEditModel.js")
                );

            bundles.Add(
                new ScriptBundle("~/Bundles/Wizard")
                    .Include("~/libs/bootstrap-wizard/jquery.bootstrap.wizard.min.js")
                    .Include("~/libs/bootstrap-wizard/form-wizard.min.js")
                );

            bundles.Add(
                new ScriptBundle("~/Bundles/UserRegistration")
                    .Include("~/Scripts/Registration/userregistration.js")
                );
            bundles.Add(
            new ScriptBundle("~/Bundles/ngregistration")
                    .Include("~/Scripts/Registration/ng-userregistration.js")
                );


            bundles.Add(
                new ScriptBundle("~/Bundles/Angular")
                    .Include(ScriptPaths.Angular)
                    .Include(ScriptPaths.Angular_Route)
                );
            bundles.Add(
                new ScriptBundle("~/Bundles/ProcessRegistration")
                    .Include("~/Scripts/Registration/registeredStudents.js")
                );
            bundles.Add(
                new ScriptBundle("~/Bundles/DatatableJS")
                    .Include(ScriptPaths.JQuery_datatables_min_js)
                    .Include(ScriptPaths.JQuery_datatablesjs)
                );
            bundles.Add(
                new StyleBundle("~/Bundles/DatatableCSS")
                    .Include(StylePaths.JQuery_datatablescss, new CssRewriteUrlTransform())
            );

            bundles.Add(
                new ScriptBundle("~/Bundles/JqueryFileUpload")
                    .Include(ScriptPaths.Jquery_FileUpload_UI_Widget)
                    .Include(ScriptPaths.Jquery_FileUpload_IframeTransport)
                    .Include(ScriptPaths.Jquery_FileUpload)
                    .Include(ScriptPaths.Jquery_FileUpload_Process)
                    .Include(ScriptPaths.Jquery_FileUpload_Image)
                    .Include(ScriptPaths.Jquery_FileUpload_audio)
                    .Include(ScriptPaths.Jquery_FileUpload_video)
                    .Include(ScriptPaths.Jquery_FileUpload_validate)
                    .Include(ScriptPaths.Jquery_FileUpload_UI)
                );
            bundles.Add(
               new ScriptBundle("~/Bundles/bootstrap-multiselect")
                   .Include(ScriptPaths.Bootstrap_multiselect)
               );
            bundles.Add(
                new StyleBundle("~/Bundles/Style/bootstrap-multiselect")
                   .Include(StylePaths.Bootstrap_multiselect)
                );

        }

        private static void AddFrontendCssMetronic(BundleCollection bundles)
        {
            bundles.Add(
                new StyleBundle("~/Bundles/Frontend/metronic/css")
                    .Include("~/App_Themes/metronic/assets/global/css/components.css", new CssRewriteUrlTransform())
                    .Include("~/App_Themes/metronic/assets/frontend/layout/css/style.css", new CssRewriteUrlTransform())
                    .Include("~/App_Themes/metronic/assets/frontend/pages/css/style-revolution-slider.css", new CssRewriteUrlTransform())
                    .Include("~/App_Themes/metronic/assets/frontend/layout/css/style-responsive.css", new CssRewriteUrlTransform())
                    .Include("~/App_Themes/metronic/assets/frontend/layout/css/themes/red.css", new CssRewriteUrlTransform())
                );
        }

        private static void AddFrontendCssLibs(BundleCollection bundles)
        {
            bundles.Add(
                new StyleBundle("~/Bundles/Frontend/libs/css")
                    .Include(StylePaths.Simple_Line_Icons, new CssRewriteUrlTransform())
                    .Include(StylePaths.FontAwesome, new CssRewriteUrlTransform())
                    .Include(StylePaths.Bootstrap, new CssRewriteUrlTransform())
                    .Include(StylePaths.SweetAlert, new CssRewriteUrlTransform())
                    .Include(StylePaths.Toastr, new CssRewriteUrlTransform())
                );
        }

        private static void AddappCssLibs(BundleCollection bundles)
        {
            bundles.Add(
                new StyleBundle("~/Bundles/app/libs/css")
                    .Include(StylePaths.JQuery_UI, new CssRewriteUrlTransform())
                    //.Include(StylePaths.JQuery_jTable_Theme, new CssRewriteUrlTransform())
                    .Include(StylePaths.FontAwesome, new CssRewriteUrlTransform())
                    .Include(StylePaths.Simple_Line_Icons, new CssRewriteUrlTransform())
                    .Include(StylePaths.Bootstrap, new CssRewriteUrlTransform())
                    .Include(StylePaths.JQuery_Uniform, new CssRewriteUrlTransform())
                    .Include(StylePaths.JsTree, new CssRewriteUrlTransform())
                    .Include(StylePaths.Morris, new CssRewriteUrlTransform())
                    .Include(StylePaths.SweetAlert, new CssRewriteUrlTransform())
                    .Include(StylePaths.Toastr, new CssRewriteUrlTransform())
                    .Include(StylePaths.Bootstrap_DateRangePicker, new CssRewriteUrlTransform())
                    .Include(StylePaths.Bootstrap_DatePicker, new CssRewriteUrlTransform())
                    .Include(StylePaths.Bootstrap_Switch, new CssRewriteUrlTransform())
                    .Include(StylePaths.Bootstrap_Select, new CssRewriteUrlTransform())
                    .Include(StylePaths.Bootstrap_timePicker, new CssRewriteUrlTransform())
                    .Include(StylePaths.Bootstrap_wysihtml5, new CssRewriteUrlTransform())
                    .Include(StylePaths.JQuery_Jcrop, new CssRewriteUrlTransform())
                );
        }



        private static void AddAppMetrinicCss(BundleCollection bundles)
        {
            bundles.Add(
                new StyleBundle("~/Bundles/app/metronic/css")
                    .Include("~/App_Themes/metronic/assets/global/css/components-md.css", new CssRewriteUrlTransform())
                    .Include("~/App_Themes/metronic/assets/global/css/plugins-md.css", new CssRewriteUrlTransform())
                    .Include("~/App_Themes/metronic/assets/admin/layout4/css/layout.css", new CssRewriteUrlTransform())
                    .Include("~/App_Themes/metronic/assets/admin/layout4/css/themes/light.css", new CssRewriteUrlTransform())
                );
        }
    }
}
