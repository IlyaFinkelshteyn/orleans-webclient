﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace BrightSkool.Installer
{
    public class ControllersInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var allTypesFromBinDir = Classes
           .FromAssemblyInDirectory(new AssemblyFilter(HttpRuntime.BinDirectory));

            container.Register(allTypesFromBinDir
                .BasedOn<IController>()
                .LifestyleTransient());
        }
    }
}