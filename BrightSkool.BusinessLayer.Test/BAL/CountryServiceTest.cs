﻿using BrightSkool.BusinessLayer.BAL.Masters;
using BrightSkool.BusinessLayer.OrleansClient.Masters;
using BrightSkool.Domain.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BrightSkool.BusinessLayer.Test.BAL
{
    public class CountryServiceTest
    {
        [Fact]
        public void ShouldGetCountriesList_BL()
        {
            //Arrange
            var countryCLient = new Mock<ICountryClient>();
            //Mocking CountriesList method
            countryCLient.Setup(s => s.GetCountries()).Returns(Task.Run(() => new List<CommonModel.Country> {
                new CommonModel.Country(){ CountryCode = "IND", CountryName="India" },
                 new CommonModel.Country(){ CountryCode = "PAK", CountryName="Pakistan" },
                 new CommonModel.Country(){ CountryCode = "BAN", CountryName="Bangladesh" },
            }));
            CountryService countryService = new CountryService(countryCLient.Object);

            //Act
            var result = countryService.GetCountries().Result;
            
            //Assert
            Assert.True(result.Count > 0);
        }

        [Fact]
        public void ShouldGetEmptyCountriesList_BL()
        {
            //Arrange
            var countryCLient = new Mock<ICountryClient>();
            //Mocking CountriesList method
            countryCLient.Setup(s => s.GetCountries()).Returns(Task.Run(() => new List<CommonModel.Country>()));
            CountryService countryService = new CountryService(countryCLient.Object);

            //Act
            var result = countryService.GetCountries().Result;

            //Assert
            Assert.True(result.Count == 0);
        }
    }
}
