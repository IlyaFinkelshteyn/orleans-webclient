﻿using BrightSkool.BusinessLayer.BAL.Masters;
using BrightSkool.BusinessLayer.OrleansClient.Masters;
using BrightSkool.Domain.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BrightSkool.BusinessLayer.Test.BAL
{
    public class CurrencyServiceTest
    {
        [Fact]
        public void ShouldGetCurrencyList_BL()
        {
            //Arrange
            var currencyCLient = new Mock<ICurrencyClient>();
            //Mocking CurrencyList method
            currencyCLient.Setup(s => s.GetCurrencies()).Returns(Task.Run(() => new List<CommonModel.Currency> {
                new CommonModel.Currency(){ CountryName = "India",CurrencyName="Indian Rupees",CurrencyCode="INR"},
                new CommonModel.Currency(){ CountryName = "United State Of America ",CurrencyName="United State Dollar",CurrencyCode="USD"},
            }));
            CurrencyService currencyService = new CurrencyService(currencyCLient.Object);

            //Act
            var result = currencyService.GetCurrencies().Result;

            //Assert
            Assert.True(result.Count > 0);
        }

        [Fact]
        public void ShouldGetEmptyCurrencyList_BL()
        {
            //Arrange
            var currencyCLient = new Mock<ICurrencyClient>();
            //Mocking CurrencyList method
            currencyCLient.Setup(s => s.GetCurrencies()).Returns(Task.Run(() => new List<CommonModel.Currency>()));
            CurrencyService currencyService = new CurrencyService(currencyCLient.Object);

            //Act
            var result = currencyService.GetCurrencies().Result;

            //Assert
            Assert.True(result.Count == 0);
        }

    }
}
