﻿using BrightSkool.BusinessLayer.BAL.Registration;
using BrightSkool.BusinessLayer.OrleansClient.Registration;
using BrightSkool.Domain.Dto;
using BrightSkool.Domain.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BrightSkool.BusinessLayer.Test.BAL
{
    public class RegistrationServiceTest
    {
        [Fact]
        public void ShouldGetRegistrationbyID_BL ()
        {
            //Arrange
            CommonModel.Person person = new CommonModel.Person();
            person.FirstName = "Ramesh";
            person.LastName = "Kumar";
            person.Gender = "Male";
            var registrationCLient = new Mock<IRegistrationClient>();
            //Mocking GetRegistrationbyID method
            registrationCLient.Setup(s => s.GetRegistrationbyID(It.IsAny<Guid>())).Returns(Task.Run(() => new RegistrationModel
            {
                Id = Guid.NewGuid(),
                Email = "test@gmail.com",
                Applicant = person
            }));
            RegistrationService registrationService = new RegistrationService(registrationCLient.Object);

            //Act
            var result = registrationService.GetRegistrationbyID(Guid.NewGuid()).Result;

            //Assert
            Assert.True(result.Id != Guid.Empty);
            Assert.True(result.Applicant.FirstName.ToUpper().Equals("RAMESH"));
            Assert.True(result.Applicant.Gender.ToUpper().Equals("MALE"));
        }

        [Fact]
        public void ShouldGetEmptyRegistrationbyIDWhenIDNotFound_BL ()
        {
            //Arrange
            var registrationCLient = new Mock<IRegistrationClient>();
            //Mocking GetRegistrationbyID method
            registrationCLient.Setup(s => s.GetRegistrationbyID(It.IsAny<Guid>())).Returns(Task.Run(() => new RegistrationModel()));
            RegistrationService registrationService = new RegistrationService(registrationCLient.Object);

            //Act
            var result = registrationService.GetRegistrationbyID(Guid.NewGuid()).Result;

            //Assert
            Assert.True(result.Id == Guid.Empty);
        }

        [Fact]
        public void ShouldGetRegistrationList_BL ()
        {
            //Arrange
            var registrationCLient = new Mock<IRegistrationClient>();
            //Mocking RegistrationsList method
            registrationCLient.Setup(s => s.RegistrationsList()).Returns(Task.Run(() => new List<RegistrationModel> {
                new RegistrationModel { Id=Guid.NewGuid(),Email="Test@gmail.com"},
                new RegistrationModel { Id=Guid.NewGuid(),Email="Test1@rediff.com"},
                new RegistrationModel { Id=Guid.NewGuid(),Email="Test2@yahoo.com"},
            }));
            RegistrationService registrationService = new RegistrationService(registrationCLient.Object);

            //Act
            var result = registrationService.RegistrationsList().Result;

            //Assert
            Assert.True(result.Count > 0);
        }

        [Fact]
        public void ShouldGetEmptyRegistrationList_BL ()
        {
            //Arrange
            var registrationCLient = new Mock<IRegistrationClient>();
            //Mocking RegistrationsList method
            registrationCLient.Setup(s => s.RegistrationsList()).Returns(Task.Run(() => new List<RegistrationModel>()));
            RegistrationService registrationService = new RegistrationService(registrationCLient.Object);

            //Act
            var result = registrationService.RegistrationsList().Result;

            //Assert
            Assert.True(result.Count == 0);
        }

        [Fact]
        public void ShouldSaveRegistration_BL ()
        {
            //Arrange
            CommonModel.Person person = new CommonModel.Person();
            person.FirstName = "Ramesh";
            person.LastName = "Kumar";
            person.Gender = "Male";
            RegistrationModel registrationModel = new RegistrationModel();
            registrationModel.Applicant = person;
            registrationModel.Email = "test@rediff.com";

            var registrationCLient = new Mock<IRegistrationClient>();
            //Mocking SaveRegistration method
            registrationCLient.Setup(s => s.SaveRegistration(registrationModel)).Returns(Task.Run(() => 1 == 1 ? true : false));
            RegistrationService registrationService = new RegistrationService(registrationCLient.Object);

            //Act
            var result = registrationService.SaveRegistration(registrationModel).Result;

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void ShouldFailSaveRegistration_BL ()
        {
            //Arrange
            CommonModel.Person person = new CommonModel.Person();
            person.FirstName = "Ramesh";
            person.LastName = "Kumar";
            person.Gender = "Male";
            RegistrationModel registrationModel = new RegistrationModel();
            registrationModel.Applicant = person;
            registrationModel.Email = "test@rediff.com";

            var registrationCLient = new Mock<IRegistrationClient>();
            //Mocking SaveRegistration method
            registrationCLient.Setup(s => s.SaveRegistration(registrationModel)).Returns(Task.Run(() => 1 == 2 ? true : false));
            RegistrationService registrationService = new RegistrationService(registrationCLient.Object);

            //Act
            var result = registrationService.SaveRegistration(registrationModel).Result;

            //Assert
            Assert.True(!result);
        }

        [Fact]
        public void ShouldProcessRegistrationApplicants_BL ()
        {
            //Arrange
            var applicantGuids = AccountGrainIds;

            var registrationCLient = new Mock<IRegistrationClient>();
            //Mocking SaveRegistration method
            //registrationCLient.Setup(s => s.ProcessRegistratedApplicants(registrationCLient,"Selected")).Returns(Task.Run(() => 1 == 1 ? true : false));
            RegistrationService registrationService = new RegistrationService(registrationCLient.Object);

            //Act
            var result = registrationService.ProcessRegistrationApplicants(applicantGuids, "Selected").Result;

            //Assert
            Assert.True(result);
        }

        public List<RegistrationModel> GetRegistrations ()
        {
            List<RegistrationModel> lstresitrationModels = new List<RegistrationModel>();
            RegistrationModel registrationModel = new RegistrationModel();
            CommonModel.Person person = new CommonModel.Person();
            person.FirstName = "Ramesh";
            person.LastName = "Kumar";
            person.Gender = "Male";
            registrationModel.Applicant = person;

            RegistrationModel registrationModel1 = new RegistrationModel();
            CommonModel.Person person1 = new CommonModel.Person();
            person1.FirstName = "Amit";
            person1.LastName = "Kumar";
            person1.Gender = "Male";
            registrationModel1.Applicant = person;
            lstresitrationModels.Add(registrationModel);
            lstresitrationModels.Add(registrationModel1);

            return lstresitrationModels;
        }

        [Fact]
        public void ShouldGetProcessRegistrations_BL ()
        {
            var registrationCLient = new Mock<IRegistrationClient>();
            List<RegistrationModel> lstRegistrationModel = new List<RegistrationModel>();
            List<RegistrationDto> processRegistrations = new List<RegistrationDto>();
            for (int i = 0; i < 20; i++)
            {
                if (i < 5)
                    lstRegistrationModel.Add(new RegistrationModel { Applicant = new CommonModel.Person { FirstName = "Madhu", LastName = "Babu", MiddleName = "Chowlur" }, Level = "10th", Nationality = new CommonModel.Nationality { CountryNationality = "Indian" }, Status = new ApplicationStatus { Code = "New", Status = "New" } });
                if (i < 12)
                    lstRegistrationModel.Add(new RegistrationModel { Applicant = new CommonModel.Person { FirstName = "Kishor", LastName = "Kumar", MiddleName = "Chowlur" }, Level = "5th", Nationality = new CommonModel.Nationality { CountryNationality = "Indian" } });
                if (i < 8)
                    lstRegistrationModel.Add(new RegistrationModel { Applicant = new CommonModel.Person { FirstName = "Lakshmi", LastName = "Sathyanarayana", MiddleName = "" }, Level = "8th", Nationality = new CommonModel.Nationality { CountryNationality = "Indian" } });
                lstRegistrationModel.Add(new RegistrationModel { Applicant = new CommonModel.Person { FirstName = "Sanju", LastName = "Sathyanarayana", MiddleName = "" }, Level = "5th", Nationality = new CommonModel.Nationality { CountryNationality = "Indian" } });
            }

            registrationCLient.Setup(s => s.RegistrationsList()).Returns(Task.Run(() => lstRegistrationModel));
            RegistrationService registrationService = new RegistrationService(registrationCLient.Object);
            //Act
            var result = registrationService.ProcessRegistrations().Result;
            //Assert
            Assert.True(result.Count > 0);
        }

        public static readonly List<Guid> AccountGrainIds = new List<Guid>
        {
            new Guid("{6b66f0ea-b329-4483-b78b-108f4df62eee}"),
            new Guid("{9c3d599b-0cac-4882-9244-12df92dc4da5}"),
            new Guid("{22447804-74e6-4a7f-aa8c-e8cd0dc851aa}"),
            new Guid("{c0f72fa5-da95-4166-8a60-1fac49820f3a}"),
            new Guid("{1a5f0a25-7965-4e22-92cf-127f0c462048}"),
        };
    }
}
