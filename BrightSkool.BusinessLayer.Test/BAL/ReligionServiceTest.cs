﻿using BrightSkool.BusinessLayer.BAL.Masters;
using BrightSkool.BusinessLayer.OrleansClient.Masters;
using BrightSkool.Domain.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BrightSkool.BusinessLayer.Test.BAL
{
    public class ReligionServiceTest
    {
        [Fact]
        public void ShouldGetReligionList_BL()
        {
            //Arrange
            var religionCLient = new Mock<IReligionClient>();
            //Mocking ReligionList method
            religionCLient.Setup(s => s.GetReligions()).Returns(Task.Run(() => new List<CommonModel.Religion> {
                new CommonModel.Religion(){ ReligionName = "Hinduism"},
                new CommonModel.Religion(){ ReligionName = "Islam"},
            }));
            ReligionService religionService = new ReligionService(religionCLient.Object);

            //Act
            var result = religionService.GetReligions().Result;

            //Assert
            Assert.True(result.Count > 0);
        }

        [Fact]
        public void ShouldGetEmptyReligionList_BL()
        {
            //Arrange
            var religionCLient = new Mock<IReligionClient>();
            //Mocking ReligionList method
            religionCLient.Setup(s => s.GetReligions()).Returns(Task.Run(() => new List<CommonModel.Religion>()));
            ReligionService religionService = new ReligionService(religionCLient.Object);

            //Act
            var result = religionService.GetReligions().Result;

            //Assert
            Assert.True(result.Count == 0);
        }

    }
}
