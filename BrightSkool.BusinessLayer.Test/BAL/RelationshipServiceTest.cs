﻿using BrightSkool.BusinessLayer.BAL.Masters;
using BrightSkool.BusinessLayer.OrleansClient.Masters;
using BrightSkool.Domain.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BrightSkool.BusinessLayer.Test.BAL
{
    public class RelationshipServiceTest
    {
        [Fact]
        public void ShouldGetRelationshipList_BL()
        {
            //Arrange
            var relationshipCLient = new Mock<IRelationshipClient>();
            //Mocking RelationshipList method
            relationshipCLient.Setup(s => s.GetRelationships()).Returns(Task.Run(() => new List<CommonModel.Relationship> {
                new CommonModel.Relationship(){ Relation = "Father"},
                new CommonModel.Relationship(){ Relation = "Mother"},
            }));
            RelationshipService relationshipService = new RelationshipService(relationshipCLient.Object);

            //Act
            var result = relationshipService.GetRelationships().Result;

            //Assert
            Assert.True(result.Count > 0);
        }

        [Fact]
        public void ShouldGetEmptyRelationshipList_BL()
        {
            //Arrange
            var relationshipCLient = new Mock<IRelationshipClient>();
            //Mocking RelationshipList method
            relationshipCLient.Setup(s => s.GetRelationships()).Returns(Task.Run(() => new List<CommonModel.Relationship>()));
            RelationshipService relationshipService = new RelationshipService(relationshipCLient.Object);

            //Act
            var result = relationshipService.GetRelationships().Result;

            //Assert
            Assert.True(result.Count == 0);
        }
    }
}
