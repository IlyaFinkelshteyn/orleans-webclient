﻿using BrightSkool.BusinessLayer.BAL.Masters;
using BrightSkool.BusinessLayer.OrleansClient.Masters;
using BrightSkool.Domain.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BrightSkool.BusinessLayer.Test.BAL
{
    public class LanguageServiceTest
    {
        [Fact]
        public void ShouldGetLanguageList_BL()
        {
            //Arrange
            var languageClient = new Mock<ILanguageClient>();
            //Mocking LanguageList method
            languageClient.Setup(s => s.GetLanguages()).Returns(Task.Run(() => new List<CommonModel.Language> {
                new CommonModel.Language(){ LangaugeCode = "ENG",LanguageName="English"},
                new CommonModel.Language(){ LangaugeCode = "HIN",LanguageName="Hindi"},
            }));
            LanguageService languageService = new LanguageService(languageClient.Object);

            //Act
            var result = languageService.GetLanguages().Result;

            //Assert
            Assert.True(result.Count > 0);
        }

        [Fact]
        public void ShouldGetEmptyRelationshipList_BL()
        {
            //Arrange
            var languageClient = new Mock<ILanguageClient>();
            //Mocking LanguageList method
            languageClient.Setup(s => s.GetLanguages()).Returns(Task.Run(() => new List<CommonModel.Language>()));
            LanguageService languageService = new LanguageService(languageClient.Object);

            //Act
            var result = languageService.GetLanguages().Result;

            //Assert
            Assert.True(result.Count == 0);
        }

    }
}
