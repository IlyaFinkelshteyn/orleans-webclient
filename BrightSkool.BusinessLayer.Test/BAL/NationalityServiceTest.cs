﻿using BrightSkool.BusinessLayer.BAL.Masters;
using BrightSkool.BusinessLayer.OrleansClient.Masters;
using BrightSkool.Domain.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BrightSkool.BusinessLayer.Test.BAL
{
    public class NationalityServiceTest
    {
        [Fact]
        public void ShouldGetNationalityList_BL()
        {
            //Arrange
            var nationalityCLient = new Mock<INationalityClient>();
            //Mocking NationalityList method
            nationalityCLient.Setup(s => s.GetNationalities()).Returns(Task.Run(() => new List<CommonModel.Nationality> {
                new CommonModel.Nationality(){ CountryNationality = "India",ISONationality="IND"},
                 new CommonModel.Nationality(){ CountryNationality = "Bangladesh",ISONationality="BAN"},
            }));
            NationalityService nationalityService = new NationalityService(nationalityCLient.Object);

            //Act
            var result = nationalityService.GetNationalities().Result;

            //Assert
            Assert.True(result.Count > 0);
        }

        [Fact]
        public void ShouldGetEmptyNationalityList_BL()
        {
            //Arrange
            var nationalityCLient = new Mock<INationalityClient>();
            //Mocking NationalityList method
            nationalityCLient.Setup(s => s.GetNationalities()).Returns(Task.Run(() => new List<CommonModel.Nationality>()));
            NationalityService nationalityService = new NationalityService(nationalityCLient.Object);

            //Act
            var result = nationalityService.GetNationalities().Result;

            //Assert
            Assert.True(result.Count == 0);
        }
    }
}
