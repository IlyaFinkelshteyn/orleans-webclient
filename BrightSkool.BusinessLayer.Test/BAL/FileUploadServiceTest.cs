﻿using BrightSkool.BusinessLayer.BAL.FileUpload;
using BrightSkool.Domain.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Xunit;

namespace BrightSkool.BusinessLayer.Test.BAL
{
    public class FileUploadServiceTest
    {
        [Fact]
        public void ShouldSavePicture()
        {
            //Arrange
            var postedfile = new Mock<HttpPostedFileBase>();
            postedfile.Setup(f => f.ContentLength).Returns(8192);
            postedfile.Setup(f => f.FileName).Returns("Test.jpeg");
            postedfile.Setup(f => f.ContentType).Returns("image/jpeg");
            postedfile.Setup(f => f.InputStream).Returns(OpenReadStream());
            postedfile.Setup(f => f.SaveAs(It.IsAny<string>())).Verifiable();

            FileUploadService fileUploadService = new FileUploadService();

            //Act
            FileUploadModel model = fileUploadService.SavePicture(postedfile.Object, "C:\\TestProject\\TMSV2", "Profile_Picture");

            //Assert
            Assert.True(model.FileName.Equals("Test.jpeg"));
            Assert.True(model.ModifiedFileName.Contains("-"));
            Assert.True(model.FileVirtualPath.Contains("/"));
            Assert.True(model.ModifiedFileName.Count(w => w == '-') <= 5);
            Assert.True(model.FileVirtualPath.Count(w => w == '/') <= 2);
        }

        [Fact]
        public void ShouldFailSavePictureWhenNoContent()
        {
            //Arrange
            var postedfile = new Mock<HttpPostedFileBase>();

            FileUploadService fileUploadService = new FileUploadService();

            //Act
            FileUploadModel model = fileUploadService.SavePicture(postedfile.Object, "C:\\TestProject\\TMSV2", "Profile_Picture");

            //Assert
            Assert.True(model==null);
        }

        public Stream OpenReadStream()
        {
            var text = "File uploaded successfully";
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(text);
            writer.Flush();
            ms.Position = 0;
            return ms;
        }
    }
}
