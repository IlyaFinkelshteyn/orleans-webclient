﻿using BrightSkool.BusinessLayer.BAL.Masters;
using BrightSkool.BusinessLayer.OrleansClient.Masters;
using BrightSkool.Domain.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BrightSkool.BusinessLayer.Test.BAL
{
    public class RaceServiceTest
    {
        [Fact]
        public void ShouldGetRaceList_BL()
        {
            //Arrange
            var raceCLient = new Mock<IRaceClient>();
            //Mocking RaceList method
            raceCLient.Setup(s => s.GetRace()).Returns(Task.Run(() => new List<CommonModel.Race> {
                new CommonModel.Race(){ RaceName = "Indian"},
                 new CommonModel.Race(){ RaceName = "American "},
            }));
            RaceService raceService = new RaceService(raceCLient.Object);

            //Act
            var result = raceService.GetRace().Result;

            //Assert
            Assert.True(result.Count > 0);
        }

        [Fact]
        public void ShouldGetEmptyRaceList_BL()
        {
            //Arrange
            var raceCLient = new Mock<IRaceClient>();
            //Mocking RaceList method
            raceCLient.Setup(s => s.GetRace()).Returns(Task.Run(() => new List<CommonModel.Race> ()));
            RaceService raceService = new RaceService(raceCLient.Object);

            //Act
            var result = raceService.GetRace().Result;

            //Assert
            Assert.True(result.Count ==0);

        }
    }
}
