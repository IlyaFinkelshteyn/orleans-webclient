﻿using BrightSkool.BusinessLayer.OrleansClient.Masters;
using Orleans;
using System;
using System.Collections.Generic;
using Xunit;
using static BrightSkool.Domain.Models.CommonModel;

namespace BrightSkool.BusinessLayer.Test.OrleansClient
{
    public class MastersTest
    {
        public MastersTest ()
        {
            if (!GrainClient.IsInitialized)
            {
                try
                {
                    GrainClient.Initialize(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "OrleansClientConfiguration.xml"));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        [Fact]
        public void ShouldGetCountries ()
        {
            //Arrange
            List<Country> countries = new List<Country>();
            //Act
            countries = new CountryClient().GetCountries().Result;
            //Assert
            Assert.NotNull(countries);
            Assert.IsType<List<Country>>(countries);
            Assert.NotEmpty(countries);
        }

        [Fact]
        public void ShouldGetCurrencies ()
        {
            //Arrange
            List<Currency> currencies = new List<Currency>();
            //Act
            currencies = new CurrencyClient().GetCurrencies().Result;
            //Assert
            Assert.NotNull(currencies);
            Assert.IsType<List<Currency>>(currencies);
            Assert.NotEmpty(currencies);
        }

        [Fact]
        public void ShouldGetLanguages ()
        {
            //Arrange
            List<Language> languages = new List<Language>();
            //Act
            languages = new LanguageClient().GetLanguages().Result;
            //Assert
            Assert.NotNull(languages);
            Assert.IsType<List<Language>>(languages);
            Assert.NotEmpty(languages);
        }

        [Fact]
        public void ShouldGetNationalities ()
        {
            //Arrange
            List<Nationality> nationalities = new List<Nationality>();
            //Act
            nationalities = new NationalityClient().GetNationalities().Result;
            //Assert
            Assert.NotNull(nationalities);
            Assert.IsType<List<Nationality>>(nationalities);
            Assert.NotEmpty(nationalities);
        }

        [Fact]
        public void ShouldGetRace ()
        {
            //Arrange
            List<Race> race = new List<Race>();
            //Act
            race = new RaceClient().GetRace().Result;
            //Assert
            Assert.NotNull(race);
            Assert.IsType<List<Race>>(race);
            Assert.NotEmpty(race);
        }

        [Fact]
        public void ShouldGetRelationships ()
        {
            //Arrange
            List<Relationship> relationships = new List<Relationship>();
            //Act
            relationships = new RelationshipClient().GetRelationships().Result;
            //Assert
            Assert.NotNull(relationships);
            Assert.IsType<List<Relationship>>(relationships);
            Assert.NotEmpty(relationships);
        }

        [Fact]
        public void ShouldGetReligions ()
        {
            //Arrange
            List<Religion> religions = new List<Religion>();
            //Act
            religions = new ReligionClient().GetReligions().Result;
            //Assert
            Assert.NotNull(religions);
            Assert.IsType<List<Country>>(religions);
            Assert.NotEmpty(religions);
        }
    }
}
