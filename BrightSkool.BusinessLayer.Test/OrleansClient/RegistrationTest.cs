﻿using BrightSkool.BusinessLayer.OrleansClient.Registration;
using BrightSkool.Domain.Dto;
using BrightSkool.Domain.Models;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BrightSkool.BusinessLayer.Test.OrleansClient
{
    public class RegistrationTest
    {
        public RegistrationTest ()
        {
            if (!GrainClient.IsInitialized)
            {
                try
                {
                    GrainClient.Initialize(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "OrleansClientConfiguration.xml"));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        [Fact]
        public void ShouldGetRegistration ()
        {
            //Arrange
            RegistrationModel Registration = new RegistrationModel();
            //Act
            Registration = new RegistrationClient().GetRegistrationbyID(new Guid("80f81f8c-0d25-4c41-9bb6-1dfe316e2906")).Result;
            //Assert
            Assert.IsType<RegistrationModel>(Registration);
            Assert.True(Registration != null);
        }

        [Fact]
        public async void ShouldGetRegistrations ()
        {
            //Arrange
            List<RegistrationModel> registrations = new List<RegistrationModel>();
            //Act
            var registrationclient = new RegistrationClient();
            registrations = await registrationclient.RegistrationsList();
            //Assert
            Assert.NotNull(registrations);
            Assert.IsType<List<RegistrationModel>>(registrations);
            Assert.True(registrations.Count > 0);
        }

        [Fact]
        public async void Shouldprocessapplicants ()
        {
            //Arrange
            List<RegistrationModel> registrations = new List<RegistrationModel>();
            List<Guid> lstguids = new List<Guid>();
            //Act
            var registrationclient = new RegistrationClient();
            registrations = await registrationclient.RegistrationsList();
            foreach (RegistrationModel item in registrations)
            {
                lstguids.Add(item.Id);
            }

            var ProcessState = await registrationclient.ProcessRegisteredApplicants(lstguids, "Waitlisted");
            registrations = await registrationclient.RegistrationsList();

            //Assert
            Assert.NotNull(registrations);
            Assert.IsType<List<RegistrationModel>>(registrations);
            Assert.True(registrations.Count > 0);
        }

        [Fact]
        public void ShouldSaveRegistration ()
        {
            //Arrange
            RegistrationDto registration = new RegistrationDto();

            registration.Id = new Guid("80f81f8c-0d25-4c41-9bb6-1dfe316e2906");
            //registration.Id = new Guid("05ecdb3a-b2bb-44e3-82fa-87a02bddf5fe");
            registration.PIN.FederalDocumentType = "Test document2";
            registration.PIN.PersonalIDNumber = "Test number2";
            registration.PIN.FederalDocumentType = "Test document2";
            registration.Nationality.CountryNationality = "Indian";
            registration.Applicant.FirstName = "Test Applicant2";
            registration.StudentAddress.AddressLine1 = "Test Address2";
            registration.StudentAddress.City = "Test City2";
            registration.StudentAddress.ZipCode = "560093";
            registration.StudentAddress.Country = "Test Country2";
            registration.PhoneNumber.PhoneNumber = "Test Number2";
            registration.Email = "test2@mail.com";
            registration.Level = "Test Level2";
            registration.SecondLanguage = "Test Language2";

            //Act
            RegistrationClient _registrationclient = new RegistrationClient();
            var Result = _registrationclient.SaveRegistration(registration).Result;

            //Assert
            Assert.True(Result);
        }
    }
}
