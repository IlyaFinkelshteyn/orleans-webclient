﻿using System;
using System.Collections.Generic;

namespace BrightSkool.Domain.Models
{
    public class CommonModel
    {
        [Serializable]
        public class Country
        {
            // public int CountryCode { get; set; }
            public string CountryCode { get; set; } // ISO character code for country
            public string CountryCodeShort { get; set; } // Short ISO character code for country
            public string CountryName { get; set; }
            public List<State> CountryStates { get; set; }

            public Country()
            {
                CountryStates = new List<State>();
            }
        }

        [Serializable]
        public class State
        {
            public string CountryName { get; set; }
            public string StateName { get; set; }
            public List<City> StateCity { get; set; }

            public State()
            {
                StateCity = new List<City>();
            }
        }

        [Serializable]
        public class City
        {
            public string StateName { get; set; }
            public string CityName { get; set; }
        }

        [Serializable]
        public class Currency
        {
            public string CurrencyCode { get; set; } // ISO character code for currency
            public string CurrencyName { get; set; }
            public string CountryName { get; set; }
        }

        [Serializable]
        public class Language
        {
            public string LangaugeCode { get; set; } // ISO character code for Language
            public string LanguageName { get; set; }
        }

        [Serializable]
        public class Religion
        {
            public string ReligionName { get; set; }
        }

        [Serializable]
        public class Race
        {
            public string RaceName { get; set; }
        }

        [Serializable]
        public class Relationship
        {
            public string Relation { get; set; }
        }

        [Serializable]
        public class Address
        {
            public string AddressLine1 { get; set; }
            public string AddressLine2 { get; set; }
            public string AddressLine3 { get; set; }
            public string ZipCode { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Country { get; set; }
            public Country CountryDetail { get; set; }

            public Address()
            {
                CountryDetail = new Country();
            }
        }

        [Serializable]
        public class Phone
        {
            public string CountryCode { get; set; }
            public string AreaCode { get; set; }
            public string PhoneNumber { get; set; }
        }

        [Serializable]
        public class Guardian
        {
            public string FirstName { get; set; }
            public string MiddleName { get; set; }
            public string LastName { get; set; }
            public string RelationShip { get; set; }
            public string Email { get; set; }
            public string BusinessEmail { get; set; }
            public List<Address> GuardianAddress { get; set; }
            public Phone GuardianPhone { get; set; }
            public Phone GuardianBusinessPhone { get; set; }
        }

        [Serializable]
        public class Person
        {
            public string FirstName { get; set; }
            public string MiddleName { get; set; }
            public string LastName { get; set; }
            public DOB DOB { get; set; }
            public string Gender { get; set; }
            public string ProfilePicFilePath { get; set; }
            public Person()
            {
                DOB = new DOB();
            }
        }

        [Serializable]
        public class Email
        {
            public string EmailAddress { get; set; }
        }

        [Serializable]
        public class Nationality
        {
            public string CountryNationality { get; set; }
            public string ISONationality { get; set; }
        }

        [Serializable]
        public class DOB
        {
            public int Year { get; set; }
            public int Month { get; set; }
            public int Date { get; set; }
        }

        [Serializable]
        public class PIN
        {
            public string PersonalIDNumber { get; set; }
            public string FederalDocumentType { get; set; } //list of all the valid federal documents should be enumerated here
        }
    }
}