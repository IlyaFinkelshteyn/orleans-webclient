﻿namespace BrightSkool.Domain.Models
{
    public class FileUploadModel
    {
        public string FileName { get; set; }
        public string ModifiedFileName { get; set; }
        public string FileVirtualPath { get; set; }
    }
}
