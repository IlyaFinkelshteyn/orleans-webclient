﻿using System;
using System.Collections.Generic;
using static BrightSkool.Domain.Models.CommonModel;

namespace BrightSkool.Domain.Models
{
    [Serializable]
    public class RegistrationModel
    {
        public Guid Id { get; set; }
        public string ApplicationNumber { get; set; }
        public Person Applicant { get; set; }
        public PIN PIN { get; set; }
        public string Level { get; set; }
        public Address StudentAddress { get; set; }
        public Nationality Nationality { get; set; }
        public Religion Religion { get; set; }
        public string SecondLanguage { get; set; }
        public string MotherTongue { get; set; }
        public List<Guardian> GuardianDetails { get; set; }
        public Phone PhoneNumber { get; set; }
        public string Email { get; set; }
        public bool IsTermsAndConditionAccept { get; set; }
        public bool IsUsageOfDataAccept { get; set; }        
        public List<StudentDocuments> UploadedDocuments { get; set; }
        public string AllergyType { get; set; }
        public string AllergicTo { get; set; }
        #region Sibling Info
        public bool IsSiblingStudyingInSameSchool { get; set; }
        public string StudentId { get; set; }
        #endregion
        public ApplicationStatus Status { get; set; }
        public DateTime RegisteredDate { get; set; }
        public RegistrationModel()
        {
            Applicant = new Person();
            PIN = new PIN();
            StudentAddress = new Address();
            Nationality = new Nationality();
            Religion = new Religion();
            GuardianDetails = new List<Guardian>();
            PhoneNumber = new Phone();
            UploadedDocuments = new List<StudentDocuments>();
            Status = new ApplicationStatus();
        }

    }
  
    [Serializable]
    public class ApplicationStatus
    {
        public string Status { get; set; }
        public string Code { get; set; }
    }


    [Serializable]
    public class StudentDocuments
    {
        public string Name { get; set; }
        public string FilePath { get; set; }
    }
}
