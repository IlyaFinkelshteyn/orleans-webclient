﻿using BrightSkool.Domain.Models;
using System;

namespace BrightSkool.Domain.Dto
{
    [Serializable]
    public class RegistrationDto : RegistrationModel
    {
        public DateTime DOB { get; set; }
        public decimal DistanceToSchool { get; set; }
        public int Age { get; set; }
        public string Sibbling { get; set; }
    }
}
